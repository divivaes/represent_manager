window.$ = window.jQuery = require('jquery');

import Popper from 'popper.js/dist/umd/popper.js';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic/build/ckeditor';

window.Popper = Popper;
window.ClassicEditor = ClassicEditor;

import moment from 'moment';
window.moment = moment;

require('bootstrap');
require('metismenu');
require('jquery-slimscroll');
require('select2');
require('jquery-datetimepicker');

require('./partials/inspinia');

$(function () {
    $('.datetime-picker').datetimepicker();

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    initSelect2();
    initDatePicker();
    initDateTimePicker();

    if ($('#content-editor').length) {
        ClassicEditor
            .create(document.querySelector('#content-editor'))
            .then(editor => {
                // console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    }

});

function initDatePicker() {
    const datePickerSelector = $('.form-control-datepicker');

    if (datePickerSelector.length) {
        datePickerSelector.each(function () {
            $(this).datetimepicker({
                timepicker: false,
                format: 'Y-m-d'
            });
        });
    }
}

function initDateTimePicker() {
    const dateTimePickerSelector = $('.form-control-datetimepicker');

    if (dateTimePickerSelector.length) {
        dateTimePickerSelector.each(function () {
            $(this).datetimepicker({
                format: 'Y-m-d H:i:s'
            });
        });
    }
}

function initSelect2() {
    const select2Selector = $('.form-control-select2');

    if (select2Selector.length) {
        select2Selector.each(function () {
            $(this).select2({
                placeholder: "Select",
                allowClear: true
            });
        });
    }
}

