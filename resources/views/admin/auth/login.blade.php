<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Reach advance | Admin</title>

        <link href="{{ mix('/css/admin.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="ibox mt-5">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3 class="m-t-none m-b">Reach advance</h3>
                                    <p>Beautifully simple digital advertising for real estate.</p>
                                    @include('flash::message')
                                    <form novalidate role="form" method="post" action="{{ route('admin.auth.authenticate') }}">
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <label for="form--login--email">E-mail address:</label>
                                            <input id="form--login--email" type="email" name="email" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="form--login--password">Password:</label>
                                            <input id="form--login--password" type="password" name="password" class="form-control">
                                        </div>
                                        <div>
                                            <button class="btn btn-sm btn-primary float-right m-t-n-xs" type="submit" id="form--login--submit">
                                                <strong>Log in</strong>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ mix('/js/admin.js') }}"></script>
    </body>
</html>