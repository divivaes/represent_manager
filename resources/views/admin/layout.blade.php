<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Reach Advance | Admin</title>

        <link href="{{ mix('/css/admin.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="logo-element">
                                Reach Advance
                            </div>
                        </li>
                        <li{!! Request::is('admin/dashboard' . '*') ? ' class="active"' :  '' !!}>
                            <a href="{{ route('admin.dashboard.index') }}">
                                <i class="fa fa-pie-chart"></i>
                                <span class="nav-label">Dashboard</span>
                            </a>
                        </li>
                        <li{!! Request::is('admin/accounts' . '*') || Request::is('admin/users' . '*') ? ' class="active"' :  '' !!}>
                            <a href="#">
                                <i class="fa fa-key"></i>
                                <span class="nav-label">Vault</span>
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse">
                                <li{!! Request::is('admin/accounts' . '*') ? ' class="active"' :  '' !!}>
                                    <a href="{{ route('admin.accounts.index') }}">
                                        <i class="fa fa-building"></i>
                                        <span class="nav-label">Accounts</span>
                                    </a>
                                </li>
                                <li{!! Request::is('admin/users' . '*') ? ' class="active"' :  '' !!}>
                                    <a href="{{ route('admin.users.index') }}">
                                        <i class="fa fa-users"></i>
                                        <span class="nav-label">Users</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li{!! Request::is('admin/subscriptions' . '*') || Request::is('admin/charges' . '*') || Request::is('admin/plans' . '*') ? ' class="active"' :  '' || Request::is('admin/transactions' . '*') || Request::is('admin/payments' . '*') ? ' class="active"' :  '' !!}>
                            <a href="#">
                                <i class="fa fa-money"></i>
                                <span class="nav-label">Payments</span>
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level collapse">
                                <li{!! Request::is('admin/plans' . '*') ? ' class="active"' :  '' !!}>
                                    <a href="{{ route('admin.plans.index') }}">
                                        <i class="fa fa-bookmark"></i>
                                        <span class="nav-label">Plans</span>
                                    </a>
                                </li>
                                <li{!! Request::is('admin/charges' . '*') ? ' class="active"' :  '' !!}>
                                    <a href="{{ route('admin.charges.index') }}">
                                        <i class="fa fa-usd"></i>
                                        <span class="nav-label">One-off charges</span>
                                    </a>
                                </li>
                                <li{!! Request::is('admin/subscriptions' . '*') ? ' class="active"' :  '' !!}>
                                    <a href="{{ route('admin.subscriptions.index') }}">
                                        <i class="fa fa-refresh"></i>
                                        <span class="nav-label">Subscriptions</span>
                                    </a>
                                </li>
                                <li{!! Request::is('admin/transactions' . '*') ? ' class="active"' :  '' !!}>
                                    <a href="{{ route('admin.transactions.index') }}">
                                        <i class="fa fa-list-ol"></i>
                                        <span class="nav-label">Transactions</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li{!! Request::is('admin/properties' . '*') ? ' class="active"' :  '' !!}>
                            <a href="{{ route('admin.properties.index') }}">
                                <i class="fa fa-building-o"></i>
                                <span class="nav-label">Properties</span>
                            </a>
                        </li>
                        <li{!! Request::is('admin/webhooks' . '*') ? ' class="active"' :  '' !!}>
                            <a href="{{ route('admin.webhooks.index') }}">
                                <i class="fa fa-database"></i>
                                <span class="nav-label">Webhooks</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#">
                                <i class="fa fa-bars"></i>
                            </a>
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li style="padding: 20px">
                                <span class="m-r-sm text-muted welcome-message">
                                    Welcome, {{ Auth::user()->email }}
                                </span>
                            </li>
                            <li>
                                <a href="{{ route('admin.auth.logout') }}">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                @yield('breadcrumb')
                <div class="wrapper wrapper-content">
                    <div class="row">
                        <div class="col-lg-12">
                            @yield('statistics')
                            @yield('filters')
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul class="mb-0">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @include('flash::message')
                            @yield('content')
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div>
                        <strong>Copyright</strong> MTR Design &copy; {{ date('Y') }}
                    </div>
                </div>
            </div>
        </div>
        <script src="https://js.braintreegateway.com/web/dropin/1.20.0/js/dropin.min.js"></script>
        <script src="{{ mix('/js/admin.js') }}"></script>
        @yield('script')
    </body>
</html>
