<div class="input-group">
    <input id="{{ $id }}" name="{{ $name }}" value="{{ $value }}" type="text" class="form-control form-control-datetimepicker">
    <div class="input-group-append">
        <div class="input-group-text">
            <i class="fa fa-calendar"></i>
        </div>
    </div>
</div>
