<form class="d-inline" action="{{ isset($custom_route) ? $custom_route : route('admin.' . $route . '.update', $record) }}" method="post">
    {{ csrf_field() }}
    {{ method_field('put') }}

    <input type="hidden" name="is_active" value="{{ $record->is_active == 1 ? 0 : 1 }}">

    <button class="btn btn-primary{{ $record->is_active == 1 ? '' : ' btn-outline' }} btn-xs">
        <i class="fa fa-check"></i>
    </button>
</form>
