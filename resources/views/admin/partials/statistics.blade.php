<div class="row">
    <div class="col-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>
                    <i class="fa fa-line-chart"></i>&nbsp; Statistics
                </h5>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-hover table-striped mb-0">
                        <tbody>
                            <tr>
                                <td>Number of new records for last 24 hours:</td>
                                <td>{{ $statistics['last_day']->count() }}</td>
                            </tr>
                            <tr>
                                <td>Number of new records for last 7 days:</td>
                                <td>{{ $statistics['last_seven_days']->count() }}</td>
                            </tr>
                            <tr>
                                <td>Number of new records for last 30 days:</td>
                                <td>{{ $statistics['last_thirty_days']->count() }}</td>
                            </tr>
                            <tr>
                                <td>Total number records:</td>
                                <td>{{ $statistics['total']->count() }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>