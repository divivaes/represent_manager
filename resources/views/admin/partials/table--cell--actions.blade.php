@if (isset($editButtonIsDisabled) && $editButtonIsDisabled === true)
    {{-- --}}
@else
    <a class="btn btn-primary btn-xs" href="{{ route('admin.' . $route . '.edit', isset($url_parameters) ? $url_parameters : $record) }}">
        <i class="fa fa-pencil"></i>&nbsp;
        Edit
    </a>
@endif
@if (isset($removeButtonIsDisabled) && $removeButtonIsDisabled === true)
    {{-- --}}
@else
    <form onsubmit="return confirm('Are you sure you want to delete this record?');" class="d-inline" action="{{ route('admin.' . $route . '.destroy', isset($url_parameters) ? $url_parameters : $record) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('delete') }}

        <button class="btn btn-danger btn-xs">
            <i class="fa fa-times"></i>&nbsp;
            Remove
        </button>
    </form>
@endif
