@extends('client.layout')

@section('content')

    <div class="container mb-5">
        <div class="row d-flex justify-content-center">
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center mt-5 mb-5">
                        <h3 class="panel-title">Forgot password</h3>
                    </div>
                    <div class="panel-body">
                        @include('flash::message')
                        <form action="{{ route('client.password.email') }}" method="post">
                            @csrf
                            <div class="form-group mb-4">
                                <input type="email" name="email" id="email" class="form-control input-sm" placeholder="Business Email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="text-muted">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group text-center mt-5">
                                <input type="submit" value="Send Reset Link" class="btn btn-secondary btn-md px-5 py-2">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
