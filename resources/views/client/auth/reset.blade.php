@extends('client.layout')

@section('content')

    <div class="container mb-5">
        <div class="row d-flex justify-content-center">
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center mt-5 mb-5">
                        <h3 class="panel-title">Reset Password</h3>
                    </div>
                    <div class="panel-body">
                        @include('flash::message')
                        <form action="{{ route('client.password.update') }}" method="post">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group mb-4">
                                <input type="email" name="email" id="email" class="form-control input-sm" placeholder="Business Email" value="{{ $email ?? old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="text-muted">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group mb-4">
                                <input type="password" name="password" id="password" class="form-control input-sm" placeholder="New Password" required>
                                @if ($errors->has('password'))
                                    <span class="text-muted">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group mb-4">
                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm New Password" required>
                            </div>

                            <div class="form-group text-center mt-5">
                                <input type="submit" value="Submit" class="btn btn-secondary btn-md text-uppercase px-5 py-2">
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
