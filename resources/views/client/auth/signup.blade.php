@extends('client.layout')

@section('content')

    <div class="container mb-5">
        <div class="row d-flex justify-content-center">
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center mt-5 mb-5">
                        <h3 class="panel-title">Get Started</h3>
                    </div>
                    <div class="panel-body">
                        <form action="{{ route('client.auth.register') }}" method="post">
                            @csrf

                            <div class="row mb-3">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="first_name" id="first_name" class="form-control input-sm" placeholder="First Name" value="{{ old('first_name') }}">
                                        @if ($errors->has('first_name'))
                                            <span class="text-muted">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="last_name" id="last_name" class="form-control input-sm" placeholder="Last Name" value="{{ old('last_name') }}">

                                        @if ($errors->has('last_name'))
                                            <span class="text-muted">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group mb-4">
                                <input type="email" name="email" id="email" class="form-control input-sm" placeholder="Business Email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="text-muted">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group mb-4">
                                <input type="tel" name="phone" id="phone" class="form-control input-sm" placeholder="Business Phone" value="{{ old('phone') }}" maxlength="15" required>

                                @if ($errors->has('phone'))
                                    <span class="text-muted">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="row mt-3">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    @if ($errors->has('password'))
                                        <span class="text-muted">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group text-center mt-5">
                                <input type="submit" value="Submit" class="btn btn-secondary btn-md text-uppercase px-5 py-2">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
