@component('mail::message')
# Request for password change

Click on the button to change your password

@component('mail::button', ['url' => 'http://127.0.0.1:8000/password/reset/' . $email . '/' . $token])
Reset your password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
