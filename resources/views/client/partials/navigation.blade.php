<nav class="navbar navbar-expand-lg navbar-light bg-gray">
    <div class="container">
        <a class="navbar-brand" href="{{ route('client.pages.index') }}">Reach Advance</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigationBar" aria-controls="navigationBar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navigationBar">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">How It Works</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('client.pages.pricing') }}">Pricing</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="servicesDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services</a>
                    <div class="dropdown-menu" aria-labelledby="servicesDropdown">
                        <a class="dropdown-item" href="#">Service 1</a>
                        <a class="dropdown-item" href="#">Service 2</a>
                        <a class="dropdown-item" href="#">Service 3</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">View Demo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link phone-number" href="#">1(800) 555-5555</a>
                </li>
                @if (auth()->check() && auth()->user()->role === 'user')
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('client.account.edit') }}">Profile</a>
                    </li>
                    <li class="nav-item signup-btn">
                        <a class="nav-link " href="{{ route('client.auth.logout') }}">Logout</a>
                    </li>
                @else
                    <li class="nav-item login-btn">
                        <a class="nav-link" href="{{ route('client.auth.signin') }}">Log In</a>
                    </li>
                    <li class="nav-item signup-btn">
                        <a class="nav-link " href="{{ route('client.auth.signup') }}">Sign Up</a>
                    </li>
                @endguest
            </ul>

        </div>
    </div>
</nav>
