<footer id="footer">
    <div class="container">
        <div class="row text-left">
            <div class="col-xs-12 col-sm-12 col-md-12 logo">
                <h2>LOGO</h2>
            </div>
        </div>
        <div class="row text-center text-xs-center text-sm-left text-md-left">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>Company</h5>
                <ul class="list-unstyled quick-links">
                    <li>
                        <a href="#">About us</a>
                    </li>
                    <li>
                        <a href="#">Blog</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>Services</h5>
                <ul class="list-unstyled quick-links">
                    <li>
                        <a href="#">Facebook Posts</a>
                    </li>
                    <li>
                        <a href="#">Branded Websites</a>
                    </li>
                    <li>
                        <a href="#">Videos</a>
                    </li>
                    <li>
                        <a href="#">Ad Campaign Management</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>Quick Links</h5>
                <ul class="list-unstyled quick-links">
                    <li>
                        <a href="{{ route('client.pages.pricing') }}">Pricing</a>
                    </li>
                    <li>
                        <a href="#">How It Works</a>
                    </li>
                    <li>
                        <a href="#">Privacy Policy</a>
                    </li>
                    <li>
                        <a href="#">Terms & Conditions</a>
                    </li>
                    <li>
                        <a href="#">FAQ</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <h5>Contact Us</h5>
                <ul class="list-unstyled quick-links">
                    <li>
                        Call Us:<br>
                        <a href="#">1(800) 555-5555</a>
                    </li>
                    <li>
                        Support:<br>
                        <a href="#">support@reachadvance.com</a>
                    </li>
                    <li>
                        Sales:<br>
                        <a href="#">sales@reachadvance.com</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
