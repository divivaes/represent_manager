@extends('client.layout')

@section('content')
    <section class="hero">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-xs-12 text-center">
                    <img src="/img/client/grey.jpg" width="500"  class="img-thumbnail" alt="Background Alt" />
                </div>
                <div class="col-md-5 col-xs-12 text-left mt-5">
                    <h2>Hero Header</h2>
                    <h4>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet</h4>
                    <a href="#" class="btn btn-lg btn-secondary px-5">Get Started</a>
                </div>
            </div>
        </div>
    </section>

    <section class="features">
        <h2 class="text-center mb-2">HOW IT WORKS</h2>
        <div class="container">
            <div class="row py-5 px-2">
                <div class="col-md-6 col-xs-12 text-center">
                    <img src="/img/client/grey.jpg" width="450"  class="img-thumbnail" alt="Background Alt" />
                </div>
                <div class="col-md-6 col-xs-12 mt-5 ml-0 px-5">
                    <h2>Feature 1</h2>
                    <h5>Lorem ipsum dolor sit amet, et vel veri paulo. Ea eirmod utamur scriptorem vim. Sed ex illud partem signiferumque. Choro utamur oportere ea per. At tritani gubergren eos. Facete phaedrum vix no, nullam eirmod eu pro. Nisl postulant eloquentiam per an, recteque partiendo te mel, facer iracundia</h5>
                </div>
            </div>

            <div class="row py-5 px-2">
                <div class="col-md-6 col-xs-12 mt-5 ml-0 px-5">
                    <h2>Feature 2</h2>
                    <h5>Lorem ipsum dolor sit amet, et vel veri paulo. Ea eirmod utamur scriptorem vim. Sed ex illud partem signiferumque. Choro utamur oportere ea per. At tritani gubergren eos. Facete phaedrum vix no, nullam eirmod eu pro. Nisl postulant eloquentiam per an, recteque partiendo te mel, facer iracundia</h5>
                </div>
                <div class="col-md-6 col-xs-12 text-center">
                    <img src="/img/client/grey.jpg" width="450"  class="img-thumbnail" alt="Background Alt" />
                </div>
            </div>

            <div class="row py-5 px-2">
                <div class="col-md-6 col-xs-12 text-center">
                    <img src="/img/client/grey.jpg" width="450"  class="img-thumbnail" alt="Background Alt" />
                </div>
                <div class="col-md-6 col-xs-12 mt-5 ml-0 px-5">
                    <h2>Feature 3</h2>
                    <h5>Lorem ipsum dolor sit amet, et vel veri paulo. Ea eirmod utamur scriptorem vim. Sed ex illud partem signiferumque. Choro utamur oportere ea per. At tritani gubergren eos. Facete phaedrum vix no, nullam eirmod eu pro. Nisl postulant eloquentiam per an, recteque partiendo te mel, facer iracundia</h5>
                </div>
            </div>

            <div class="row py-5 px-2">
                <div class="col-md-6 col-xs-12 mt-5 ml-0 px-5">
                    <h2>Feature 4</h2>
                    <h5>Lorem ipsum dolor sit amet, et vel veri paulo. Ea eirmod utamur scriptorem vim. Sed ex illud partem signiferumque. Choro utamur oportere ea per. At tritani gubergren eos. Facete phaedrum vix no, nullam eirmod eu pro. Nisl postulant eloquentiam per an, recteque partiendo te mel, facer iracundia</h5>
                </div>
                <div class="col-md-6 col-xs-12 text-center">
                    <img src="/img/client/grey.jpg" width="450"  class="img-thumbnail" alt="Background Alt" />
                </div>
            </div>


            <div class="row py-5 px-2">
                <div class="col-md-6 col-xs-12 text-center">
                    <img src="/img/client/grey.jpg" width="450"  class="img-thumbnail" alt="Background Alt" />
                </div>
                <div class="col-md-6 col-xs-12 mt-5 ml-0 px-5">
                    <h2>Feature 5</h2>
                    <h5>Lorem ipsum dolor sit amet, et vel veri paulo. Ea eirmod utamur scriptorem vim. Sed ex illud partem signiferumque. Choro utamur oportere ea per. At tritani gubergren eos. Facete phaedrum vix no, nullam eirmod eu pro. Nisl postulant eloquentiam per an, recteque partiendo te mel, facer iracundia</h5>
                </div>
            </div>

            <div class="row mt-5 mb-5">
                <div class="col-md-12 col-xs-12 text-center">
                    <a href="#" class="btn btn-lg btn-secondary px-5">Get Started</a>
                </div>
            </div>
        </div>
    </section>

    <section class="stats">
        <div class="container">
            <div class="row  text-center">
                <div class="col-md-4">
                    <img src="/img/client/grey.jpg" width="450" class="img-thumbnail" alt="Background Alt" />
                    <h2 class="mt-1">Stats 1</h2>
                </div>
                <div class="col-md-4">
                    <img src="/img/client/grey.jpg" width="450" class="img-thumbnail" alt="Background Alt" />
                    <h2 class="mt-1">Stats 2</h2>
                </div>
                <div class="col-md-4">
                    <img src="/img/client/grey.jpg" width="450" class="img-thumbnail" alt="Background Alt" />
                    <h2 class="mt-1">Stats 3</h2>
                </div>
            </div>
        </div>
    </section>

    <section class="leads py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12 mt-5 ml-0 px-5">
                    <h1 class="text-white display-4">Maximize your leads with Reach Advance</h1>
                    <a href="#" class="btn btn-lg btn-secondary px-5 mt-5">Get Started</a>
                </div>
                <div class="col-md-6 col-xs-12 text-center">
                    <img src="/img/client/grey.jpg" width="500" height="500" alt="Background Alt" />
                </div>
            </div>
        </div>
    </section>

    <section class="clients mb-3">
        <h1 class="text-center mt-5">WHAT OUR CLIENTS ARE SAYING ABOUT US</h1>
        <div id="carouselExampleIndicators" class="carousel slide mt-5 mb-5" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 clients-image text-center px-5">
                                <img class="d-block" src="/img/client/grey.jpg"  alt="First slide">
                            </div>
                            <div class="col-md-8">
                                <div class="clients_box">
                                    <h3 class="clients_box-title">LOVE REACH ADVANCE</h3>
                                    <h5 class="mt-4 clients_box-comment">
                                        Lorem ipsum dolor sit amet, et vel veri paulo. Ea eirmod utamur scriptorem vim. Sed ex illud partem signiferumque. Choro utamur oportere ea per. At tritani gubergren eos. Facete phaedrum vix no, nullam eirmod eu pro. Nisl postulant eloquentiam per an, recteque partiendo te mel, facer iracundia
                                    </h5>
                                    <h5 class="pull-right mt-2">John Doe</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev"  href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>

    <section class="promo mt-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-3 text-center mt-5">
                    <h1 class="display-4">LOGO</h1>
                </div>
                <div class="col-md-9">
                    <img src="/img/client/grey.jpg" width="600" height="350" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="join bg-gray mt-2 px-3 py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h2 class="font-weight-light">JOIN THOUSANDS OF SUCCESSFUL AGENTS</h2>
                </div>
                <div class="col-md-4 text-center">
                    <a href="#" class="btn btn-secondary btn-lg">Get Started</a>
                </div>
            </div>
        </div>
    </section>
@endsection
