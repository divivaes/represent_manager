@extends('client.layout')

@section('content')

    <section class="pricing py-5">
        <div class="container">
            <div class="row">
                <!-- Free Tier -->
                <div class="col-lg-4 price-box">
                    <div class="card mb-5 mb-lg-0">
                        <div class="card-body">
                            <div class="card-header">
                                <h5 class="card-title text-center">Free</h5>
                                <h6 class="card-price text-center">$199<span class="period">/month</span></h6>
                            </div>
                            <div class="card-description">
                                <p>Lorem ipsum dolor sit amet, et vel veri paulo. Ea eirmod utamur scriptorem vim. Sed ex illud partem signiferumque.</p>
                            </div>
                            <div class="card-divider">
                                <hr>
                            </div>
                            <ul class="fa-ul">
                                <li>Scheduled Facebook Posts <i class="fa fa-question-circle-o"></i></li>
                                <li>Branded Website</li>
                                <li>Scheduled Website Posts</li>
                            </ul>
                            <div class="card-footer text-center">
                                <a href="{{ route('client.payment.index', ['plan_id' => 'pl1']) }}" class="btn btn-secondary btn-md px-5">Get Started</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Plus Tier -->
                <div class="col-lg-4 price-box">
                    <div class="card mb-5 mb-lg-0">
                        <div class="card-body">
                            <div class="card-header">
                                <h5 class="card-title text-center">Pro</h5>
                                <h6 class="card-price text-center">$299<span class="period">/month</span></h6>
                            </div>
                            <div class="card-description">
                                <p>Lorem ipsum dolor sit amet, et vel veri paulo. Ea eirmod utamur scriptorem vim. Sed ex illud partem signiferumque.</p>
                            </div>
                            <div class="card-divider">
                                <hr>
                            </div>
                            <ul class="fa-ul">
                                <li>Scheduled Facebook Posts <i class="fa fa-question-circle-o"></i></li>
                                <li>Branded Website</li>
                                <li>Scheduled Website Posts</li>
                                <li>Local Market Update Videos <i class="fa fa-question-circle-o"></i></li>
                                <li>Dedicated Account Manager</li>
                                <li><b>Guaranteed Reach</b> - 5,0000 Prospects <i class="fa fa-question-circle-o"></i></li>
                            </ul>
                            <div class="card-footer text-center">
                                <a href="{{ route('client.payment.index', ['plan_id' => 'pl2']) }}" class="btn btn-secondary btn-md px-5">Get Started</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Pro Tier -->
                <div class="col-lg-4 price-box">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-header">
                                <h5 class="card-title text-center">Premium</h5>
                                <h6 class="card-price text-center">$399<span class="period">/month</span></h6>
                            </div>
                            <div class="card-description">
                                <p>Lorem ipsum dolor sit amet, et vel veri paulo. Ea eirmod utamur scriptorem vim. Sed ex illud partem signiferumque.</p>
                            </div>
                            <div class="card-divider">
                                <hr>
                            </div>
                            <ul class="fa-ul">
                                <li>Scheduled Facebook Posts <i class="fa fa-question-circle-o"></i></li>
                                <li>Branded Website</li>
                                <li>Scheduled Website Posts</li>
                                <li>Local Market Update Videos <i class="fa fa-question-circle-o"></i></li>
                                <li>Dedicated Account Manager</li>
                                <li><b>Guaranteed Reach</b> - 10,0000 Prospects <i class="fa fa-question-circle-o"></i></li>
                                <li>Ad Campaign Management</li>
                                <li>Dedicated Phone Support</li>
                            </ul>
                            <div class="card-footer text-center">
                                <a href="{{ route('client.payment.index', ['plan_id' => 'pl3']) }}" class="btn btn-secondary btn-md px-5">Get Started</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
