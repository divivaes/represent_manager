<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Reach advance</title>

        <link href="{{ mix('/css/client.css') }}" rel="stylesheet">
    </head>
    <body>
        @include('client.partials.navigation')
        @yield('content')
        @include('client.partials.footer')

        <script src="https://js.braintreegateway.com/web/dropin/1.20.0/js/dropin.min.js"></script>
        <script src="{{ mix('/js/client.js') }}"></script>
        @yield('script')
    </body>
</html>
