<?php

namespace App\Repositories\Client;

use App\Models\AccountUser;
use Illuminate\Database\Eloquent\Model;

class AccountUserRepository extends Repository
{
    protected $model;

    public function __construct(AccountUser $model)
    {
        $this->model = $model;
    }

    public function create(array $data = []): Model
    {
        return parent::create($data);
    }
}
