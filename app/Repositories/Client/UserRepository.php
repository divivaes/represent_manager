<?php

namespace App\Repositories\Client;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserRepository extends Repository
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function getByEmail($email): Model
    {
        return $this->model->byEmail($email)->first();
    }

    public function firstOrCreate(array $params1 = [], array $params2 = []): Model
    {
        return $this->model->firstOrCreate($params1, $params2);
    }

    public function create(array $data = []): Model
    {
        if (isset($data['password'])
            && isset($data['password_confirmation'])
            && mb_strlen($data['password'])
            && mb_strlen($data['password_confirmation'])
        ) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        return parent::create($data);
    }

    public function update(Model $model, array $data = []): Model
    {
        if (isset($data['password'])
            && isset($data['password_confirmation'])
            && mb_strlen($data['password'])
            && mb_strlen($data['password_confirmation'])
        ) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        return parent::update($model, $data);
    }
}
