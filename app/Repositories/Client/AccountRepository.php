<?php

namespace App\Repositories\Client;

use App\Models\Account;
use Illuminate\Database\Eloquent\Model;

class AccountRepository extends Repository
{
    protected $model;

    public function __construct(Account $model)
    {
        $this->model = $model;
    }

    public function create(array $data = []): Model
    {
        return parent::create($data);
    }
}
