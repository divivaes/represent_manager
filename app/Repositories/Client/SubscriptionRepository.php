<?php

namespace App\Repositories\Client;

use App\Models\Subscription;
use Illuminate\Database\Eloquent\Model;

class SubscriptionRepository extends Repository
{
    protected $model;

    public function __construct(Subscription $model)
    {
        $this->model = $model;
    }

    public function create(array $data = []): Model
    {
        return parent::create($data);
    }
}
