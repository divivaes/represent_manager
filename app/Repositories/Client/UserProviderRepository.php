<?php

namespace App\Repositories\Client;

use App\Models\UserProvider;
use Illuminate\Database\Eloquent\Model;

class UserProviderRepository extends Repository
{
    protected $model;

    public function __construct(UserProvider $model)
    {
        $this->model = $model;
    }

    public function getById($id): ?Model
    {
        return $this->model->ByUser($id)->first();
    }

    public function updateOrCreate(array $params1 = [], array $params2 = []): Model
    {
        return $this->model->updateOrCreate($params1, $params2);
    }
}
