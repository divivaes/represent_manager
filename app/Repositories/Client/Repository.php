<?php

namespace App\Repositories\Client;

use Illuminate\Database\Eloquent\Model;

class Repository
{
    protected $model;

    public function prepare()
    {
        return $this->model;
    }

    public function create(array $data = []): Model
    {
        $model = $this->prepare();
        $model->fill($data);
        $model->save();

        return $model;
    }

    public function update(Model $model, array $data = []): Model
    {
        $model->fill($data);
        $model->save();

        return $model;
    }
}
