<?php

namespace App\Repositories\Admin;

use App\Models\AccountUpdate;
use App\Repositories\Admin\Traits\GetStatisticsTrait;

class AccountUpdateRepository extends Repository
{
    use GetStatisticsTrait;

    protected $model;

    public function __construct(AccountUpdate $model)
    {
        $this->model = $model;
    }

    public function getRecords(array $data = [])
    {
        $shows = $this->prepareFilterQuery($data);

        return $shows;
    }

    protected function prepareFilterQuery(array $data = [])
    {
        $records = $this->model->sort();

        return $records;
    }
}
