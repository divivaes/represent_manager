<?php

namespace App\Repositories\Admin;

use App\Models\PropertyVideo;
use Illuminate\Database\Eloquent\Model;

class PropertyVideoRepository extends Repository
{
    protected $model;

    public function __construct(PropertyVideo $model)
    {
        $this->model = $model;
    }

    public function create(array $data = []): Model
    {
        return parent::create($data);
    }

    public function update(Model $model, array $data = []): Model
    {
        return parent::update($model, $data);
    }
}
