<?php

namespace App\Repositories\Admin;

use App\Models\User;
use App\Repositories\Admin\Traits\GetStatisticsTrait;
use Illuminate\Database\Eloquent\Model;

class UserRepository extends Repository
{
    use GetStatisticsTrait;

    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function create(array $data = []): Model
    {
        if (isset($data['password'])
            && isset($data['password_confirmation'])
            && mb_strlen($data['password'])
            && mb_strlen($data['password_confirmation'])
        ) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        return parent::create($data);
    }

    public function update(Model $model, array $data = []): Model
    {
        if (isset($data['password'])
            && isset($data['password_confirmation'])
            && mb_strlen($data['password'])
            && mb_strlen($data['password_confirmation'])
        ) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        return parent::update($model, $data);
    }

    public function getRecords(array $data = [])
    {
        $shows = $this->prepareFilterQuery($data);

        return $shows;
    }

    protected function prepareFilterQuery(array $data = [])
    {
        $records = $this->model->sort();

        $records = $records->byAccount(isset($data['account_id']) ? $data['account_id'] : null);
        $records = $records->byFullName(isset($data['name']) ? $data['name'] : null);
        $records = $records->byEmail(isset($data['email']) ? $data['email'] : null);
        $records = $records->byRole(isset($data['role']) ? $data['role'] : null);
        $records = $records->byIsActive(isset($data['is_active']) ? $data['is_active'] : null);

        return $records;
    }
}
