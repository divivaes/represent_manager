<?php

namespace App\Repositories\Admin;

use App\Models\AccountContact;
use Illuminate\Database\Eloquent\Model;

class AccountContactRepository extends Repository
{
    protected $model;

    public function __construct(AccountContact $model)
    {
        $this->model = $model;
    }

    public function create(array $data = []): Model
    {
        return parent::create($data);
    }

    public function import(array $data = []): void
    {
        $this->model->create($data);
    }

    public function update(Model $model, array $data = []): Model
    {
        return parent::update($model, $data);
    }
}
