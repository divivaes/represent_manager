<?php

namespace App\Repositories\Admin\Traits;

use Carbon\Carbon;

trait GetStatisticsTrait
{
    public function getStatistics(array $data = [])
    {
        $output = [];

        $output['last_day'] = $this->prepareFilterQuery($data)
            ->where('created_at', '>=', Carbon::now()->subDays(1));
        $output['last_seven_days'] = $this->prepareFilterQuery($data)
            ->where('created_at', '>=', Carbon::now()->subDays(7));
        $output['last_thirty_days'] = $this->prepareFilterQuery($data)
            ->where('created_at', '>=', Carbon::now()->subDays(30));
        $output['total'] = $this->prepareFilterQuery($data);

        return $output;
    }
}
