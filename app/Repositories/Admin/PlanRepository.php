<?php

namespace App\Repositories\Admin;

use App\Models\Plan;
use App\Repositories\Admin\Traits\GetStatisticsTrait;
use Illuminate\Database\Eloquent\Model;

class PlanRepository extends Repository
{
    use GetStatisticsTrait;

    protected $model;

    public function __construct(Plan $model)
    {
        $this->model = $model;
    }

    public function getRecords(array $data = [])
    {
        $shows = $this->prepareFilterQuery($data);

        return $shows;
    }

    protected function prepareFilterQuery(array $data = [])
    {
        $records = $this->model->sort();

        return $records;
    }
}
