<?php

namespace App\Repositories\Admin;

use App\Models\Subscription;
use App\Repositories\Admin\Traits\GetStatisticsTrait;

class SubscriptionRepository extends Repository
{
    use GetStatisticsTrait;

    protected $model;

    public function __construct(Subscription $model)
    {
        $this->model = $model;
    }

    public function getRecords(array $data = [])
    {
        $shows = $this->prepareFilterQuery($data);

        return $shows;
    }

    protected function prepareFilterQuery(array $data = [])
    {
        $records = $this->model->sort();

        $records = $records->byAccount(isset($data['account_id']) ? $data['account_id'] : null);
        $records = $records->byUser(isset($data['user_id']) ? $data['user_id'] : null);
        $records = $records->byPlan(isset($data['plan_id']) ? $data['plan_id'] : null);
        $records = $records->byStatus(isset($data['status']) ? $data['status'] : null);

        return $records;
    }
}
