<?php


namespace App\Repositories\Admin;

use App\Models\Microsite;
use Illuminate\Database\Eloquent\Model;

class MicrositeRepository extends Repository
{

    protected $model;

    public function __construct(Microsite $model)
    {
        $this->model = $model;
    }

    public function create(array $data = []): Model
    {
        return parent::create($data);
    }

    public function update(Model $model, array $data = []): Model
    {
        return parent::update($model, $data);
    }

    public function getRecords(array $data = [])
    {
        $shows = $this->prepareFilterQuery($data);

        return $shows;
    }

    protected function prepareFilterQuery(array $data = [])
    {
        $records = $this->model->sort();
        $records = $records->byRole(isset($data['role']) ? $data['role'] : null);
        $records = $records->byIsActive(isset($data['is_active']) ? $data['is_active'] : null);

        return $records;
    }
}
