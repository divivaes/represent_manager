<?php

namespace App\Repositories\Admin;

use App\Models\PropertyVideoFile;

class PropertyVideoFileRepository extends Repository
{
    protected $model;

    public function __construct(PropertyVideoFile $model)
    {
        $this->model = $model;
    }
}
