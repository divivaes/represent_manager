<?php

namespace App\Repositories\Admin;

use App\Models\AccountTag;
use Illuminate\Database\Eloquent\Model;

class AccountTagRepository extends Repository
{
    protected $model;

    public function __construct(AccountTag $model)
    {
        $this->model = $model;
    }

    public function create(array $data = []): Model
    {
        return parent::create($data);
    }

    public function update(Model $model, array $data = []): Model
    {
        return parent::update($model, $data);
    }

    public function destroy(Model $model): bool
    {
        return parent::destroy($model);
    }

    public function getRecords()
    {
        $shows = $this->model->sort();

        return $shows;
    }
}
