<?php


namespace App\Repositories\Admin;

use App\Models\MicrositeJob;
use Illuminate\Database\Eloquent\Model;

class MicrositeJobsRepository extends Repository
{

    protected $model;

    public function __construct(MicrositeJob $model)
    {
        $this->model = $model;
    }

    public function create(array $data = []): Model
    {
        return parent::create($data);
    }

    public function update(Model $model, array $data = []): Model
    {
        return parent::update($model, $data);
    }

    public function getRecords(array $data = [])
    {
        $shows = $this->prepareFilterQuery($data);

        return $shows;
    }

    protected function prepareFilterQuery(array $data = [])
    {
        $records = $this->model;
        if (isset($data['user_id'])) {
            $records = $records->byUser($data['user_id']);
        }
        if (isset($data['microsite_id'])) {
            $records = $records->ByMicrosite($data['microsite_id']);
        }
        return $records;
    }
}
