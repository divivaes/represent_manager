<?php

namespace App\Repositories\Admin;

use App\Models\Account;
use App\Repositories\Admin\Traits\GetStatisticsTrait;

class AccountRepository extends Repository
{
    use GetStatisticsTrait;

    protected $model;

    public function __construct(Account $model)
    {
        $this->model = $model;
    }

    public function getRecords(array $data = [])
    {
        $shows = $this->prepareFilterQuery($data);

        return $shows;
    }

    protected function prepareFilterQuery(array $data = [])
    {
        $records = $this->model->sort();

        $records = $records->byName(isset($data['name']) ? $data['name'] : null);
        $records = $records->byUser(isset($data['user_id']) ? $data['user_id'] : null);
        $records = $records->byIsActive(isset($data['is_active']) ? $data['is_active'] : null);

        return $records;
    }

    public function micrositeSave($microsite)
    {
        $this->model->microsite()->save($microsite);
    }
}
