<?php

namespace App\Repositories\Admin;

use App\Models\AccountMarketingTemplate;
use Illuminate\Database\Eloquent\Model;

class AccountMarketingTemplateRepository extends Repository
{
    protected $model;

    public function __construct(AccountMarketingTemplate $model)
    {
        $this->model = $model;
    }

    public function create(array $data = []): Model
    {
        return parent::create($data);
    }

    public function update(Model $model, array $data = []): Model
    {
        return parent::update($model, $data);
    }
}
