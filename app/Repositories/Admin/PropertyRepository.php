<?php

namespace App\Repositories\Admin;

use App\Models\Property;
use App\Repositories\Admin\Traits\GetStatisticsTrait;

class PropertyRepository extends Repository
{
    use GetStatisticsTrait;

    protected $model;

    public function __construct(Property $model)
    {
        $this->model = $model;
    }

    public function getRecords(array $data = [])
    {
        $shows = $this->prepareFilterQuery($data);

        return $shows;
    }

    protected function prepareFilterQuery(array $data = [])
    {
        $records = $this->model->sort();

        $records = $records->byAddress(isset($data['address']) ? $data['address'] : null);
        $records = $records->byType(isset($data['type']) ? $data['type'] : null);
        $records = $records->byStatus(isset($data['status']) ? $data['status'] : null);
        $records = $records->byIsActive(isset($data['is_active']) ? $data['is_active'] : null);

        return $records;
    }

    public function micrositeSave($microsite)
    {
        $this->model->microsite()->save($microsite);
    }
}
