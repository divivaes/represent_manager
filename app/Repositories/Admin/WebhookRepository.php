<?php

namespace App\Repositories\Admin;

use App\Models\Webhook;
use App\Repositories\Admin\Traits\GetStatisticsTrait;

class WebhookRepository extends Repository
{
    use GetStatisticsTrait;

    protected $model;

    public function __construct(Webhook $model)
    {
        $this->model = $model;
    }

    public function getSources()
    {
        $sources = $this->model->select('source')->groupBy('source')->distinct()->orderBy('source', 'asc')->get();

        return $sources->pluck('source');
    }

    public function getTypes()
    {
        $sources = $this->model->select('type')->groupBy('type')->distinct()->orderBy('type', 'asc')->get();

        return $sources->pluck('type');
    }

    public function getRecords(array $data = [])
    {
        $shows = $this->prepareFilterQuery($data);

        return $shows;
    }

    protected function prepareFilterQuery(array $data = [])
    {
        $records = $this->model->sort();

        $records = $records->bySubscription(isset($data['subscription_id']) ? $data['subscription_id'] : null);
        $records = $records->bySource(isset($data['source']) ? $data['source'] : null);
        $records = $records->byType(isset($data['type']) ? $data['type'] : null);
        $records = $records->byPayload(isset($data['payload']) ? $data['payload'] : null);

        return $records;
    }
}
