<?php

namespace App\Repositories\Admin;

use App\Models\CrmNotification;
use Illuminate\Database\Eloquent\Model;

class CrmNotificationRepository extends Repository
{
    protected $model;

    public function __construct(CrmNotification $model)
    {
        $this->model = $model;
    }

    public function edit(Model $model, array $data = []): void
    {
        $model->fill($data);
        $model->save();
    }
}
