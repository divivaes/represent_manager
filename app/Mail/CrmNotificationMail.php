<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CrmNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $content;
    protected $title;

    public function __construct($content, $title)
    {
        $this->content = $content;
        $this->title = $title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->markdown('admin.account_marketing_templates.email.template')
            ->subject($this->title)
            ->with(['content' => $this->content]);
    }
}
