<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\UuidAble;
use App\Models\Scopes\ByMicrositeScope;
use App\Models\Scopes\ByStatusScope;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class MicrositeJob extends Model
{

    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use UuidAble;
    use ByStatusScope;
    use ByMicrositeScope;

    public const STATUS_WAITING = 'waiting';
    public const STATUS_PROCESSING = 'processing';
    public const STATUS_DONE = 'done';
    public const STATUS_ERROR = 'error';

    protected $fillable = [
        'user_id',
        'microsite_id',
        'delete_uuid',
        'delete_domain',
        'delete_type'
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function microsite()
    {
        return $this->belongsTo(Microsite::class, 'microsite_id');
    }
}
