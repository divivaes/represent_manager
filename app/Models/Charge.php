<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\GeoAble;
use App\Models\Boot\UuidAble;
use App\Models\Scopes\ByStatusScope;
use App\Models\Scopes\ByUserScope;
use App\Models\Scopes\ByAccountScope;
use App\Models\Scopes\SortScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Charge extends Model
{
    use SoftDeletes;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use UuidAble;
    use GeoAble;
    use SortScope;
    use ByUserScope;
    use ByStatusScope;
    use ByAccountScope;

    protected $fillable = [
        'account_id',
        'account_name',
        'user_id',
        'user_first_name',
        'user_last_name',
        'user_email',
        'plan_id',
        'plan_name',
        'start_at',
        'end_at',
        'price',
        'status',
        'braintree_charge_id',
        'braintree_charge_payload',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function scopeByPlan(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('plan_id', $data);
        }

        return $query;
    }

    public function getStartAtToDatetimeAttribute()
    {
        if ($this->start_at) {
            return Carbon::parse($this->start_at)->toRfc822String();
        }

        return null;
    }

    public function getEndAtToDatetimeAttribute()
    {
        if ($this->end_at) {
            return Carbon::parse($this->end_at)->toRfc822String();
        }

        return null;
    }

    public function getPriceToHumanAttribute()
    {
        if ($this->price) {
            return sprintf('$%s USD', $this->price);
        }

        return null;
    }
}
