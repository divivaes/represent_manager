<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\GeoAble;
use App\Models\Boot\UuidAble;
use App\Models\Scopes\ByAccountScope;
use App\Models\Scopes\ByStatusScope;
use App\Models\Scopes\ByUserScope;
use App\Models\Scopes\SortScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Subscription extends Model
{
    use SoftDeletes;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use UuidAble;
    use GeoAble;
    use SortScope;
    use ByStatusScope;
    use ByUserScope;
    use ByAccountScope;

    protected $fillable = [
        'account_id',
        'account_name',
        'user_id',
        'user_first_name',
        'user_last_name',
        'user_email',
        'plan_id',
        'plan_name',
        'status',
        'braintree_subscription_id',
        'braintree_subscription_plan_id',
        'braintree_subscription_price',
        'braintree_subscription_first_billing_date',
        'braintree_subscription_paid_through_date',
        'braintree_subscription_payload',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function transactions()
    {
        $transactions = Webhook::bySubscription($this->id)->sort()->get();

        return $transactions;
    }

    public function scopeByPlan(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('plan_id', $data);
        }

        return $query;
    }

    public function getStartAtToDatetimeAttribute()
    {
        if ($this->braintree_subscription_first_billing_date) {
            return Carbon::parse($this->braintree_subscription_first_billing_date)->toRfc822String();
        }

        return null;
    }

    public function getEndAtToDatetimeAttribute()
    {
        if ($this->braintree_subscription_paid_through_date) {
            return Carbon::parse($this->braintree_subscription_paid_through_date)->toRfc822String();
        }

        return null;
    }

    public function getPriceToHumanAttribute()
    {
        if ($this->braintree_subscription_price) {
            return sprintf('$%s USD', $this->braintree_subscription_price);
        }

        return null;
    }
}
