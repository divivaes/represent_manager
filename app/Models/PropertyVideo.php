<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\GeoAble;
use App\Models\Boot\UuidAble;
use App\Models\Scopes\ByTitleScope;
use App\Models\Scopes\ByTypeScope;
use App\Models\Scopes\SortScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class PropertyVideo extends Model
{
    use SoftDeletes;
    use UuidAble;
    use GeoAble;
    use SortScope;
    use ByTypeScope;
    use ByTitleScope;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;

    protected $fillable = [
        'user_id',
        'property_id',
        'title',
        'type',
        'job_status',
        'job_started_at',
        'job_ended_at',
        'job_notes',
    ];

    const TYPES = [
        '1' => 'Open house',
        '2' => 'Listing',
        '3' => 'Just sold',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function files()
    {
        return $this->hasMany(PropertyVideoFile::class);
    }

    public function getTypeToHumanAttribute()
    {
        if (isset(self::TYPES[$this->type])) {
            return self::TYPES[$this->type];
        }

        return null;
    }

    public function getJobStartedAtToDatetimeAttribute()
    {
        if ($this->job_started_at) {
            return Carbon::parse($this->job_started_at)->toRfc822String();
        }

        return null;
    }

    public function getJobEndedAtToDatetimeAttribute()
    {
        if ($this->job_ended_at) {
            return Carbon::parse($this->job_ended_at)->toRfc822String();
        }

        return null;
    }

    public function getUrlAviAttribute()
    {
        if ($this->path_avi) {
            return Storage::url($this->path_avi);
        }

        return null;
    }
}
