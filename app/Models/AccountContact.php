<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\FullNameAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\GeoAble;
use App\Models\Boot\UuidAble;
use App\Models\Scopes\SortScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountContact extends Model
{
    use SoftDeletes;
    use UuidAble;
    use FullNameAttribute;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use SortScope;
    use GeoAble;

    protected $fillable = [
        'account_id',
        'user_id',
        'first_name',
        'last_name',
        'phone',
        'email',
        'notes',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function tags()
    {
        return $this->morphToMany(AccountTag::class, 'taggable', 'taggables')
            ->withTimestamps()
            ->withPivot('user_id');
    }
}
