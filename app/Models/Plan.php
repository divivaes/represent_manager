<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\UuidAble;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Plan extends Model
{
    use SoftDeletes;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use UuidAble;

    protected $fillable = [
        'name',
        'price',
        'description',
        'features',
        'is_active',
        'braintree_plan_id',
    ];

    public function scopeSort(Builder $query) : Builder
    {
        return $query->orderBy('id', 'asc');
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function charges()
    {
        return $this->hasMany(Charge::class);
    }

    public function getPriceToHumanAttribute()
    {
        if ($this->price) {
            return sprintf('$%s USD', $this->price);
        }

        return null;
    }
}
