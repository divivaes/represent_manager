<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\UuidAble;
use Illuminate\Database\Eloquent\Model;

class Microsite extends Model
{

    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use UuidAble;

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function microsites()
    {
        return $this->morphTo();
    }

    public function jobs()
    {
        $this->hasMany(MicrositeJob::class);
    }
}
