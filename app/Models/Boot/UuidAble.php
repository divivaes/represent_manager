<?php

namespace App\Models\Boot;

use Webpatser\Uuid\Uuid;

trait UuidAble
{
    public static function bootUuidAble()
    {
        static::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }
}
