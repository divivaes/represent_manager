<?php

namespace App\Models\Boot;

use Illuminate\Support\Facades\Storage;
use PulkitJalan\GeoIP\GeoIP;
use Jenssegers\Agent\Agent;

trait GeoAble
{
    public static function bootGeoAble()
    {
        static::saving(function ($model) {
            $config = [
              'driver' => 'maxmind',
              'maxmind' => [
                'database' => storage_path() . '/GeoLite2-City.mmdb',
              ],
            ];
            $geoip = new GeoIP($config);
            $agent = new Agent();

            if (!$model->submitter_ip) {
                $model->submitter_ip = $geoip->getIp();
            }

            if (!$model->submitter_city) {
                $model->submitter_city = $geoip->get('city');
            }

            if (!$model->submitter_country) {
                $model->submitter_country = $geoip->get('country');
            }

            if (!$model->submitter_browser) {
                $model->submitter_browser = $agent->browser();
            }

            if (!$model->submitter_browser) {
                $model->submitter_browser = $agent->browser();
            }

            if (!$model->submitter_platform) {
                $model->submitter_platform = $agent->platform();
            }

            if (!$model->submitter_agent) {
                $model->submitter_agent = $agent->getUserAgent();
            }
        });
    }
}
