<?php

namespace App\Models\Boot;

use App\Services\Client\BraintreeService;

trait BraintreeAble
{
    public static function bootBraintreeAble()
    {
        static::saving(function ($model) {
            if (!$model->braintree_customer_id) {
                try {
                    $braintreeService = new BraintreeService();

                    $response = $braintreeService->gateway()->customer()->create([
                        'firstName' => $model->first_name,
                        'lastName' => $model->last_name,
                        'email' => $model->email,
                        'phone' => $model->phone,
                    ]);

                    if ($response->success) {
                        $model->braintree_customer_id = $response->customer->id;
                        $model->braintree_customer_payload = json_encode($response->customer);
                    }
                } catch (\Exception $exception) {
                    //
                }
            }
        });
    }
}
