<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\GeoAble;
use App\Models\Boot\UuidAble;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CrmNotification extends Model
{
    use SoftDeletes;
    use UuidAble;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use GeoAble;

    protected $fillable = [
        'contact_id', 'contact_email', 'template_id', 'template_type', 'template_title',
        'template_content', 'is_sent', 'sending_datetime'
    ];
}
