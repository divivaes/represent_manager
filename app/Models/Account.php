<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\GeoAble;
use App\Models\Boot\UuidAble;
use App\Models\Scopes\ByIsActiveScope;
use App\Models\Scopes\ByNameScope;
use App\Models\Scopes\ByUserScope;
use App\Models\Scopes\SortScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use SoftDeletes;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use UuidAble;
    use SortScope;
    use GeoAble;
    use ByNameScope;
    use ByIsActiveScope;

    protected $fillable = [
        'name',
        'is_active',
    ];

    public function scopeByUser(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->whereHas('users', function ($query) use ($data) {
                $query->where('account_user.user_id', $data);
            });
        }

        return $query;
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'account_user', 'account_id', 'user_id');
    }

    public function updates()
    {
        return $this->hasMany(AccountUpdate::class);
    }

    public function contacts()
    {
        return $this->hasMany(AccountContact::class);
    }

    public function marketingTemplates()
    {
        return $this->hasMany(AccountMarketingTemplate::class);
    }

    public function tags()
    {
        return $this->hasMany(AccountTag::class);
    }
    
    public function microsite()
    {
        return $this->morphOne(Microsite::class, 'micrositeable');
    }
}
