<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\UuidAble;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountUser extends Model
{
    use SoftDeletes;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use UuidAble;

    protected $table = 'account_user';

    protected $fillable = [
        'is_active',
        'role',
        'account_id',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
