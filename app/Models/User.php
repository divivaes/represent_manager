<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\FullNameAttribute;
use App\Models\Attributes\LastLoginAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\BraintreeAble;
use App\Models\Boot\UuidAble;
use App\Models\Boot\GeoAble;
use App\Models\Scopes\ByEmailScope;
use App\Models\Scopes\ByFullNameScope;
use App\Models\Scopes\ByIsActiveScope;
use App\Models\Scopes\ByPhoneScope;
use App\Models\Scopes\ByRoleScope;
use App\Models\Scopes\SortScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use LastLoginAtAttribute;
    use FullNameAttribute;
    use ByFullNameScope;
    use ByEmailScope;
    use ByPhoneScope;
    use ByRoleScope;
    use ByIsActiveScope;
    use SortScope;
    use UuidAble;
    use GeoAble;
    use BraintreeAble;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role',
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'is_active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            if (!$model->accounts()->count()) {
                $model->accounts()->create([
                    'name' => $model->full_name,
                ]);
            }
        });
    }

    public function scopeByAccount(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->whereHas('accounts', function ($query) use ($data) {
                $query->where('account_user.account_id', $data);
            });
        }

        return $query;
    }

    public function accounts()
    {
        return $this->belongsToMany(Account::class, 'account_user', 'user_id', 'account_id')
            ->withTimestamps();
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function charges()
    {
        return $this->hasMany(Charge::class);
    }

    public function providers()
    {
        return $this->hasMany(UserProvider::class);
    }

    public function providerFacebook()
    {
        return $this->providers()->where('provider_name', 'facebook')->first();
    }

    public function contacts()
    {
        return $this->hasMany(AccountContact::class);
    }

    public function marketingTemplates()
    {
        return $this->hasMany(AccountMarketingTemplate::class);
    }

    public function tags()
    {
        return $this->hasMany(AccountTag::class);
    }
}
