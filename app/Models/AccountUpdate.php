<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\UuidAble;
use App\Models\Boot\GeoAble;
use App\Models\Scopes\SortScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class AccountUpdate extends Model
{
    use SoftDeletes;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use UuidAble;
    use GeoAble;
    use SortScope;

    protected $fillable = [
        'account_id',
        'user_id',
        'title',
        'content',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
