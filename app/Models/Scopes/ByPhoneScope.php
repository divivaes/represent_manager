<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait ByPhoneScope
{
    public function scopeByPhone(Builder $query, $data = null): Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('phone', 'like', '%' . $data . '%');
        }

        return $query;
    }
}
