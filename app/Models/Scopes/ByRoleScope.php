<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait ByRoleScope
{
    public function scopeByRole(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('role', $data);
        }

        return $query;
    }
}
