<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait ByAccountScope
{
    public function scopeByAccount(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('account_id', $data);
        }

        return $query;
    }
}
