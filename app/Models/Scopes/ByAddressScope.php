<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait ByAddressScope
{
    public function scopeByAddress(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('address', 'like', '%' . $data . '%');
        }

        return $query;
    }
}
