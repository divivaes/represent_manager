<?php

namespace App\Models\Scopes;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

trait ByNameScope
{
    public function scopeByName(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where(function ($query) use ($data) {
                $query->where('name', 'like', '%' . $data . '%');
            });
        }

        return $query;
    }
}
