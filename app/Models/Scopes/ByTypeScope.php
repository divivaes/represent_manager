<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait ByTypeScope
{
    public function scopeByType(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('type', 'like', '%' . $data . '%');
        }

        return $query;
    }
}
