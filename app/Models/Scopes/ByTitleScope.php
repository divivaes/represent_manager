<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait ByTitleScope
{
    public function scopeByTitle(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('title', 'like', '%' . $data . '%');
        }

        return $query;
    }
}
