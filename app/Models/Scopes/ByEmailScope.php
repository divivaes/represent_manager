<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait ByEmailScope
{
    public function scopeByEmail(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('email', 'like', '%' . $data . '%');
        }

        return $query;
    }
}
