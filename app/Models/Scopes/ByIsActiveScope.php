<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait ByIsActiveScope
{
    public function scopeByIsActive(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data) && in_array($data, ['active', 'inactive'])) {
            $query = $query->where(
                'is_active',
                $data === 'active' ? 1 : 0
            );
        }

        return $query;
    }
}
