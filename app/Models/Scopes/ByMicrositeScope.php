<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait ByMicrositeScope
{
    public function scopeByMicrosite(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('microsite_id', $data);
        }
        return $query;
    }
}
