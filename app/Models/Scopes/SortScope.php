<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait SortScope
{
    public $sortColumn = 'id';
    public $sortDirection = 'desc';

    public function scopeSort(Builder $query) : Builder
    {
        return $query->orderBy($this->sortColumn, $this->sortDirection);
    }
}
