<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait ByStatusScope
{
    public function scopeByStatus(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('status', $data);
        }

        return $query;
    }
}
