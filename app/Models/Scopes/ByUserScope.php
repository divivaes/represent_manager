<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;

trait ByUserScope
{
    public function scopeByUser(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('user_id', $data);
        }

        return $query;
    }
}
