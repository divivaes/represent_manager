<?php

namespace App\Models\Scopes;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

trait ByFullNameScope
{
    public function scopeByFullName(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where(function ($query) use ($data) {
                $query->where('first_name', 'like', '%' . $data . '%');
                $query->orWhere('last_name', 'like', '%' . $data . '%');
                $query->orWhere(DB::raw('CONCAT(`first_name`, " ", `last_name`)'), 'like', '%' . $data . '%');
            });
        }

        return $query;
    }
}
