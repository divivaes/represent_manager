<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\GeoAble;
use App\Models\Boot\UuidAble;
use App\Models\Scopes\SortScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountTag extends Model
{
    use SortScope;
    use SoftDeletes;
    use UuidAble;
    use GeoAble;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;

    protected $fillable = [
        'account_id',
        'user_id',
        'title'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function contacts()
    {
        return $this->morphedByMany(AccountContact::class, 'taggable', 'taggables')
            ->withTimestamps()
            ->withPivot('user_id');
    }
}
