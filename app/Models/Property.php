<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\GeoAble;
use App\Models\Boot\UuidAble;
use App\Models\Scopes\ByAddressScope;
use App\Models\Scopes\ByIsActiveScope;
use App\Models\Scopes\ByStatusScope;
use App\Models\Scopes\ByTypeScope;
use App\Models\Scopes\SortScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use SoftDeletes;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use ByAddressScope;
    use ByTypeScope;
    use ByStatusScope;
    use ByIsActiveScope;
    use SortScope;
    use UuidAble;
    use GeoAble;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'user_id',
        'address',
        'type',
        'status',
        'price',
        'year_built',
        'beds',
        'baths',
        'square_feet',
        'lot_size',
        'description',
        'is_active',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function videos()
    {
        return $this->hasMany(PropertyVideo::class);
    }

    public function microsite()
    {
        return $this->morphOne(Microsite::class, 'micrositeable');
    }
}
