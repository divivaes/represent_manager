<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\GeoAble;
use App\Models\Boot\UuidAble;
use App\Models\Scopes\SortScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountMarketingTemplate extends Model
{
    use UuidAble;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use SortScope;
    use GeoAble;
    use SoftDeletes;

    protected $fillable = [
        'account_id',
        'user_id',
        'type',
        'title',
        'content'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
