<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Boot\UuidAble;
use App\Models\Scopes\SortScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Webhook extends Model
{
    use SoftDeletes;
    use UuidAble;
    use SortScope;
    use CreatedAtAttribute;

    protected $fillable = [
        'source',
        'type',
        'payload',
    ];

    protected $casts = [
        'payload' => 'array',
    ];

    public function scopeBySource(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('source', $data);
        }

        return $query;
    }

    public function scopeByType(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('type', $data);
        }

        return $query;
    }

    public function scopeByPayload(Builder $query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $query = $query->where('payload', 'like', sprintf('%%%s%%', $data));
        }

        return $query;
    }

    public function scopeBySubscription($query, $data = null) : Builder
    {
        if (mb_strlen($data)) {
            $subscription = Subscription::where('id', $data)->first();

            if ($subscription && $subscription->braintree_subscription_id) {
                $query = $query
                    ->bySource('braintree')
                    ->byType('subscription_charged_successfully')
                    ->where('payload->subject->subscription->id', $subscription->braintree_subscription_id);
            } else {
                $query = $query->where('id', -1);
            }
        }

        return $query;
    }
}
