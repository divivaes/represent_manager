<?php

namespace App\Models\Attributes;

use Carbon\Carbon;

trait CreatedAtAttribute
{
    public function getCreatedAtToDatetimeAttribute()
    {
        if ($this->created_at) {
            return Carbon::parse($this->created_at)->toRfc822String();
        }

        return null;
    }
}
