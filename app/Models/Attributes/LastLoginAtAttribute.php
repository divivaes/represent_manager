<?php

namespace App\Models\Attributes;

use Carbon\Carbon;

trait LastLoginAtAttribute
{
    public function getLastLoginAtToDatetimeAttribute()
    {
        if ($this->last_login_at) {
            return Carbon::parse($this->last_login_at)->toRfc822String();
        }

        return null;
    }
}
