<?php

namespace App\Models\Attributes;

use Carbon\Carbon;

trait UpdatedAtAttribute
{
    public function getUpdatedAtToDatetimeAttribute()
    {
        if ($this->updated_at) {
            return Carbon::parse($this->updated_at)->toRfc822String();
        }

        return null;
    }
}
