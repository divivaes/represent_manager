<?php

namespace App\Models\Attributes;

trait FullNameAttribute
{
    public function getFullNameAttribute()
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }
}
