<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\UuidAble;
use App\Models\Boot\GeoAble;
use App\Models\Scopes\ByEmailScope;
use App\Models\Scopes\ByFullNameScope;
use App\Models\Scopes\ByIsActiveScope;
use App\Models\Scopes\ByPhoneScope;
use App\Models\Scopes\ByRoleScope;
use App\Models\Scopes\ByUserScope;
use App\Models\Scopes\SortScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class UserProvider extends Model
{
    use SoftDeletes;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use UuidAble;
    use ByUserScope;
    use GeoAble;

    protected $fillable = [
        'user_id',
        'provider_name',
        'provider_id',
        'provider_token',
        'provider_token_expire_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
