<?php

namespace App\Models;

use App\Models\Attributes\CreatedAtAttribute;
use App\Models\Attributes\UpdatedAtAttribute;
use App\Models\Boot\GeoAble;
use App\Models\Boot\UuidAble;
use App\Models\Scopes\SortScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class PropertyVideoFile extends Model
{
    use UuidAble;
    use GeoAble;
    use SortScope;
    use CreatedAtAttribute;
    use UpdatedAtAttribute;
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'property_video_id',
        'path',
        'order_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function video()
    {
        return $this->belongsTo(PropertyVideo::class);
    }

    public function getUrlAttribute()
    {
        if ($this->path) {
            return Storage::url($this->path);
        }

        return null;
    }
}
