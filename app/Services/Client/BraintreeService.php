<?php

namespace App\Services\Client;

use Braintree\Gateway;
use Braintree\Configuration;

class BraintreeService
{
    public function gateway()
    {
        try {
            $configuration = new Configuration();

            $configuration->setEnvironment(config('braintree.env'));
            $configuration->setMerchantId(config('braintree.merchant_id'));
            $configuration->setPublicKey(config('braintree.public_key'));
            $configuration->setPrivateKey(config('braintree.private_key'));

            return new Gateway($configuration);
        } catch (\Exception $exception) {
            //
        }

        return null;
    }
}
