<?php

namespace App\Services\Client;

use App\Mail\ResetPasswordMail;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class TokenService
{
    public function createToken($email)
    {
        $old_token = DB::table('password_resets')->where('email', $email)->first();
        if ($old_token) {
            return $old_token->token;
        }

        $token = Str::random(60);
        $this->saveToken($token, $email);

        return $token;
    }

    public function saveToken($token, $email)
    {
        DB::table('password_resets')->insert([
            'email' => $email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
    }

    public function deleteToken($token)
    {
        DB::table('password_resets')->where('token', $token)->delete();
    }

    public function send($email)
    {
        $token = $this->createToken($email);
        Mail::to($email)->send(new ResetPasswordMail($token, $email));
    }
}
