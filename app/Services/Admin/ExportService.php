<?php

namespace App\Services\Admin;

use Illuminate\Database\Eloquent\Collection;
use Laracsv\Export;

class ExportService
{
    protected $export;

    public function __construct(Export $export)
    {
        $this->export = $export;
    }

    public function export(Collection $collection, array $fields, string $filename = null)
    {
        // Ability to change column value
        // $this->export->beforeEach(function ($collection) use ($fields) {
        //     foreach ($fields as $key => $field) {
        //         if (isset($field['type'])) {
        //             if ($field['type'] == 'boolean') {
        //                 $collection->{$key} = $collection->{$key} ? 'yes' : 'no';
        //             }
        //         }
        //     }
        // });

        $this->export->build($collection, collect($fields)->keys()->all())->download($filename);

        exit;
    }
}
