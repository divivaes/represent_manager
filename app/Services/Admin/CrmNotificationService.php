<?php

namespace App\Services\Admin;

use App\Mail\CrmNotificationMail;
use Illuminate\Support\Facades\Mail;

class CrmNotificationService
{
    public function send($data)
    {
        if ($data['template_type'] == 'email') {
            $this->sendEmail($data);
        } else {
            $this->sendSms($data);
        }
    }

    public function sendEmail($data)
    {
        Mail::to($data['contact_email'])
            ->send(new CrmNotificationMail($data['template_content'], $data['template_title']));
    }

    public function sendSms($data)
    {
        return $data;
    }
}
