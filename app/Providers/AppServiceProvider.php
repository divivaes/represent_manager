<?php

namespace App\Providers;

use App\Models\Microsite;
use App\Models\Property;
use App\Observers\MicrositeObserver;
use App\Observers\PropertyObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Microsite::observe(MicrositeObserver::class);
        Property::observe(PropertyObserver::class);
        url()->forceScheme(config('app.url_scheme'));
    }
}
