<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AccountRequest;
use App\Http\Requests\Admin\MicrositeRequest;
use App\Models\Account;
use App\Models\Microsite;
use App\Repositories\Admin\AccountRepository;
use App\Repositories\Admin\MicrositeJobsRepository;
use App\Repositories\Admin\UserRepository;
use App\Services\Admin\ExportService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    protected $repository;
    protected $userRepository;
    protected $exportService;

    public function __construct(
        AccountRepository $repository,
        UserRepository $userRepository,
        ExportService $exportService
    ) {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->exportService = $exportService;
    }

    public function index(Request $request)
    {
        if ($request->get('export')) {
            $this->exportService->export($this->repository->getRecords($request->all())->get(), [
                'id' => ['type' => 'string'],
            ]);
        }

        $records = $this->repository->getRecords($request->all())->paginate(30);
        $users = $this->userRepository->getRecords()->get();

        $statistics = $this->repository->getStatistics($request->all());

        return view('admin.accounts.index', compact('records', 'statistics', 'users'));
    }

    public function edit(Account $record)
    {
        return view('admin.accounts.form', compact('record'));
    }

    public function update(
        Request $request,
        Account $record,
        AccountRequest $accountRequest,
        MicrositeRequest $micrositesRequest,
        MicrositeJobsRepository $micrositeJobsRepository
    ) {
        $this->repository->update($record, $request->all());
        if ($record->microsite) {
            $microsite = $record->microsite;
        } else {
            $microsite = new Microsite;
        }
        if (empty($request->get('microsite'))) {
            if (isset($record->microsite->uuid) && $record->microsite->uuid) {
                $userID = is_null(Auth::user()) ? null : Auth::user()->id;
                $micrositeDomain = isset($record->microsite->domain)?$record->microsite->domain:null;
                $data = [
                    'user_id' => $userID,
                    'delete_uuid' => $record->microsite->uuid,
                    'delete_domain' => $micrositeDomain,
                    'delete_type' => 'account',
                    'status' => 'waiting'
                ];
                $micrositeJobsRepository->create($data);
                $record->microsite()->delete();
            }
        } else {
            $microsite->domain = $request->get('microsite');
            $microsite->is_active = $request->get('microsite_is_active');
            $microsite->user_id = is_null(Auth::user())?null:Auth::user()->id;
            $record->microsite()->save($microsite);
        }
        flash('The account has been successfully updated.', 'success');


        return redirect()->route('admin.accounts.index');
    }
}
