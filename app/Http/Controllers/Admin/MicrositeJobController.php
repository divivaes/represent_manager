<?php

namespace App\Http\Controllers\Admin;

use App\Models\Microsite;
use App\Models\MicrositeJob;
use App\Repositories\Admin\MicrositeJobsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MicrositeJobController extends Controller
{
    protected $repository;

    public function __construct(
        MicrositeJobsRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function add(Request $request)
    {
        $message = 'The site generation request has been sent successfully';
        $status = 'success';
        $siteId = $request->record;
        if (is_numeric($siteId)) {
            $microsite = Microsite::find($siteId);
            if ($microsite && isset($microsite->id)) {
                $isWaitingJobExists = $this->repository
                    ->getRecords(['microsite_id' => $siteId, 'status' => MicrositeJob::STATUS_WAITING])
                    ->limit(1)
                    ->get();

                if (count($isWaitingJobExists)) {
                    $message = 'There is already a pending request to generate this microsite';
                    $status = 'warning';
                } else {
                    $userID = is_null(Auth::user()) ? null : Auth::user()->id;
                    $data = [
                        'user_id' => $userID,
                        'microsite_id' => $microsite->id,
                        'status' => 'waiting'
                    ];
                    $this->repository->create($data);
                }
            } else {
                $message = 'The system cannot find such record';
            }
        } else {
            $message = "There is something wrong. Maybe this site doesn't exists";
            $status = 'error';
        }
        flash($message, $status);
        return redirect()->back();
    }
}
