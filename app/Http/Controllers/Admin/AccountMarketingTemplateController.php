<?php

namespace App\Http\Controllers\Admin;

use App\Models\Account;
use App\Models\AccountMarketingTemplate;
use App\Repositories\Admin\AccountMarketingTemplateRepository;
use App\Http\Requests\Admin\AccountMarketingTemplateRequest;
use App\Repositories\Admin\CrmNotificationRepository;
use Illuminate\Http\Request;

class AccountMarketingTemplateController extends Controller
{
    protected $repository;
    protected $crmNotificationRepository;

    public function __construct(
        AccountMarketingTemplateRepository $repository,
        CrmNotificationRepository $crmNotificationRepository
    ) {
        $this->repository = $repository;
        $this->crmNotificationRepository = $crmNotificationRepository;
    }

    public function index(Account $account, Request $request)
    {
        $records = $account->marketingTemplates()->sort()->paginate(30);
        
        return view('admin.account_marketing_templates.index', compact('records', 'account'));
    }

    public function create(Account $account)
    {
        $record = $this->repository->prepare();
        $users = $account->users()->get();

        return view('admin.account_marketing_templates.form', compact('record', 'users', 'account'));
    }

    public function store(Account $account, AccountMarketingTemplateRequest $request)
    {
        $data = $request->all();
        $data['account_id'] = $account->id;
        $data['user_id'] = $request->user_id;

        $this->repository->create($data);

        flash('The template has been successfully created.', 'success');

        return redirect()->route('admin.account_marketing_templates.index', $account);
    }

    public function edit(Account $account, AccountMarketingTemplate $record)
    {
        $users = $account->users()->get();

        return view('admin.account_marketing_templates.form', compact('record', 'users', 'account'));
    }

    public function update(Account $account, Request $request, AccountMarketingTemplate $record)
    {
        $this->repository->update($record, $request->all());

        flash('The template has been successfully updated.', 'success');

        return redirect()->route('admin.account_marketing_templates.index', $account);
    }

    public function destroy(Account $account, AccountMarketingTemplate $record)
    {
        $this->repository->destroy($record);

        flash('The template has been successfully deleted.', 'success');

        return redirect()->route('admin.account_marketing_templates.index', $account);
    }

    public function prepare(Account $account, AccountMarketingTemplate $record)
    {
        $contacts = $account->contacts()->get();
        $users = $account->users()->get();

        return view('admin.account_marketing_templates.send', compact('record', 'contacts', 'users', 'account'));
    }

    public function send(Account $account, AccountMarketingTemplate $record, AccountMarketingTemplateRequest $request)
    {
        $contacts = $account->contacts()->whereIn('account_id', $request->contacts)->get();

        foreach ($contacts as $contact) {
            $this->crmNotificationRepository->prepare()->create([
                'contact_id' => $contact->id,
                'contact_email' => $contact->email,
                'template_id' => $record->id,
                'template_type' => $request->type,
                'template_title' => $request->title,
                'template_content' => $request['content'],
                'sending_datetime' => $request->sending_datetime
            ]);
        }

        flash('The mail will be send to contacts on a listed date.', 'success');

        return redirect()->route('admin.account_marketing_templates.index', $account);
    }
}
