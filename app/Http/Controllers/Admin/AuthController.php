<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        return view('admin.auth.login');
    }

    public function authenticate(Request $request)
    {
        $data = [
            'email' => $request->get('email', ''),
            'password' => $request->get('password', ''),
            'role' => 'admin',
            'is_active' => true,
        ];

        if ($user = Auth::attempt($data)) {
            return redirect()->intended(route('admin.dashboard.index'));
        }

        flash()->error('The credentials you supplied were not correct or did not grant access to this resource.');

        return redirect(route('admin.auth.login'));
    }

    public function logout()
    {
        Auth::logout();

        return redirect(route('admin.auth.login'));
    }
}
