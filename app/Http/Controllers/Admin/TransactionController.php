<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Admin\WebhookRepository;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    protected $repository;
    protected $exportService;
    protected $braintreeService;

    public function __construct(WebhookRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $data = array_merge(
            $request->all(),
            ['source' => 'braintree', 'type' => 'subscription_charged_successfully']
        );

        $records = $this->repository->getRecords($data)->paginate(30);

        $statistics = $this->repository->getStatistics($data);

        return view(
            'admin.transactions.index',
            compact(
                'records',
                'statistics'
            )
        );
    }
}
