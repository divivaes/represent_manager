<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AccountTagRequest;
use App\Models\Account;
use App\Models\AccountTag;
use App\Repositories\Admin\AccountTagRepository;
use Illuminate\Http\Request;

class AccountTagController extends Controller
{
    protected $repository;

    public function __construct(AccountTagRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Account $account, Request $request)
    {
        $records = $account->tags()->sort()->paginate(30);

        return view('admin.account_tags.index', compact('records', 'account'));
    }

    public function create(Account $account)
    {
        $record = $this->repository->prepare();
        $users = $account->users()->get();

        return view('admin.account_tags.form', compact('record', 'account', 'users'));
    }

    public function store(Account $account, AccountTagRequest $request)
    {
        $data = $request->all();
        $data['account_id'] = $account->id;
        $data['user_id'] = $request->user_id;

        $this->repository->create($data);

        flash('The tag has been successfully created.', 'success');

        return redirect()->route('admin.account_tags.index', $account);
    }

    public function edit(Account $account, AccountTag $record)
    {
        $users = $account->users()->get();

        return view('admin.account_tags.form', compact('record', 'account', 'users'));
    }

    public function update(Account $account, Request $request, AccountTag $record)
    {
        $this->repository->update($record, $request->all());

        flash('The tag has been successfully updated.', 'success');

        return redirect()->route('admin.account_tags.index', $account);
    }

    public function destroy(Account $account, AccountTag $record)
    {
        $this->repository->destroy($record);

        flash('The tag has been successfully deleted.', 'success');

        return redirect()->route('admin.account_tags.index', $account);
    }
}
