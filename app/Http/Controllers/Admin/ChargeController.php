<?php

namespace App\Http\Controllers\Admin;

use App\Models\Charge;
use App\Repositories\Admin\ChargeRepository;
use App\Repositories\Admin\PlanRepository;
use App\Repositories\Admin\AccountRepository;
use App\Services\Admin\ExportService;
use App\Services\Client\BraintreeService;
use Illuminate\Http\Request;

class ChargeController extends Controller
{
    protected $repository;
    protected $accountRepository;
    protected $planRepository;
    protected $exportService;
    protected $braintreeService;

    public function __construct(
        ChargeRepository $repository,
        AccountRepository $accountRepository,
        PlanRepository $planRepository,
        ExportService $exportService,
        BraintreeService $braintreeService
    ) {
        $this->repository = $repository;
        $this->accountRepository = $accountRepository;
        $this->planRepository = $planRepository;
        $this->exportService = $exportService;
        $this->braintreeService = $braintreeService;
    }

    public function index(Request $request)
    {
        if ($request->get('export')) {
            $this->exportService->export($this->repository->getRecords($request->all())->get(), [
                'id' => ['type' => 'number'],
            ]);
        }

        $accounts = $this->accountRepository->getRecords()->get();
        $records = $this->repository->getRecords($request->all())->paginate(30);
        $statistics = $this->repository->getStatistics($request->all());
        $plans = $this->planRepository->getRecords()->get();

        return view(
            'admin.charges.index',
            compact('records', 'statistics', 'accounts', 'plans')
        );
    }

    public function edit(Charge $record)
    {
        return view('admin.charges.form', compact('record'));
    }

    public function pay(Charge $record, Request $request)
    {
        if ($record->status !== 'Intended' || !$record->user) {
            flash('The charge has been already paid.', 'error');

            return redirect()->route('admin.charges.index');
        }

        $data = [
            'paymentMethodNonce' => $request->get('nonce', null),
            'amount' => $record->price,
        ];

        $result = $this->braintreeService->gateway()->transaction()->sale($data);

        if ($result->success) {
            $charge = $result->transaction;

            $this->repository->update($record, [
                'status' => 'Authorized',
                'braintree_charge_id' => $charge->id,
                'braintree_charge_payload' => json_encode($charge),
            ]);
        } else {
            $errors = [];

            foreach ($result->errors->deepAll() as $error) {
                $errors[] = $error->message;
            }

            flash(implode('<br>', $errors), 'danger');
            return redirect()->route('admin.charges.edit', $record);
        }

        flash('The charge has been successfully updated.', 'success');
        return redirect()->route('admin.charges.index');
    }
}
