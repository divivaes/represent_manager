<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PlanRequest;
use App\Models\Plan;
use App\Repositories\Admin\PlanRepository;
use App\Services\Client\BraintreeService;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    protected $repository;
    protected $exportService;
    protected $braintreeService;

    public function __construct(PlanRepository $repository, BraintreeService $braintreeService)
    {
        $this->repository = $repository;
        $this->braintreeService = $braintreeService;
    }

    public function index(Request $request)
    {
        $records = $this->repository->getRecords($request->all())->paginate(30);

        return view('admin.plans.index', compact('records'));
    }

    public function edit(Plan $record)
    {
        $braintreePlans = $this->braintreeService->gateway()->plan()->all();

        return view('admin.plans.form', compact('record', 'braintreePlans'));
    }

    public function update(Request $request, PlanRequest $planrRequest, Plan $record)
    {
        $this->repository->update($record, $request->all());

        flash('The plan has been successfully updated.', 'success');

        return redirect()->route('admin.plans.index');
    }
}
