<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PaymentRequest;
use App\Models\Account;
use App\Models\Plan;
use App\Models\User;
use App\Repositories\Admin\AccountRepository;
use App\Repositories\Admin\ChargeRepository;
use App\Repositories\Admin\SubscriptionRepository;
use App\Repositories\Admin\PlanRepository;
use App\Repositories\Admin\UserRepository;
use App\Services\Admin\ExportService;
use App\Services\Client\BraintreeService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected $chargeRepository;
    protected $subscriptionRepository;
    protected $accountRepository;
    protected $userRepository;
    protected $planRepository;
    protected $exportService;
    protected $braintreeService;

    public function __construct(
        ChargeRepository $chargeRepository,
        SubscriptionRepository $subscriptionRepository,
        AccountRepository $accountRepository,
        UserRepository $userRepository,
        PlanRepository $planRepository,
        ExportService $exportService,
        BraintreeService $braintreeService
    ) {
        $this->chargeRepository = $chargeRepository;
        $this->subscriptionRepository = $subscriptionRepository;
        $this->accountRepository = $accountRepository;
        $this->userRepository = $userRepository;
        $this->planRepository = $planRepository;
        $this->exportService = $exportService;
        $this->braintreeService = $braintreeService;
    }

    public function create()
    {
        $accounts = $this->accountRepository->getRecords()->get();
        $users = $this->userRepository->getRecords(['role' => 'user'])->get();
        $plans = $this->planRepository->getRecords()->get();
        $braintreePlans = $this->braintreeService->gateway()->plan()->all();

        return view(
            'admin.payments.form',
            compact('accounts', 'users', 'plans', 'braintreePlans')
        );
    }

    public function store(Request $request, PaymentRequest $paymentRequest)
    {
        $data = [
            'account_user' => $request->get('account_user', null),
            'plan_id' => $request->get('plan_id', null),
            'charge' => $request->get('charge', null),
            'charge_start_at' => $request->get('charge_start_at', null),
            'charge_end_at' => $request->get('charge_end_at', null),
            'charge_price' => $request->get('charge_price', null),
            'subscription' => $request->get('subscription', null),
            'subscription_plan_id' => $request->get('subscription_plan_id', null),
            'subscription_price' => $request->get('subscription_price', null),
            'subscription_start_at' => $request->get('subscription_start_at', null),
        ];

        list($accountId, $userId) = explode('_', $data['account_user']);

        $account = Account::where('id', $accountId)->firstOrFail();
        $user = User::whereHas('accounts', function ($query) use ($userId, $accountId) {
            $query->where('user_id', $userId);
            $query->where('account_id', $accountId);
        })->firstOrFail();

        $plan = Plan::where('id', $data['plan_id'])->firstOrFail();

        if ($data['charge']) {
            $this->chargeRepository->create([
                'account_id' => $account->id,
                'account_name' => $account->name,

                'user_id' => $user->id,
                'user_first_name' => $user->first_name,
                'user_last_name' => $user->last_name,
                'user_email' => $user->email,

                'plan_id' => $plan->id,
                'plan_name' => $plan->name,

                'start_at' => Carbon::parse($data['charge_start_at'])->toDateTimeString(),
                'end_at' => Carbon::parse($data['charge_end_at'])->toDateTimeString(),

                'price' => $data['charge_price'],

                'status' => 'Intended',
            ]);
        }

        if ($data['subscription']) {
            if (!is_null($data['subscription_start_at'])) {
                $data['subscription_start_at'] = Carbon::parse($data['subscription_start_at'])->toDateTimeString();
            }

            $this->subscriptionRepository->create([
                'account_id' => $account->id,
                'account_name' => $account->name,

                'user_id' => $user->id,
                'user_first_name' => $user->first_name,
                'user_last_name' => $user->last_name,
                'user_email' => $user->email,

                'plan_id' => $plan->id,
                'plan_name' => $plan->name,

                'braintree_subscription_plan_id' => $data['subscription_plan_id'],
                'braintree_subscription_price' => $data['subscription_price'],
                'braintree_subscription_first_billing_date' => $data['subscription_start_at'],

                'status' => 'Intended',
            ]);
        }

        flash('The payment has been successfully created.', 'success');

        return redirect()->route('admin.subscriptions.index');
    }
}
