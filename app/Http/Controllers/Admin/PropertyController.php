<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\MicrositeRequest;
use App\Http\Requests\Admin\PropertyRequest;
use App\Models\Account;
use App\Models\Property;
use App\Models\User;
use App\Repositories\Admin\AccountRepository;
use App\Models\Microsite;
use App\Repositories\Admin\MicrositeJobsRepository;
use App\Repositories\Admin\PropertyRepository;
use App\Services\Admin\ExportService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PropertyController extends Controller
{
    protected $repository;
    protected $exportService;
    protected $accountRepository;

    public function __construct(
        PropertyRepository $repository,
        ExportService $exportService,
        AccountRepository $accountRepository
    ) {
        $this->repository = $repository;
        $this->exportService = $exportService;
        $this->accountRepository = $accountRepository;
    }

    public function index(Request $request)
    {
        if ($request->get('export')) {
            $this->exportService->export($this->repository->getRecords($request->all())->get(), [
                'address' => ['type' => 'string'],
                'type' => ['type' => 'string'],
                'status' => ['type' => 'string'],
                'price' => ['type' => 'string'],
                'year_built' => ['type' => 'string'],
                'beds' => ['type' => 'string'],
                'baths' => ['type' => 'string'],
                'square_feet' => ['type' => 'string'],
                'lot_size' => ['type' => 'string'],
                'is_active' => ['type' => 'boolean'],
                'created_at' => ['type' => 'datetime'],
            ]);
        }

        $records = $this->repository->getRecords($request->all())->paginate(30);

        $statistics = $this->repository->getStatistics($request->all());

        return view('admin.properties.index', compact('records', 'statistics'));
    }

    public function create()
    {
        $record = $this->repository->prepare();
        $accounts = $this->accountRepository->getRecords()->get();

        return view('admin.properties.form', compact('record', 'accounts'));
    }

    public function store(PropertyRequest $request, MicrositeRequest $micrositeRequest)
    {
        list($accountId, $userId) = explode('_', $request->get('account_user', null));

        $account = Account::where('id', $accountId)->firstOrFail();
        $user = User::whereHas('accounts', function ($query) use ($userId, $accountId) {
            $query->where('user_id', $userId);
            $query->where('account_id', $accountId);
        })->firstOrFail();

        $this->repository->create(
            array_merge($request->all(), ['account_id' => $account->id, 'user_id' => $user->id])
        );

        if (! empty($request->get('microsite'))) {
            $microsite = new Microsite;
            $microsite->domain = $request->get('microsite');
            $microsite->is_active = $request->get('microsite_is_active');
            $microsite->user_id = is_null(Auth::user())?null:Auth::user()->id;
            $this->repository->micrositeSave($microsite);
        }

        flash('The property has been successfully created.', 'success');

        return redirect()->route('admin.properties.index');
    }

    public function edit(Property $record)
    {
        $accounts = $this->accountRepository->getRecords()->get();

        return view('admin.properties.form', compact('record', 'accounts'));
    }

    public function update(
        PropertyRequest $request,
        Property $record,
        MicrositeRequest $micrositesRequest,
        MicrositeJobsRepository $micrositeJobsRepository
    ) {
        list($accountId, $userId) = explode('_', $request->get('account_user', null));

        $account = Account::where('id', $accountId)->firstOrFail();
        $user = User::whereHas('accounts', function ($query) use ($userId, $accountId) {
            $query->where('user_id', $userId);
            $query->where('account_id', $accountId);
        })->firstOrFail();

        $this->repository->update(
            $record,
            array_merge($request->all(), ['account_id' => $account->id, 'user_id' => $user->id])
        );

        if ($record->microsite) {
            $microsite = $record->microsite;
        } else {
            $microsite = new Microsite;
        }

        if (count($request->all()) !== 3) {
            if (empty($request->get('microsite'))) {
                if (isset($record->microsite->uuid) && $record->microsite->uuid) {
                    $userID = is_null(Auth::user()) ? null : Auth::user()->id;
                    $micrositeDomain = isset($record->microsite->domain) ? $record->microsite->domain : null;
                    $data = [
                        'user_id' => $userID,
                        'delete_uuid' => $record->microsite->uuid,
                        'delete_domain' => $micrositeDomain,
                        'delete_type' => 'property',
                        'status' => 'waiting'
                    ];
                    $micrositeJobsRepository->create($data);
                    $record->microsite()->delete();
                }
            } else {
                $microsite->domain = $request->get('microsite');
                $microsite->is_active = $request->get('microsite_is_active');
                $microsite->user_id = is_null(Auth::user()) ? null : Auth::user()->id;
                $record->microsite()->save($microsite);
            }
        }

        flash('The property has been successfully updated.', 'success');

        return redirect()->route('admin.properties.index');
    }

    public function updateStatus(PropertyRequest $request, Property $record)
    {
        $this->repository->update($record, $request->all());

        flash('The property has been successfully updated.', 'success');

        return redirect()->route('admin.properties.index');
    }

    public function destroy(
        Property $record,
        MicrositeJobsRepository $micrositeJobsRepository
    ) {
        if (isset($record->microsite->uuid)) {
            $userID = is_null(Auth::user()) ? null : Auth::user()->id;
            $micrositeDomain = isset($record->microsite->domain) ? $record->microsite->domain : null;
            $data = [
                'user_id' => $userID,
                'delete_uuid' => $record->microsite->uuid,
                'delete_domain' => $micrositeDomain,
                'status' => 'waiting'
            ];
            $micrositeJobsRepository->create($data);
            $record->microsite()->delete();
        }
        $this->repository->destroy($record);

        flash('The property has been successfully deleted.', 'success');
        return redirect()->route('admin.properties.index');
    }
}
