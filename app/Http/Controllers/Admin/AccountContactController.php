<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AccountContactRequest;
use App\Models\Account;
use App\Models\AccountTag;
use App\Models\User;
use App\Models\AccountContact;
use App\Repositories\Admin\AccountContactRepository;
use App\Repositories\Admin\AccountContactTagRepository;
use App\Repositories\Admin\AccountTagRepository;
use App\Services\Admin\ImportService;
use Illuminate\Http\Request;

class AccountContactController extends Controller
{
    protected $repository;
    protected $service;
    protected $accountTagRepository;

    public function __construct(
        AccountContactRepository $repository,
        ImportService $service,
        AccountTagRepository $accountTagRepository
    ) {
        $this->repository = $repository;
        $this->service = $service;
        $this->accountTagRepository = $accountTagRepository;
    }

    public function index(Account $account, Request $request)
    {
        $records = $account->contacts()->sort()->paginate(30);

        return view('admin.account_contacts.index', compact('records', 'account'));
    }

    public function create(Account $account)
    {
        $record = $this->repository->prepare();
        $users = $account->users()->get();
        $tags = $account->tags()->get();

        return view('admin.account_contacts.form', compact('record', 'account', 'users', 'tags'));
    }

    public function store(Account $account, AccountContactRequest $request)
    {
        $data = $request->all();
        $data['account_id'] = $account->id;
        $data['user_id'] = $request->user_id;

        $contact = $this->repository->create($data);

        $tags = $this->accountTagRepository->getRecords()->whereIn('id', $request->tag_ids)->get();

        if (!is_null($tags)) {
            foreach ($tags as $tag) {
                $contact->tags()->attach($tag, ['user_id' => $contact->user_id]);
            }
        }

        flash('The contact has been successfully created.', 'success');

        return redirect()->route('admin.account_contacts.index', $account);
    }

    public function edit(Account $account, AccountContact $record)
    {
        $users = $account->users()->get();
        $tags = $account->tags()->get();

        return view('admin.account_contacts.form', compact('record', 'account', 'users', 'tags'));
    }

    public function update(Account $account, Request $request, AccountContact $record)
    {
        $this->repository->update($record, $request->all());

        $record->tags()->detach();

        if (!is_null($request->tag_ids)) {
            $tags = $this->accountTagRepository->getRecords()->whereIn('id', $request->tag_ids)->get();

            if (!is_null($tags)) {
                foreach ($tags as $tag) {
                    $record->tags()->attach($tag, ['user_id' => $record->user_id]);
                }
            }
        }

//        $record->tags()->sync($request->tag_ids);

        flash('The contact has been successfully updated.', 'success');

        return redirect()->route('admin.account_contacts.index', $account);
    }

    public function destroy(Account $account, AccountContact $record)
    {
        $this->repository->destroy($record);

        flash('The contact has been successfully deleted.', 'success');

        return redirect()->route('admin.account_contacts.index', $account);
    }

    public function import(Account $account, Request $request)
    {
        if ($request->hasFile('import')) {
            $this->validate($request, [
                'file' => 'file|max:10240|mimes:csv,text/csv'
            ]);

            $file = $request->import->getRealPath();

            $data = $this->service->csvToArray($file);

            for ($i = 0; $i < count($data); $i++) {
                $this->repository->import($data[$i]);
            }

            flash('File imported successfully.', 'success');
        } else {
            flash('Please upload file.', 'danger');
        }

        return redirect()->route('admin.account_contacts.index', $account);
    }
}
