<?php

namespace App\Http\Controllers\Admin;

use App\Models\Subscription;
use App\Repositories\Admin\AccountRepository;
use App\Repositories\Admin\PlanRepository;
use App\Repositories\Admin\SubscriptionRepository;
use App\Services\Admin\ExportService;
use App\Services\Client\BraintreeService;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    protected $repository;
    protected $accountRepository;
    protected $planRepository;
    protected $exportService;
    protected $braintreeService;

    public function __construct(
        SubscriptionRepository $repository,
        AccountRepository $accountRepository,
        PlanRepository $planRepository,
        ExportService $exportService,
        BraintreeService $braintreeService
    ) {
        $this->repository = $repository;
        $this->accountRepository = $accountRepository;
        $this->planRepository = $planRepository;
        $this->exportService = $exportService;
        $this->braintreeService = $braintreeService;
    }

    public function index(Request $request)
    {
        if ($request->get('export')) {
            $this->exportService->export($this->repository->getRecords($request->all())->get(), [
                'id' => ['type' => 'number'],
            ]);
        }

        $accounts = $this->accountRepository->getRecords()->get();
        $records = $this->repository->getRecords($request->all())->paginate(30);
        $statistics = $this->repository->getStatistics($request->all());
        $plans = $this->planRepository->getRecords()->get();

        return view(
            'admin.subscriptions.index',
            compact('records', 'statistics', 'accounts', 'plans')
        );
    }

    public function edit(Subscription $record)
    {
        return view('admin.subscriptions.form', compact('record'));
    }

    public function pay(Subscription $record, Request $request)
    {
        if ($record->status !== 'Intended') {
            flash('The subscription has been already paid.', 'error');

            return redirect()->route('admin.subscriptions.index');
        }

        $data = [
            'paymentMethodNonce' => $request->get('nonce', null),
            'price' => $record->braintree_subscription_price,
            'planId' => $record->braintree_subscription_plan_id,
        ];

        if ($record->braintree_subscription_first_billing_date) {
            $data['firstBillingDate'] = $record->braintree_subscription_first_billing_date;
        }

        $result = $this->braintreeService->gateway()->subscription()->create($data);

        if ($result->success) {
            $subscription = $result->subscription;

            $this->repository->update($record, [
                'status' => $subscription->status,
                'braintree_subscription_id' => $subscription->id,
                'braintree_subscription_plan_id' => $subscription->planId,
                'braintree_subscription_price' => $subscription->price,
                'braintree_subscription_first_billing_date' => $subscription->firstBillingDate,
                'braintree_subscription_paid_through_date' => $subscription->paidThroughDate,
                'braintree_subscription_payload' => json_encode($subscription),
            ]);
        } else {
            $errors = [];

            foreach ($result->errors->deepAll() as $error) {
                $errors[] = $error->message;
            }

            flash(implode('<br>', $errors), 'danger');
            return redirect()->route('admin.subscriptions.edit', $record);
        }

        flash('The subscription has been successfully updated.', 'success');
        return redirect()->route('admin.subscriptions.index');
    }
}
