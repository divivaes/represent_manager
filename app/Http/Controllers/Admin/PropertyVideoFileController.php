<?php

namespace App\Http\Controllers\Admin;

use App\Models\Property;
use App\Models\PropertyVideo;
use App\Models\PropertyVideoFile;
use App\Models\User;
use App\Repositories\Admin\PropertyVideoFileRepository;
use App\Http\Requests\Admin\PropertyVideoFileRequest;

class PropertyVideoFileController extends Controller
{
    protected $repository;
    protected $imageRepository;

    public function __construct(PropertyVideoFileRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Property $property, PropertyVideo $video)
    {
        $records = $video->files()->sort()->get();

        return view(
            'admin.property_video_files.index',
            compact('records', 'property', 'video')
        );
    }

    public function create(Property $property, PropertyVideo $video)
    {
        $record = $this->repository->prepare();

        return view(
            'admin.property_video_files.form',
            compact('record', 'property', 'video')
        );
    }

    public function store(PropertyVideoFileRequest $request, Property $property, PropertyVideo $video)
    {
        $userId = $request->get('user_id', null);
        $accountId = $property->account->id;

        User::whereHas('accounts', function ($query) use ($userId, $accountId) {
            $query->where('user_id', $userId);
            $query->where('account_id', $accountId);
        })->firstOrFail();

        $path = null;
        $upload = $request->file('file');

        if (!is_null($upload)) {
            $path = $upload->store(
                sprintf('properties/%s/videos/%s/files', $property->uuid, $video->uuid)
            );
        }

        $this->repository->create(array_merge($request->all(), ['property_video_id' => $video->id, 'path' => $path]));

        flash('The file has been successfully created.', 'success');

        return redirect()->route('admin.property_video_files.index', [$property, $video]);
    }

    public function destroy(Property $property, PropertyVideo $video, PropertyVideoFile $record)
    {
        $this->repository->destroy($record);

        flash('The file has been successfully deleted.', 'success');

        return redirect()->route('admin.property_video_files.index', [$property, $video]);
    }
}
