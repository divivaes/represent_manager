<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\UserRequest;
use App\Models\User;
use App\Repositories\Admin\AccountRepository;
use App\Repositories\Admin\UserRepository;
use App\Services\Admin\ExportService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $repository;
    protected $accountRepository;
    protected $exportService;

    public function __construct(
        UserRepository $repository,
        AccountRepository $accountRepository,
        ExportService $exportService
    ) {
        $this->repository = $repository;
        $this->accountRepository = $accountRepository;
        $this->exportService = $exportService;
    }

    public function index(Request $request)
    {
        if ($request->get('export')) {
            $this->exportService->export($this->repository->getRecords($request->all())->get(), [
                'first_name' => ['type' => 'string'],
                'last_name' => ['type' => 'string'],
                'email' => ['type' => 'string'],
                'role' => ['type' => 'string'],
                'is_active' => ['type' => 'boolean'],
                'created_at' => ['type' => 'datetime'],
                'last_login_at' => ['type' => 'datetime'],
            ]);
        }

        $records = $this->repository->getRecords($request->all())->paginate(30);
        $accounts = $this->accountRepository->getRecords()->get();

        $statistics = $this->repository->getStatistics($request->all());

        return view('admin.users.index', compact('records', 'statistics', 'accounts'));
    }

    public function create()
    {
        $record = $this->repository->prepare();

        return view('admin.users.form', compact('record'));
    }

    public function store(Request $request, UserRequest $userRequest)
    {
        $this->repository->create($request->all());

        flash('The user has been successfully created.', 'success');

        return redirect()->route('admin.users.index');
    }

    public function edit(User $record)
    {
        return view('admin.users.form', compact('record'));
    }

    public function update(Request $request, UserRequest $userRequest, User $record)
    {
        $this->repository->update($record, $request->all());

        flash('The user has been successfully updated.', 'success');

        return redirect()->route('admin.users.index');
    }

    public function destroy(User $record)
    {
        $this->repository->destroy($record);

        flash('The user has been successfully deleted.', 'success');

        return redirect()->route('admin.users.index');
    }
}
