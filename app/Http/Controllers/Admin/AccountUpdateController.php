<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AccountUpdateRequest;
use App\Models\Account;
use App\Models\AccountUpdate;
use App\Repositories\Admin\UserRepository;
use App\Repositories\Admin\AccountUpdateRepository;
use Illuminate\Http\Request;

class AccountUpdateController extends Controller
{
    protected $repository;
    protected $userRepository;

    public function __construct(AccountUpdateRepository $repository, UserRepository $userRepository)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
    }

    public function index(Account $account, Request $request)
    {
        $records = $account->updates()->sort()->paginate(30);

        $statistics = $this->repository->getStatistics($request->all());

        return view('admin.account_updates.index', compact('account', 'records', 'statistics'));
    }

    public function create(Account $account)
    {
        $record = $this->repository->prepare();
        $users = $account->users()->get();

        return view('admin.account_updates.form', compact('account', 'record', 'users'));
    }

    public function store(Account $account, Request $request, AccountUpdateRequest $accountUpdateRequest)
    {
        $data = $request->all();
        $data['account_id'] = $account->id;

        $this->repository->create($data);

        flash('The update has been successfully created.', 'success');

        return redirect()->route('admin.account_updates.index', $account);
    }

    public function edit(Account $account, AccountUpdate $record)
    {
        $users = $account->users()->get();

        return view('admin.account_updates.form', compact('account', 'record', 'users'));
    }

    public function update(
        Account $account,
        AccountUpdate $record,
        Request $request,
        AccountUpdateRequest $accountUpdateRequest
    ) {
        $this->repository->update($record, $request->all());

        flash('The update has been successfully updated.', 'success');

        return redirect()->route('admin.account_updates.index', $account);
    }

    public function destroy(Account $account, AccountUpdate $record)
    {
        $this->repository->destroy($record);

        flash('The update has been successfully deleted.', 'success');

        return redirect()->route('admin.account_updates.index', $account);
    }
}
