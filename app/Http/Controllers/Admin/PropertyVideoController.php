<?php

namespace App\Http\Controllers\Admin;

use App\Models\Property;
use App\Models\PropertyVideo;
use App\Models\User;
use App\Repositories\Admin\PropertyVideoRepository;
use App\Http\Requests\Admin\PropertyVideoRequest;
use Carbon\Carbon;

class PropertyVideoController extends Controller
{
    protected $repository;
    protected $imageRepository;

    public function __construct(PropertyVideoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Property $property)
    {
        $records = $property->videos()->sort()->paginate(30);

        return view('admin.property_videos.index', compact('records', 'property'));
    }

    public function create(Property $property)
    {
        $record = $this->repository->prepare();

        return view('admin.property_videos.form', compact('record', 'property'));
    }

    public function store(PropertyVideoRequest $request, Property $property)
    {
        $userId = $request->get('user_id', null);
        $accountId = $property->account->id;

        User::whereHas('accounts', function ($query) use ($userId, $accountId) {
            $query->where('user_id', $userId);
            $query->where('account_id', $accountId);
        })->firstOrFail();

        $this->repository->create(array_merge($request->all(), ['property_id' => $property->id]));

        flash('The video has been successfully created.', 'success');

        return redirect()->route('admin.property_videos.index', $property);
    }

    public function edit(Property $property, PropertyVideo $record)
    {
        return view('admin.property_videos.form', compact('record', 'property'));
    }

    public function update(PropertyVideoRequest $request, Property $property, PropertyVideo $record)
    {
        $userId = $request->get('user_id', null);
        $accountId = $property->account->id;

        User::whereHas('accounts', function ($query) use ($userId, $accountId) {
            $query->where('user_id', $userId);
            $query->where('account_id', $accountId);
        })->firstOrFail();

        $this->repository->update($record, $request->all());

        flash('The video has been successfully updated.', 'success');

        return redirect()->route('admin.property_videos.index', $property);
    }

    public function generate(Property $property, PropertyVideo $record)
    {
        if ($record->files()->count() < 5) {
            flash('The minimum for this type of videos is 5 files.', 'danger');
        } else {
            $this->repository->update($record, [
                'job_status' => 'pending',
                'job_started_at' => null,
                'job_ended_at' => null,
                'job_notes' => null,
            ]);

            flash('The video has been successfully queued.', 'success');
        }

        return redirect()->route('admin.property_videos.index', $property);
    }

    public function destroy(Property $property, PropertyVideo $record)
    {
        $this->repository->destroy($record);

        flash('The video has been successfully deleted.', 'success');

        return redirect()->route('admin.property_videos.index', $property);
    }
}
