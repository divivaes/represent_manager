<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Admin\WebhookRepository;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    protected $repository;
    protected $exportService;
    protected $braintreeService;

    public function __construct(WebhookRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $records = $this->repository->getRecords($request->all())->paginate(30);

        $statistics = $this->repository->getStatistics($request->all());

        $sources = $this->repository->getSources();

        $types = $this->repository->getTypes();

        return view(
            'admin.webhooks.index',
            compact(
                'records',
                'statistics',
                'sources',
                'types'
            )
        );
    }
}
