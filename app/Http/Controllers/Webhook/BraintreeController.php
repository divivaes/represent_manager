<?php

namespace App\Http\Controllers\Webhook;

use App\Repositories\Admin\WebhookRepository;
use App\Services\Client\BraintreeService;
use Illuminate\Http\Request;

class BraintreeController extends Controller
{
    protected $braintreeService;
    protected $webhookRepository;

    public function __construct(BraintreeService $braintreeService, WebhookRepository $webhookRepository)
    {
        $this->braintreeService = $braintreeService;
        $this->webhookRepository = $webhookRepository;
    }

    public function store(Request $request)
    {
        $signature = $request->get('bt_signature', null);
        $payload = $request->get('bt_payload', null);

        if (!is_null($signature) && !is_null($payload)) {
            try {
                $result = $this->braintreeService->gateway()->webhookNotification()->parse($signature, $payload);

                $toJson = json_encode($result);

                $this->webhookRepository->create([
                    'source' => 'braintree',
                    'type' => $result->kind,
                    'payload' => is_string($toJson) ? json_decode($toJson, true) : [],
                ]);
            } catch (\Exception $exception) {
                print_r($exception->getMessage());
            }
        }

        return '';
    }
}
