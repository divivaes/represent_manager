<?php

namespace App\Http\Controllers\Client;

use Laravel\Socialite\Facades\Socialite;
use App\Http\Requests\Client\Auth\SignInRequest;
use App\Http\Requests\Client\Auth\SignUpRequest;
use App\Repositories\Client\AccountRepository;
use App\Repositories\Client\AccountUserRepository;
use App\Repositories\Client\UserRepository;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $repository;
    protected $accountUserRepository;
    protected $accountRepository;
    protected $redirectRoute = "/cabinet";

    public function __construct(
        UserRepository $repository,
        AccountUserRepository $accountUserRepository,
        AccountRepository $accountRepository
    ) {
        $this->repository = $repository;
        $this->accountUserRepository = $accountUserRepository;
        $this->accountRepository = $accountRepository;
    }

    public function signup()
    {
        return view('client.auth.signup');
    }

    public function signin()
    {
        return view('client.auth.signin');
    }

    public function register(SignUpRequest $request)
    {
        $user = $this->repository->create($request->all());

        $account = $this->accountRepository->create([]);

        $this->accountUserRepository->create(['user_id' => $user->id, 'account_id' => $account->id]);

        Auth::login($user);

        return redirect($this->redirectRoute);
    }

    public function login(SignInRequest $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if ($user = Auth::attempt($data)) {
            return redirect()->intended(route('client.cabinet.index'));
        }

        flash()->error('The credentials you supplied were not correct or did not grant access to this resource.');

        return redirect(route('client.auth.signin'));
    }

    public function logout()
    {
        Auth::logout();

        return redirect(route('client.auth.signin'));
    }
}
