<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        return view('client.pages.index');
    }

    public function pricing()
    {
        return view('client.pages.pricing');
    }
}
