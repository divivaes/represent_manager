<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\Client\Account\EditAccountRequest;
use App\Repositories\Client\UserProviderRepository;
use App\Repositories\Client\UserRepository;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{

    protected $repository;
    protected $accountUserRepository;
    protected $accountRepository;

    public function __construct(
        UserRepository $repository
    ) {
        $this->repository = $repository;
    }

    public function edit()
    {
        $user = Auth::user();
        return view('client.account.edit', compact('user'));
    }

    public function update(EditAccountRequest $request)
    {
        if (!is_null(Auth::user())) {
            $userModel = $this->repository->getByEmail(Auth::user()->email);
            $this->repository->update($userModel, $request->all());
            flash()->info('Your data has been successfully updated');
            return redirect(route('client.account.edit'));
        }
        return redirect(route('client.auth.signin'));
    }
}
