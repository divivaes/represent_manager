<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CabinetController extends Controller
{
    public function index()
    {
        $data = [];

        if (!is_null(Auth::user()) && Auth::user()->providerFacebook()) {
            try {
                $fb = new \Facebook\Facebook([
                    'app_id' => config('services.facebook.client_id'),
                    'app_secret' => config('services.facebook.client_secret'),
                    'default_graph_version' => 'v4.0',
                    'default_access_token' => Auth::user()->providerFacebook()->provider_token,
                ]);

                $response = $fb->get('/me/accounts');

                $response = json_decode($response->getBody(), true);

                $data['facebook']['groups'] = $response['data'];
            } catch (\Exception $exception) {
                //
            }
        }

        return view('client.cabinet.index', $data);
    }

    public function test(Request $request)
    {
        if (!is_null(Auth::user()) && Auth::user()->providerFacebook()) {
            try {
                $fb = new \Facebook\Facebook([
                    'app_id' => config('services.facebook.client_id'),
                    'app_secret' => config('services.facebook.client_secret'),
                    'default_graph_version' => 'v4.0',
                ]);

                $message = $request->get('message', null);
                $group = $request->get('group', null);

                if (!is_null($message) && !is_null($group)) {
                    $group = explode('_', $group);

                    if (count($group) === 2) {
                        $id = $group[0];
                        $token = $group[1];

                        $response = $fb->post(
                            '/' . $id . '/feed?message=' . $message . '&access_token=' . $token
                        );
                    }
                }
            } catch (\Exception $exception) {
                //
            }
        }

        return redirect()->route('client.cabinet.index');
    }
}
