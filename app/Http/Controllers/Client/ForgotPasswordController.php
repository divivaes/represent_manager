<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\Client\Auth\ResetLinkEmailRequest;
use App\Http\Requests\Client\Auth\ResetPasswordRequest;
use App\Repositories\Client\UserRepository;
use App\Services\Client\TokenService;

class ForgotPasswordController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct(UserRepository $repository, TokenService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function showLinkRequestForm()
    {
        return view('client.auth.forgot');
    }

    public function sendResetLinkEmail(ResetLinkEmailRequest $request)
    {
        $this->service->send($request->email);

        flash()->success('Reset mail is send successfully. Go check your inbox');

        return redirect()->route('client.password.forgot');
    }

    public function reset(ResetPasswordRequest $request)
    {
        $user = $this->repository->getByEmail($request->email);

        $this->repository->update($user, $request->all());

        $this->service->deleteToken($request->token);

        flash()->success('Your password has been changed successfully. You can log in now');

        return redirect()->route('client.auth.signin');
    }

    public function showResetForm($email, $token)
    {
        return view('client.auth.reset', [
            'token' => $token,
            'email' => $email
        ]);
    }
}
