<?php

namespace App\Http\Controllers\Client;

use App\Repositories\Client\SubscriptionRepository;
use App\Services\Client\BraintreeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    protected $braintreeService;
    protected $subscriptionRepository;

    public function __construct(
        BraintreeService $braintreeService,
        SubscriptionRepository $subscriptionRepository
    ) {
        $this->braintreeService = $braintreeService;
        $this->subscriptionRepository = $subscriptionRepository;
    }

    public function index()
    {
        return view('client.payment.index');
    }

    public function store(Request $request)
    {
        $payload = $request->input('payload', []);
        $planId = $request->input('plan_id', '');

        if (isset($payload['nonce']) && mb_strlen($payload['nonce'])) {
            try {
                $result = $this->braintreeService->gateway()->subscription()->create([
                    'paymentMethodNonce' => $payload['nonce'],
                    'planId' => $planId,
                ]);

                if ($result->success) {
                    $subscription = $result->subscription;

                    $user = Auth::user();
                    $userId = null;
                    $userFirstName = null;
                    $userLastName = null;
                    $userEmail = null;

                    if (!is_null($user)) {
                        $userId = $user->id;
                        $userFirstName = $user->first_name;
                        $userLastName = $user->last_name;
                        $userEmail = $user->email;
                    }

                    $this->subscriptionRepository->create([
                        'user_id' => $userId,
                        'user_first_name' => $userFirstName,
                        'user_last_name' => $userLastName,
                        'user_email' => $userEmail,
                        'braintree_subscription_id' => $subscription->id,
                        'braintree_subscription_plan_id' => $subscription->planId,
                        'status' => $subscription->status,
                        'braintree_subscription_price' => $subscription->price,
                        'braintree_subscription_first_billing_date' => $subscription->firstBillingDate,
                        'braintree_subscription_paid_through_date' => $subscription->paidThroughDate,
                        'braintree_subscription_payload' => json_encode($subscription),
                    ]);

                    return response()->json(['success' => true]);
                }


                foreach ($result->errors->deepAll() as $item) {
                    return response()->json(['success' => false, 'message' => $item->message]);
                }

                return response()->json(['success' => false, 'message' => 'n/a']);
            } catch (\Exception $ex) {
                return response()->json(['success' => false, 'message' => $ex->getMessage()]);
            }
        }

        return response()->json(['success' => false, 'message' => 'n/a']);
    }
}
