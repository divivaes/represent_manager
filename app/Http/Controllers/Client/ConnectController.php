<?php

namespace App\Http\Controllers\Client;

use App\Repositories\Client\UserProviderRepository;
use App\Repositories\Client\UserRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class ConnectController extends Controller
{
    protected $userRepository;
    protected $userProviderRepository;

    public function __construct(
        UserRepository $userRepository,
        UserProviderRepository $userProviderRepository
    ) {
        $this->userRepository = $userRepository;
        $this->userProviderRepository = $userProviderRepository;
    }

    public function redirect(string $provider = 'facebook')
    {
        return Socialite::driver($provider)->scopes(['manage_pages', 'publish_pages'])->redirect();
    }

    public function callback(string $provider = 'facebook')
    {
        $socialite = Socialite::driver($provider)->fields([
            'id',
            'email',
            'first_name',
            'last_name',
        ])->user();

        $user = $this->userRepository->firstOrCreate([
            'email' => $socialite->user['email'],
        ], [
            'first_name' => $socialite->user['first_name'],
            'last_name' => $socialite->user['last_name'],
        ]);

        $this->userProviderRepository->updateOrCreate([
            'user_id' => $user->id,
            'provider_name' => 'facebook',
        ], [
            'provider_id' => $socialite->user['id'],
            'provider_token' => $socialite->token,
            'provider_token_expire_at' => Carbon::now()->addSeconds($socialite->expiresIn)->toDateTimeString(),
        ]);

        Auth::loginUsingId($user->id);

        return redirect(route('client.cabinet.index'));
    }
}
