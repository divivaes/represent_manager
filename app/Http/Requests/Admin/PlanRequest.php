<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PlanRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules() : array
    {
        $rules = [];

        $rules['name'] = 'required|min:3|max:35';
        $rules['price'] = 'required|numeric';
        $rules['description'] = 'required|string|min:10';
        $rules['features'] = 'required|string|min:10';
        $rules['braintree_plan_id'] = 'required|string';

        return $rules;
    }
}
