<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class MicrositeRequest extends FormRequest
{

    public function __construct(ValidationFactory $validationFactory)
    {
        parent::__construct();
        $validationFactory->extend(
            'domain',
            function ($attribute, $value, $parameters) {
                $reserved = [
                    'a', 'about', 'aboutu', 'abuse', 'acme', 'ad', 'admanager', 'admin', 'admindashboard',
                    'administrator', 'ads', 'adsense', 'adult', 'adword', 'adwords', 'affiliate', 'affiliatepage',
                    'affiliates', 'afp', 'alpha', 'anal', 'analytic', 'android', 'answer', 'anu', 'anus', 'ap', 'api',
                    'api1', 'api2', 'api3', 'app', 'appengine', 'application', 'appnew', 'arse', 'as', 'asdf', 'ass',
                    'asset', 'assets', 'assets1', 'assets2', 'assets3', 'assets4', 'assets5', 'asshole', 'atf',
                    'backup', 'ball', 'balls', 'ballsack', 'bank', 'base', 'bastard', 'beginner', 'beta', 'biatch',
                    'billing', 'binarie', 'binary', 'bitch', 'biz', 'blackberry', 'blog', 'blogsearch', 'bloody',
                    'blowjob', 'blowjobs', 'bollock', 'boner', 'boob', 'boobs', 'book', 'bugger', 'bum', 'butt',
                    'buttplug', 'buy', 'buzz', 'c', 'cache', 'calendar', 'cart', 'catalog', 'ceo', 'chart', 'chat',
                    'checkout', 'ci', 'cia', 'client', 'clients', 'clitori', 'clitoris', 'cname', 'cnarne', 'cock',
                    'code', 'community', 'confirm', 'confirmation', 'contact', 'contact-u', 'contactu', 'content',
                    'controlpanel', 'coon', 'core', 'corp', 'countrie', 'country', 'cp', 'cpanel', 'crap', 'cs', 'css',
                    'css1', 'css2', 'css3', 'cunt', 'cv', 'damn', 'dashboard', 'data', 'demo', 'deploy', 'deployment',
                    'desktop', 'dev', 'devel', 'developement', 'developer', 'developers', 'development', 'dick', 'dike',
                    'dildo', 'dir', 'directory', 'discussion', 'dl', 'doc', 'docs', 'document', 'donate', 'download',
                    'dyke', 'e', 'earth', 'email', 'enable', 'encrypted', 'engine', 'error', 'errorlog', 'fag',
                    'faggot', 'fbi', 'feature', 'feck', 'feed', 'feedburner', 'feedproxy', 'feeds', 'felching',
                    'fellate', 'fellatio', 'file', 'files', 'finance', 'flange', 'folder', 'forgotpassword', 'forum',
                    'friend', 'ftp', 'fuck', 'fudgepacker', 'fun', 'fusion', 'gadget', 'gear', 'geographic',
                    'gettingstarted', 'git', 'gitlab', 'gmail', 'go', 'goddamn', 'goto',
                    'gov', 'graph', 'graphs', 'group', 'hell', 'help', 'home', 'homo', 'html', 'htrnl',
                    'http', 'https', 'i', 'image', 'images', 'imap', 'img', 'img1', 'img2', 'img3',
                    'investor', 'invoice', 'invoices', 'io', 'ios', 'ipad', 'iphone', 'irnage', 'irng', 'item',
                    'j', 'jenkin', 'jerk', 'jira', 'jizz', 'job', 'join', 'js', 'js1', 'js2',
                    'kb', 'knobend', 'knowledgebase', 'lab', 'labia', 'legal', 'lesbo', 'list', 'lmao', 'lmfao',
                    'local', 'locale', 'location', 'log', 'login', 'logout', 'logs', 'm', 'mail', 'manage',
                    'manager', 'map', 'marketing', 'me', 'media', 'message', 'misc', 'mm', 'mms', 'mobile',
                    'model', 'money', 'movie', 'muff', 'mx', 'my', 'mystore', 'n', 'net', 'network',
                    'networks', 'new', 'news', 'newsite', 'nigga', 'nigger', 'npm', 'ns', 'ns1', 'ns2',
                    'ns3', 'ns4', 'ns5', 'omg', 'online', 'order', 'org', 'other', 'p0rn', 'pack',
                    'packagist', 'page', 'pages', 'partner', 'partnerpage', 'partners', 'password', 'payment',
                    'payments', 'peni', 'penis', 'people', 'person', 'pi', 'pis', 'piss', 'place', 'podcast', 'policy',
                    'poop', 'pop', 'pop3', 'popular', 'porn', 'pr0n', 'press', 'pricing', 'prick', 'print', 'privacy',
                    'private', 'prod', 'product', 'production', 'profile', 'promo', 'promotion', 'proxie', 'proxies',
                    'proxy', 'pube', 'public', 'purchase', 'pussy', 'queer', 'querie', 'queries', 'query', 'r', 'radio',
                    'random', 'reader', 'recover', 'redirect', 'register', 'registration', 'release', 'report',
                    'research', 'resolve', 'resolver', 'rnail', 'rnicrosoft', 'root', 'rs', 'rss', 'sale', 'sandbox',
                    'scholar', 'scrotum', 'search', 'secure', 'seminar', 'server', 'servers', 'service', 'sex', 'sftp',
                    'sh1t', 'shit', 'shop', 'shopping', 'shortcut', 'signin', 'signup', 'site', 'sitemap', 'sitenew',
                    'sitenews', 'sites', 'sketchup', 'sky', 'slash', 'slashinvoice', 'slut', 'sm', 'smegma', 'sms',
                    'smtp', 'soap', 'software', 'sorry', 'spreadsheet', 'spunk', 'srntp', 'ssh', 'ssl', 'stage',
                    'staging', 'stat', 'static', 'statistic', 'statistics', 'stats', 'statu', 'status', 'store',
                    'suggest', 'suggestquerie', 'suggestquery', 'support', 'survey', 'surveys', 'surveytool', 'svn',
                    'sync', 'sysadmin', 'talk', 'talkgadget', 'test',
                    'tester', 'testing', 'text', 'tit', 'tits', 'tool', 'toolbar', 'tosser', 'trac', 'translate',
                    'translation', 'translator', 'trend', 'turd', 'twat', 'txt', 'ul', 'upload', 'uploads', 'vagina',
                    'validation', 'validations', 'vid', 'video', 'video-stat', 'videos', 'voice', 'w', 'wank', 'wave',
                    'webdisk', 'webmail', 'webmaster', 'webrnail', 'whm', 'whoi', 'whois', 'whore', 'wifi', 'wiki',
                    'wtf', 'ww', 'www', 'www1', 'www2', 'wwww', 'xhtml', 'xhtrnl', 'xml', 'xxx'
                ];

                if (empty($value)) {
                    return true;
                }

                if (!preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $value) ||
                    !preg_match("/^.{1,253}$/", $value) ||
                    !preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $value) ||
                    !filter_var($value, FILTER_VALIDATE_DOMAIN)) {
                    return false;
                }

                if (substr($value, -strlen(config('app.domain'))) === config('app.domain')) {
                    $reachDomainParts = explode('.', $value);
                    if ((count($reachDomainParts) != 3) || in_array($reachDomainParts[0], $reserved)) {
                        return false;
                    }
                }
                return true;
            },
            'The microsite domain name must be a valid domain without an http protocol e.g. '.
            'mydoamin.reachadvance.com, mycustomdomain.com. Warning - there are some reserved domain names!'
        );
    }

    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        $micrositeID = -1;
        if (isset($this->record->microsite) && isset($this->record->microsite->id)) {
            $micrositeID = $this->record->microsite->id;
        }

        $rules = [
            'microsite_is_active' => 'boolean',
            'microsite' => 'domain|unique:microsites,domain,' . $micrositeID . ',id'
        ];
        return $rules;
    }
}
