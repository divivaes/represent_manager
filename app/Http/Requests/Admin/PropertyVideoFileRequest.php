<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PropertyVideoFileRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id' => 'required|numeric',
            'file' => sprintf(
                '%s%s',
                'required|mimetypes:image/png|',
                'dimensions:min_width=640,min_height=640,max_width=3000,max_height=3000|max:10240'
            ),
        ];
    }
}
