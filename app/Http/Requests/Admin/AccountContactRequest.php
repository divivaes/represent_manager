<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AccountContactRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id'    => 'nullable|integer|min:1',
            'first_name' => 'required|min:3|max:35',
            'last_name'  => 'required|min:3|max:35',
            'phone'      => 'nullable|string|max:20',
            'email'      => 'required|email',
            'notes'      => 'nullable|min:3',
            'status'     => 'required|string'
        ];
    }
}
