<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules() : array
    {
        $rules = [];

        $rules['is_active'] = 'required|boolean';

        if ($this->getMethod() === 'PUT') {
            $data = request()->all();

            if (count($data) === 3 && array_key_exists('is_active', $data)) {
                return $rules;
            }
        }

        $rules['first_name'] = 'required|min:3|max:35';
        $rules['last_name'] = 'required|min:3|max:35';
        $rules['role'] = 'required|in:user,admin';

        $rules['email'] = sprintf(
            'required|email|unique:users,email,%s,id,deleted_at,NULL',
            $this->getMethod() === 'PUT' ? $this->record->id : 'NULL'
        );

        if (mb_strlen($this->password) && mb_strlen($this->password_confirmation)) {
            $rules['password'] = 'required|confirmed|min:6';
        }

        return $rules;
    }
}
