<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AccountUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules() : array
    {
        $rules = [];

        $rules['user_id'] = 'required|numeric';
        $rules['title'] = 'required|min:3';
        $rules['content'] = 'required|min:10';

        return $rules;
    }
}
