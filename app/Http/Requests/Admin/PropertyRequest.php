<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules() : array
    {
        $rules = [];

        $rules['is_active'] = 'required|boolean';

        if ($this->getMethod() === 'PUT') {
            $data = request()->all();

            if (count($data) === 3 && array_key_exists('is_active', $data)) {
                return $rules;
            }
        }

        $rules['account_user'] = 'required|regex:/(^(\d+)_(\d+)$)/';
        $rules['address'] = 'required|min:10';
        $rules['type'] = 'required';
        $rules['status'] = 'required';
        $rules['price'] = 'required|numeric';
        $rules['beds'] = 'required|numeric';
        $rules['baths'] = 'required|numeric';

        return $rules;
    }
}
