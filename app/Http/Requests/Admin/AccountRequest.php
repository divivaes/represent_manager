<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AccountRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules() : array
    {
        $rules = [];

        $rules['is_active'] = 'required|boolean';

        if ($this->getMethod() === 'PUT') {
            $data = request()->all();

            if (count($data) === 3 && array_key_exists('is_active', $data)) {
                return $rules;
            }
        }

        $rules['name'] = 'required|min:3';

        return $rules;
    }
}
