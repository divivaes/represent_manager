<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AccountMarketingTemplateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id'          => 'nullable|integer|min:1',
            'type'             => 'required|string',
            'title'            => 'required|string|min:2',
            'content'          => 'required|string|min:2',
            'contacts'         => 'nullable|array',
            'sending_datetime' => 'nullable|date_format:Y-m-d H:i:s'
        ];
    }
}
