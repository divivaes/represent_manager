<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules() : array
    {
        $rules = [];

        $rules['account_user'] = 'required|regex:/(^(\d+)_(\d+)$)/';
        $rules['plan_id'] = 'required|numeric';
        $rules['charge'] = 'required_without:subscription';

        if ($this->charge) {
            $rules['charge_start_at'] = 'required|date';
            $rules['charge_end_at'] = 'required|date|after:charge_start_at';
            $rules['charge_price'] = 'required|numeric';
        }

        if ($this->subscription) {
            $rules['subscription_plan_id'] = 'required';
            $rules['subscription_price'] = 'required|numeric';
            $rules['subscription_start_at'] = 'date|nullable|after:tomorrow';
        }

        return $rules;
    }
}
