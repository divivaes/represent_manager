<?php


namespace App\Http\Requests\Client\Account;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Factory as ValidationFactory;

class EditAccountRequest extends FormRequest
{

    public function __construct(ValidationFactory $validationFactory)
    {
        parent::__construct();
        $validationFactory->extend(
            'match_old_password',
            function ($attribute, $value, $parameters) {
                if (is_null(Auth::user())) {
                    return false;
                }
                if (Auth::user()->getAuthPassword()) {
                    return Hash::check($value, Auth::user()->getAuthPassword());
                }
                return true;
            },
            "Sorry, the old password doesn't match the current one!"
        );

        $validationFactory->extend(
            'extend_different',
            function ($attribute, $value, $parameters) {
                if (is_null(Auth::user())) {
                    return false;
                }
                if (Auth::user()->getAuthPassword() == '') {
                    return true;
                }
                foreach ($parameters as $parameter) {
                    $other = $this->request->get($parameter);
                    if ($value === $other) {
                        return false;
                    }
                }
                return true;
            },
            "The password and old password must be different!"
        );
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $userId = -1;
        if (!is_null(Auth::user())) {
            $userId = Auth::user()->id;
        }
        return [
            'first_name' => 'required|string|max:35',
            'last_name' => 'required|string|max:35',
            'email' => 'required|email|unique:users,email,' . $userId . '|max:254',
            'phone' => 'required|string|max:15',
            'old_password' => 'match_old_password',
            'password' => 'confirmed|extend_different:old_password|nullable'
        ];
    }
}
