<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class AuthOnlyUser
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        $user = $this->auth->user();

        if (is_null($user)) {
            return redirect()->route('client.auth.signin');
        }

        if ($user->role !== 'user' || $user->is_active !== 1) {
            return redirect()->route('client.auth.signin');
        }

        return $next($request);
    }
}
