<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthNotUser
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if (!is_null($user)) {
            if ($user->role === 'user' && $user->is_active === 1) {
                return redirect()->route('client.cabinet.index');
            }
        }

        return $next($request);
    }
}
