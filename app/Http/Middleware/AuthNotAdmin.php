<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthNotAdmin
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if (!is_null($user)) {
            if ($user->role === 'admin' && $user->is_active === 1) {
                return redirect()->route('admin.dashboard.index');
            }
        }

        return $next($request);
    }
}
