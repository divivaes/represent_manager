<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthOnlyAdmin
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if (is_null($user)) {
            return redirect()->route('admin.auth.login');
        }

        if ($user->role !== 'admin' || $user->is_active !== 1) {
            return redirect()->route('admin.auth.login');
        }

        return $next($request);
    }
}
