<?php

namespace App\Observers;

use App\Models\Microsite;

class MicrositeObserver
{
    /**
     * Handle the microsites "created" event.
     *
     * @param  Microsite  $microsites
     * @return void
     */
    public function created(Microsite $microsites)
    {
        //
    }

    /**
     * Handle the microsites "updated" event.
     *
     * @param  Microsite  $microsites
     * @return void
     */
    public function updated(Microsite $microsites)
    {
        //
    }

    /**
     * Handle the microsites "deleted" event.
     *
     * @param  Microsite  $microsites
     * @return void
     */
    public function deleted(Microsite $microsites)
    {
        //
    }

    /**
     * Handle the microsites "restored" event.
     *
     * @param  Microsite  $microsites
     * @return void
     */
    public function restored(Microsite $microsites)
    {
        //
    }

    /**
     * Handle the microsites "force deleted" event.
     *
     * @param  Microsite  $microsites
     * @return void
     */
    public function forceDeleted(Microsite $microsites)
    {
        //
    }
}
