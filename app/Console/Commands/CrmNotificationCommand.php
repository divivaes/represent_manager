<?php

namespace App\Console\Commands;

use App\Models\CrmNotification;
use App\Repositories\Admin\CrmNotificationRepository;
use App\Services\Admin\CrmNotificationService;
use Illuminate\Console\Command;

class CrmNotificationCommand extends Command
{
    protected $signature = 'notify:contacts';
    protected $description = 'Send email to contacts';
    protected $crmNotificationService;
    protected $crmNotificationRepository;

    public function __construct(
        CrmNotificationService $crmNotificationService,
        CrmNotificationRepository $crmNotificationRepository
    ) {
        parent::__construct();
        $this->crmNotificationService = $crmNotificationService;
        $this->crmNotificationRepository = $crmNotificationRepository;
    }

    public function handle()
    {
        $notifications = CrmNotification::query()
            ->where([
                ['sending_datetime', '<=', now()],
                ['is_sent', false]
            ])
            ->orWhere([
                ['sending_datetime', null],
                ['is_sent', false]
            ])
            ->get();

        if ($notifications->count() >= 1) {
            foreach ($notifications as $notification) {
                $this->crmNotificationService->send($notification);

                $this->crmNotificationRepository->edit($notification, ['is_sent' => true]);
            }
        }

        $this->info("Command run successfully");
    }
}
