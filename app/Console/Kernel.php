<?php

namespace App\Console;

use App\Console\Commands\CrmNotificationCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CrmNotificationCommand::class
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('notify:contacts')->everyMinute();
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
    }
}
