<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Facades\Artisan;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        $this->clearCache();

        return $app;
    }

    protected function clearCache()
    {
        $commands = [
            'clear-compiled',
            'cache:clear',
            'view:clear',
            'route:clear',
            'config:clear'
        ];

        foreach ($commands as $command) {
            Artisan::call($command);
        }
    }
}
