<?php

namespace Tests;

use File;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public $faker = null;

    protected function setUp(): void
    {
        parent::setUp();

        session()->start();

        $this->faker = \Faker\Factory::create();
    }

    protected function loginAsAdmin()
    {
        $admin = factory(User::class)->create();

        $data = [
            '_token' => csrf_token(),
            'email' => $admin->email,
            'password' => 'secret',
        ];

        $this->post(route('admin.auth.authenticate'), $data);

        return $admin;
    }

    protected function loginAsUser(array $userData = null)
    {
        if (is_null($userData)) {
            $user = factory(User::class)->create([
                'role' => 'user'
            ]);
            $password = 'secret';
        } else {
            $password = $userData['passwordText'];
            unset($userData['passwordText']);
            $user = factory(User::class)->create($userData);
        }

        $data = [
            '_token'   => csrf_token(),
            'email'    => $user->email,
            'password' => $password
        ];

        $this->post(route('client.auth.login'), $data);
        return $user;
    }

    public function getFixture(string $file = null, bool $toArray = true)
    {
        $fixture = File::get(base_path(sprintf('tests/Fixtures/%s', $file)));
        return json_decode($fixture, $toArray);
    }
}
