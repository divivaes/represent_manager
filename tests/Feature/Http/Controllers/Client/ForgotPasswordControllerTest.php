<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ForgotPasswordControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Client\ForgotPasswordController::showLinkRequestForm
     * @covers \App\Http\Middleware\AuthNotUser::handle
     */
    public function iShouldBeAbleToSeeForgotPasswordPageAsGuest()
    {
        $this->assertFalse(Auth::check());

        $response = $this->get(route('client.password.forgot'));

        $response->assertStatus(200);
        $response->assertSeeText('Forgot password');
    }


    /**
     * @test
     * @covers \App\Http\Controllers\Client\ForgotPasswordController::sendResetLinkEmail
     * @covers \App\Http\Requests\Client\Auth\ResetLinkEmailRequest::authorize
     * @covers \App\Http\Requests\Client\Auth\ResetLinkEmailRequest::rules
     * @covers \App\Services\Client\TokenService::send
     * @covers \App\Services\Client\TokenService::createToken
     * @covers \App\Services\Client\TokenService::saveToken
     * @covers \App\Mail\ResetPasswordMail::__construct
     * @covers \App\Mail\ResetPasswordMail::build
     * @covers \App\Http\Middleware\AuthNotUser::handle
     */
    public function iShouldBeAbleSendMyEmailToGetResetLink()
    {
        $user = factory(User::class)->create([
            'role' => 'user',
        ]);

        $data = [
            '_token' => csrf_token(),
            'email'  => $user->email
        ];

        $response = $this->post(route('client.password.email'), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('client.password.forgot'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'Reset mail is send successfully. Go check your inbox',
            $flash['message']
        );

        $this->assertFalse(Auth::check());
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Client\ForgotPasswordController::showResetForm
     */
    public function iShouldBeAbleSeePasswordResetPage()
    {
        $user = factory(User::class)->create([
            'role' => 'user',
        ]);

        $token = Str::random(60);

        $response = $this->get(route('client.password.reset', ['email' => $user->email, 'token' => $token]));

        $response->assertStatus(200);
        $response->assertSeeText('Reset Password');
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Client\ForgotPasswordController::__construct
     * @covers \App\Http\Controllers\Client\ForgotPasswordController::reset
     * @covers \App\Services\Client\TokenService::deleteToken
     * @covers \App\Http\Requests\Client\Auth\ResetPasswordRequest::authorize
     * @covers \App\Http\Requests\Client\Auth\ResetPasswordRequest::rules
     * @covers \App\Repositories\Client\Repository::update
     * @covers \App\Repositories\Client\UserRepository::update
     * @covers \App\Repositories\Client\UserRepository::getByEmail
     */
    public function iShouldBeAbleChangePassword()
    {
        $user = factory(User::class)->create([
            'role' => 'user',
        ]);

        $token = Str::random(60);

        $data = [
            '_token'                => csrf_token(),
            'email'                 => $user->email,
            'token'                 => $token,
            'password'              => 'secret',
            'password_confirmation' => 'secret'
        ];

        $response = $this->post(route('client.password.update'), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('client.auth.signin'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'Your password has been changed successfully. You can log in now',
            $flash['message']
        );

        $this->assertFalse(Auth::check());
    }
}
