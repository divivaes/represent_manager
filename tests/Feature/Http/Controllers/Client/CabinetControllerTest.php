<?php

namespace Tests\Feature\Http\Controllers\Client;

use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CabinetControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Client\CabinetController::index
     * @covers \App\Http\Middleware\AuthOnlyUser::__construct
     * @covers \App\Http\Middleware\AuthOnlyUser::handle
     */
    public function iShouldBeAbleToAccessCabinetPageAsUser()
    {
        $this->loginAsUser();

        $this->assertTrue(Auth::check());

        $response = $this->get(route('client.cabinet.index'));
        $response->assertStatus(200);
        $response->assertSee(sprintf('Cabinet'));
    }
}
