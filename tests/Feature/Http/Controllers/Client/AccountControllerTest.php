<?php


namespace Tests\Feature\Http\Controllers\Client;

use App\Models\User;
use App\Models\UserProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AccountControllerTest extends TestCase
{

    use RefreshDatabase;

    private function getUserData(): array
    {
        $userPassword = 'password';
        $userPasswordHash = Hash::make($userPassword);

        return [
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'phone' => '111222333',
            'email' => 'test1232@test.com',
            'role' => 'user',
            'password' => $userPasswordHash,
            'passwordText' => $userPassword
        ];
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Client\AccountController::edit
     * @covers \App\Http\Middleware\AuthOnlyUser::__construct
     * @covers \App\Http\Middleware\AuthOnlyUser::handle
     */
    public function iShouldNotBeAbleToAccessAccountEditPageWithoutLogin()
    {
        $response = $this->get(route('client.account.edit'));
        $response->assertStatus(302);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Client\AccountController::edit
     * @covers \App\Http\Middleware\AuthOnlyUser::__construct
     * @covers \App\Http\Middleware\AuthOnlyUser::handle
     */
    public function iShouldBeAbleToAccessAccountEditPageAsUser()
    {
        $this->loginAsUser();
        $this->assertTrue(Auth::check());
        $response = $this->get(route('client.account.edit'));
        $response->assertStatus(200);
        $response->assertSee('Edit Account Data');
        $response->assertSee('first_name');
        $response->assertSee('last_name');
        $response->assertSee('email');
        $response->assertSee('password');
        $response->assertSee('password_confirmation');
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Client\AccountController::update
     * @covers \App\Http\Middleware\AuthOnlyUser::__construct
     * @covers \App\Http\Middleware\AuthOnlyUser::handle
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::authorize
     */

    public function iShouldNotBeAbleToSaveAccountDataWithoutLogin()
    {
        $putData = [
            '_token' => csrf_token(),
            'first_name' => 'NewFirstName',
            'last_name' => 'NewLastName',
            'old_password' => '',
            'phone' => '999999999',
            'email' => 'test@test.com',
        ];
        $response = $this->put(route('client.account.update'), $putData);
        $response->assertStatus(302);
    }


    /**
     * @test
     * @covers \App\Http\Controllers\Client\AccountController::__construct
     * @covers \App\Http\Controllers\Client\AccountController::update
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::__construct
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::rules
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::authorize
     */

    public function iShouldBeAbleToUpdateAccountDataAsUser()
    {
        $userData = $this->getUserData();
        $user = $this->loginAsUser($userData);
        $this->assertTrue(Auth::check());

        $putData = [
            '_token' => csrf_token(),
            'first_name' => 'NewFirstName',
            'last_name' => 'NewLastName',
            'old_password' => $userData['passwordText'],
            'phone' => '999999999',
            'email' => 'test@test.com',
        ];

        $response = $this->put(route('client.account.update'), $putData);
        $response->assertStatus(302);
        $response->assertRedirect(route('client.account.edit'));
        $user->refresh();
        $this->assertEquals($putData['first_name'], $user->first_name);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Client\AccountController::__construct
     * @covers \App\Http\Controllers\Client\AccountController::update
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::__construct
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::rules
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::authorize
     */

    public function iShouldNotBeAbleToUpdateAccountDataAsUserWithEqualsOldAndNewPasswords()
    {
        $userData = $this->getUserData();
        $user = $this->loginAsUser($userData);
        $this->assertTrue(Auth::check());

        $putData = [
            '_token' => csrf_token(),
            'first_name' => 'NewFirstName',
            'last_name' => 'NewLastName',
            'old_password' => $userData['passwordText'],
            'phone' => '999999999',
            'email' => 'test@test.com',
            'password' => $userData['passwordText'],
            'password_confirmation' => $userData['passwordText']
        ];

        $response = $this->put(route('client.account.update'), $putData);
        $response->assertStatus(302);
        $flash = session('errors')->first();
        $user->refresh();
        $this->assertEquals($flash, "The password and old password must be different!");
    }


    /**
     * @test
     * @covers \App\Http\Controllers\Client\AccountController::__construct
     * @covers \App\Http\Controllers\Client\AccountController::update
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::__construct
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::rules
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::authorize
     */

    public function iShouldNotBeAbleToUpdateAccountDataAsUserWithDifferentPasswordAndConfirmation()
    {
        $userData = $this->getUserData();
        $user = $this->loginAsUser($userData);
        $this->assertTrue(Auth::check());

        $putData = [
            '_token' => csrf_token(),
            'first_name' => 'NewFirstName',
            'last_name' => 'NewLastName',
            'old_password' => $userData['passwordText'],
            'phone' => '999999999',
            'email' => 'test@test.com',
            'password' => "test",
            'password_confirmation' => 'test2'
        ];

        $response = $this->put(route('client.account.update'), $putData);
        $response->assertStatus(302);
        $flash = session('errors')->first();
        $user->refresh();
        $this->assertEquals($flash, "The password confirmation does not match.");
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Client\AccountController::__construct
     * @covers \App\Http\Controllers\Client\AccountController::update
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::__construct
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::rules
     * @covers \App\Http\Requests\Client\Account\EditAccountRequest::authorize
     */

    public function iShouldNotBeAbleToUpdateAccountDataAsUserWithoutOldPassword()
    {
        $userData = $this->getUserData();
        $user = $this->loginAsUser($userData);
        $this->assertTrue(Auth::check());

        $putData = [
            '_token' => csrf_token(),
            'first_name' => 'NewFirstName',
            'last_name' => 'NewLastName',
            'old_password' => '',
            'phone' => '999999999',
            'email' => 'test@test.com',
        ];

        $response = $this->put(route('client.account.update'), $putData);
        $response->assertStatus(302);

        $flash = session('errors')->first();
        $user->refresh();
        $this->assertEquals($flash, "Sorry, the old password doesn't match the current one!");
        $this->assertNotEquals($putData['first_name'], $user->first_name);
        $this->assertEquals($userData['first_name'], $user->first_name);
    }

//    /**
//     * @test
//     * @covers \App\Http\Controllers\Client\AccountController::edit
//     * @covers \App\Http\Middleware\AuthOnlyUser::__construct
//     * @covers \App\Http\Middleware\AuthOnlyUser::handle
//     */
//    public function iShouldBeAbleToUpdateAccountDataAsUserWithoutOldPasswordIfImFacebookUser()
//    {
//        $userProvider = factory(UserProvider::class)->create();
//        $user = User::find($userProvider->id)->first();
//        $this->be($user);
//        $this->assertTrue(Auth::check());
//
//        $putData = [
//            '_token' => csrf_token(),
//            'first_name' => 'NewFirstName',
//            'last_name' => 'NewLastName',
//            'phone' => '999999999',
//            'email' => 'test@test.com',
//        ];
//
//        $response = $this->put(route('client.account.update'), $putData);
//        $response->assertStatus(302);
//
//        $flash = session('info')->first();
//        $user = User::find($userProvider->id)->first();
//        $this->assertEquals($flash, "Sorry, the old password doesn't match the current one!");
//        $this->assertEquals($putData['first_name'], $user->first_name);
//
//        $response = $this->get(route('client.account.edit'));
//        $response->assertStatus(302);
//    }
}
