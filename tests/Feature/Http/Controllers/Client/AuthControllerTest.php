<?php

namespace Tests\Feature\Http\Controllers\Client;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Providers\AppServiceProvider::register
     * @covers \App\Providers\AppServiceProvider::boot
     *
     * @covers \App\Providers\RouteServiceProvider::boot
     * @covers \App\Providers\RouteServiceProvider::map
     * @covers \App\Providers\RouteServiceProvider::mapWebRoutes
     *
     * @covers \App\Http\Controllers\Client\AuthController::signup
     * @covers \App\Http\Middleware\AuthNotUser::handle
     */
    public function iShouldBeAbleToSeeSignUpPageAsGuest()
    {
        $this->assertFalse(Auth::check());

        $response = $this->get(route('client.auth.signup'));

        $response->assertStatus(200);
    }

    /**
     * @test
     * @covers \App\Providers\AppServiceProvider::register
     * @covers \App\Providers\AppServiceProvider::boot
     *
     * @covers \App\Providers\RouteServiceProvider::boot
     * @covers \App\Providers\RouteServiceProvider::map
     * @covers \App\Providers\RouteServiceProvider::mapWebRoutes
     *
     * @covers \App\Http\Controllers\Client\AuthController::signin
     * @covers \App\Http\Middleware\AuthNotUser::handle
     */
    public function iShouldBeAbleToSeeSignInPageAsGuest()
    {
        $this->assertFalse(Auth::check());

        $response = $this->get(route('client.auth.signin'));

        $response->assertStatus(200);
    }

    /**
     * @test
     * @covers \App\Http\Requests\Client\Auth\SignUpRequest::authorize
     * @covers \App\Http\Requests\Client\Auth\SignUpRequest::rules
     * @covers \App\Repositories\Client\Repository::create
     * @covers \App\Repositories\Client\Repository::prepare
     * @covers \App\Repositories\Client\UserRepository::__construct
     * @covers \App\Repositories\Client\UserRepository::create
     * @covers \App\Repositories\Client\AccountRepository::__construct
     * @covers \App\Repositories\Client\AccountRepository::create
     * @covers \App\Repositories\Client\AccountUserRepository::__construct
     * @covers \App\Repositories\Client\AccountUserRepository::create
     * @covers \App\Http\Controllers\Client\AuthController::__construct
     * @covers \App\Http\Controllers\Client\AuthController::signup
     * @covers \App\Http\Controllers\Client\AuthController::register
     * @covers \App\Listeners\SuccessfulLogin::handle
     */
    public function iShouldBeAbleToSignUpAnAccount()
    {
        $data = [
            '_token' => csrf_token(),
            'first_name' => 'Barry',
            'last_name' => 'Allen',
            'email' => 'allen@speed.com',
            'phone' => '+359',
            'password' => 'secret',
            'password_confirmation' => 'secret',
        ];

        $response = $this->post(route('client.auth.register', $data));
        $response->assertRedirect(route('client.cabinet.index'));

        $this->assertTrue(Auth::check());
    }

    /**
     * @test
     * @covers \App\Http\Requests\Client\Auth\SignInRequest::rules
     * @covers \App\Http\Controllers\Client\AuthController::login
     */
    public function iShouldNotBeAbleSignInWithIncorrectCredentials()
    {
        $data = [
            '_token' => csrf_token(),
            'email' => 'divivaes@gmail.com',
            'password' => 'secret',
        ];

        $response = $this->post(route('client.auth.login', $data));

        $response->assertStatus(302);
        $response->assertRedirect(route('client.auth.signin'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('danger', $flash['level']);
        $this->assertEquals(
            'The credentials you supplied were not correct or did not grant access to this resource.',
            $flash['message']
        );
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Client\AuthController::login
     * @covers \App\Http\Requests\Client\Auth\SignInRequest::rules
     * @covers \App\Listeners\SuccessfulLogin::handle
     */
    public function iShouldBeAbleToLoginWithAccount()
    {
        $user = factory(User::class)->create([
            'role' => 'user',
        ]);

        $this->assertNull($user->last_login_at);

        $data = [
            '_token' => csrf_token(),
            'email' => $user->email,
            'password' => 'secret'
        ];

        $response = $this->post(route('client.auth.login'), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('client.cabinet.index'));

        $this->assertTrue(Auth::check());

        $user->refresh();

        $this->assertNotNull($user->last_login_at);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Client\AuthController::logout
     */
    public function iShouldBeAbleToLogout()
    {
        $this->loginAsUser();

        $this->assertTrue(Auth::check());

        $response = $this->get(route('client.auth.logout'));

        $response->assertStatus(302);
        $response->assertRedirect(route('client.auth.signin'));

        $this->assertFalse(Auth::check());
    }

    /**
     * @test
     * @covers \App\Http\Middleware\AuthOnlyUser::__construct
     * @covers \App\Http\Middleware\AuthOnlyUser::handle
     */
    public function iShouldNotBeAbleToAccessCabinetPageAsGuest()
    {
        $this->assertFalse(Auth::check());

        $response = $this->get(route('client.cabinet.index'));
        $response->assertStatus(302);
        $response->assertRedirect(route('client.auth.signin'));
    }

    /**
     * @test
     * @covers \App\Http\Middleware\AuthNotUser::handle
     */
    public function iShouldNotBeAbleToSeeLoginPageAsUser()
    {
        $this->loginAsUser();

        $this->assertTrue(Auth::check());

        $response = $this->get(route('client.auth.signin'));
        $response->assertStatus(302);
        $response->assertRedirect(route('client.cabinet.index'));
    }
}
