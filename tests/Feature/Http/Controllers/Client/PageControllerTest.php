<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PageControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Providers\AppServiceProvider::register
     * @covers \App\Providers\AppServiceProvider::boot
     *
     * @covers \App\Providers\RouteServiceProvider::boot
     * @covers \App\Providers\RouteServiceProvider::map
     * @covers \App\Providers\RouteServiceProvider::mapWebRoutes
     *
     * @covers \App\Providers\EventServiceProvider::boot
     *
     * @covers \App\Http\Controllers\Client\PageController::index
     */
    public function iShouldBeAbleToSeeHomePageAsGuest()
    {
        $response = $this->get(route('client.pages.index'));

        $response->assertStatus(200);
    }

    /**
     * @test
     * @covers \App\Providers\AppServiceProvider::register
     * @covers \App\Providers\AppServiceProvider::boot
     *
     * @covers \App\Providers\RouteServiceProvider::boot
     * @covers \App\Providers\RouteServiceProvider::map
     * @covers \App\Providers\RouteServiceProvider::mapWebRoutes
     *
     * @covers \App\Providers\EventServiceProvider::boot
     *
     * @covers \App\Http\Controllers\Client\PageController::pricing
     */
    public function iShouldBeAbleToSeePricingPageAsGuest()
    {
        $response = $this->get(route('client.pages.pricing'));

        $response->assertStatus(200);
    }
}
