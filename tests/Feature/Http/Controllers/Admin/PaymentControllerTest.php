<?php

namespace Tests\Feature;

use App\Models\Charge;
use App\Models\Plan;
use App\Models\User;
use App\Models\Subscription;
use App\Services\Client\BraintreeService;
use Carbon\Carbon;
use Mockery;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\PaymentController::__construct
     * @covers \App\Http\Requests\Admin\PaymentRequest::rules
     * @covers \App\Http\Requests\Admin\PaymentRequest::authorize
     * @covers \App\Http\Controllers\Admin\PaymentController::create
     * @covers \App\Http\Controllers\Admin\PaymentController::store
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\Repository::create
     * @covers \App\Repositories\Admin\ChargeRepository::create
     * @covers \App\Repositories\Admin\ChargeRepository::__construct
     * @covers \App\Repositories\Admin\SubscriptionRepository::create
     * @covers \App\Repositories\Admin\SubscriptionRepository::__construct
     */
    public function iShouldBeAbleToCreateChargeAndSubscription()
    {
        $this->loginAsAdmin();

        $braintreeResponse = $this->getFixture('Braintree/plan.all.json', false);

        $mock = Mockery::mock(BraintreeService::class);
        $mock->shouldReceive('gateway')->andReturnSelf();
        $mock->shouldReceive('plan')->andReturnSelf();
        $mock->shouldReceive('all')->andReturn($braintreeResponse);

        $this->app->instance(BraintreeService::class, $mock);

        $response = $this->get(route('admin.payments.create'));

        $response->assertStatus(200);

        $user = factory(User::class)->create();
        $plan = factory(Plan::class)->create();

        $data = [
            '_token' => csrf_token(),
            'account_user' => sprintf('%s_%s', $user->accounts[0]->id, $user->id),
            'plan_id' => $plan->id,

            'charge' => 1,
            'charge_start_at' => Carbon::now()->addDays(1),
            'charge_end_at' => Carbon::now()->addDays(10),
            'charge_price' => 5.00,

            'subscription' => 1,
            'subscription_plan_id' => $braintreeResponse[0]->id,
            'subscription_price' => $braintreeResponse[0]->price,
            'subscription_start_at' => Carbon::now()->addDays(11),
        ];

        $response = $this->post(route('admin.payments.store'), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.subscriptions.index'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The payment has been successfully created.',
            $flash['message']
        );

        $subscription = Subscription::first();

        $this->assertNotNull($subscription);
        $this->assertNotNull($subscription->uuid);

        $charge = Charge::first();

        $this->assertNotNull($charge);
        $this->assertNotNull($charge->uuid);
    }
}
