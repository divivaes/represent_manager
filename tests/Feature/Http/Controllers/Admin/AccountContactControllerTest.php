<?php

namespace Tests\Feature;

use App\Models\Account;
use App\Models\AccountTag;
use App\Models\AccountUser;
use App\Models\User;
use App\Models\AccountContact;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountContactControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountContactController::__construct
     * @covers \App\Http\Controllers\Admin\AccountContactController::index
     */
    public function iShouldBeAbleToSeeAllContacts()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $contact1 = factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);
        $contact2 = factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $response = $this->get(route('admin.account_contacts.index', $account));

        $response->assertStatus(200);
        $response->assertSee($contact1->full_name);
        $response->assertSee($contact2->full_name);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountContactController::__construct
     * @covers \App\Http\Controllers\Admin\AccountContactController::create
     * @covers \App\Http\Controllers\Admin\AccountContactController::store
     * @covers \App\Http\Requests\Admin\AccountContactRequest::rules
     * @covers \App\Http\Requests\Admin\AccountContactRequest::authorize
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\Repository::create
     * @covers \App\Repositories\Admin\AccountContactRepository::__construct
     * @covers \App\Repositories\Admin\AccountContactRepository::create
     * @covers \App\Repositories\Admin\AccountTagRepository::getRecords
     * @covers \App\Models\Boot\UuidAble::bootUuidAble
     * @covers \App\Models\Boot\GeoAble::bootGeoAble
     */
    public function iShouldBeAbleToCreateContact()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $response = $this->get(route('admin.account_contacts.create', $account));

        $response->assertStatus(200);

        $tag = factory(AccountTag::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $data = [
            '_token' => csrf_token(),
            'user_id' => $user->id,
            'tag_ids' => [
                '0' => $tag->id
            ],
            'first_name' => 'Barry',
            'last_name' => 'Allen',
            'phone' => '+0123456789',
            'email' => 'test@test.com',
            'notes' => 'Real Estate Backend',
            'status' => 'New'
        ];

        $response = $this->post(route('admin.account_contacts.store', $account), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_contacts.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The contact has been successfully created.',
            $flash['message']
        );

        $contact = AccountContact::query()->first();

        $this->assertNotNull($contact);
        $this->assertNotNull($contact->uuid);
        $this->assertEquals($contact->first_name, $data['first_name']);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountContactController::__construct
     * @covers \App\Http\Controllers\Admin\AccountContactController::edit
     * @covers \App\Http\Controllers\Admin\AccountContactController::update
     * @covers \App\Repositories\Admin\Repository::update
     * @covers \App\Repositories\Admin\AccountContactRepository::update
     */
    public function iShouldBeAbleToUpdateContact()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        factory(AccountTag::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id,
            'title' => 'Test Tag'
        ]);

        $contact = factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $response = $this->get(route('admin.account_contacts.edit', [$account, $contact]));

        $response->assertStatus(200);

        $data = $contact->toArray();
        $data['_token'] = csrf_token();
        $data['first_name'] = 'Bruce';
        $data['last_name'] = 'Wayne';
        $data['email'] = 'bruce@batcave.com';

        $response = $this->put(route('admin.account_contacts.update', [$account, $contact]), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_contacts.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The contact has been successfully updated.',
            $flash['message']
        );

        $contact->refresh();
        $this->assertEquals($data['email'], $contact->email);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountContactController::__construct
     * @covers \App\Http\Controllers\Admin\AccountContactController::destroy
     * @covers \App\Repositories\Admin\Repository::destroy
     */
    public function iShouldBeAbleToDestroyContact()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $contact = factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $data = [];
        $data['_token'] = csrf_token();
        $data['_method'] = 'delete';

        $response = $this->delete(route('admin.account_contacts.destroy', [$account, $contact]), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_contacts.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The contact has been successfully deleted.',
            $flash['message']
        );

        $contact->refresh();
        $this->assertTrue($contact->trashed());
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountContactController::import
     * @covers \App\Services\Admin\ImportService::csvToArray
     * @covers \App\Repositories\Admin\AccountContactRepository::import
     */
    public function iShouldBeAbleImportFromCSV()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        Storage::fake('test');

        $filePath = '/tmp/phpcJONmC.csv';

        file_put_contents($filePath, "first_name,last_name,phone,email,notes,status\nBarry,Allen,+12,fl@m.ru,Note,new");

        $data = [
            '_token' => csrf_token(),
            'import' => new UploadedFile($filePath, 'data.csv', null, null, null)
        ];

        $response = $this->post(route('admin.account_contacts.import', $account), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_contacts.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'File imported successfully.',
            $flash['message']
        );
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountContactController::import
     * @covers \App\Services\Admin\ImportService::csvToArray
     * @covers \App\Repositories\Admin\AccountContactRepository::import
     */
    public function iShouldBeAbleSeeErrorWhenImportFieldEmpty()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $data = [
            '_token' => csrf_token()
        ];

        $response = $this->post(route('admin.account_contacts.import', $account), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_contacts.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('danger', $flash['level']);
        $this->assertEquals(
            'Please upload file.',
            $flash['message']
        );
    }
}
