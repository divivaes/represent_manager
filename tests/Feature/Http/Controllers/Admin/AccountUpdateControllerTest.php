<?php

namespace Tests\Feature;

use App\Models\Account;
use App\Models\AccountUpdate;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountUpdateControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountUpdateController::__construct
     * @covers \App\Http\Controllers\Admin\AccountUpdateController::index
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\AccountUpdateRepository::__construct
     * @covers \App\Repositories\Admin\AccountUpdateRepository::getRecords
     * @covers \App\Repositories\Admin\AccountUpdateRepository::getStatistics
     * @covers \App\Repositories\Admin\AccountUpdateRepository::prepareFilterQuery
     * @covers \App\Repositories\Admin\Traits\GetStatisticsTrait::getStatistics
     * @covers \App\Services\Admin\ExportService::__construct
     * @covers \App\Services\Admin\ExportService::export
     */
    public function iShouldBeAbleToSeeAllUpdates()
    {
        $update1 = factory(AccountUpdate::class)->create();
        $update2 = factory(AccountUpdate::class)->create();

        $this->loginAsAdmin();

        $response = $this->get(route('admin.account_updates.index', $update1->account));

        $response->assertStatus(200);
        $response->assertSee($update1->title);
        $response->assertDontSee($update2->title);

        $response = $this->get(route('admin.account_updates.index', $update2->account));

        $response->assertStatus(200);
        $response->assertDontSee($update1->title);
        $response->assertSee($update2->title);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountUpdateController::__construct
     * @covers \App\Http\Requests\Admin\AccountUpdateRequest::rules
     * @covers \App\Http\Requests\Admin\AccountUpdateRequest::authorize
     * @covers \App\Http\Controllers\Admin\AccountUpdateController::create
     * @covers \App\Http\Controllers\Admin\AccountUpdateController::store
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\Repository::create
     * @covers \App\Repositories\Admin\AccountUpdateRepository::create
     * @covers \App\Repositories\Admin\AccountUpdateRepository::__construct
     * @covers \App\Models\Boot\UuidAble::bootUuidAble
     */
    public function iShouldBeAbleToCreateUpdate()
    {
        $account = factory(Account::class)->create();

        $user = $account->users()->save(factory(User::class)->create());

        $this->loginAsAdmin();

        $response = $this->get(route('admin.account_updates.create', $account));

        $response->assertStatus(200);

        $data = [
            '_token' => csrf_token(),
            'user_id' => $user->id,
            'title' => 'Test title',
            'content' => 'Test content for the update...'
        ];

        $response = $this->post(route('admin.account_updates.store', $account), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_updates.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The update has been successfully created.',
            $flash['message']
        );

        $response = $this->get(route('admin.account_updates.index', $account));

        $response->assertStatus(200);
        $response->assertSee($data['title']);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountUpdateController::__construct
     * @covers \App\Http\Requests\Admin\AccountUpdateRequest::rules
     * @covers \App\Http\Requests\Admin\AccountUpdateRequest::authorize
     * @covers \App\Http\Controllers\Admin\AccountUpdateController::edit
     * @covers \App\Http\Controllers\Admin\AccountUpdateController::update
     * @covers \App\Repositories\Admin\Repository::update
     * @covers \App\Repositories\Admin\AccountUpdateRepository::update
     * @covers \App\Repositories\Admin\AccountUpdateRepository::__construct
     */
    public function iShouldBeAbleToUpdateUpdate()
    {
        $update = factory(AccountUpdate::class)->create();

        $this->loginAsAdmin();

        $response = $this->get(route('admin.account_updates.edit', [$update->account, $update]));

        $response->assertStatus(200);

        $data = $update->toArray();
        $data['_token'] = csrf_token();
        $data['title'] = 'Test title';

        $response = $this->put(route('admin.account_updates.update', [$update->account, $update]), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_updates.index', $update->account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The update has been successfully updated.',
            $flash['message']
        );

        $update->refresh();
        $this->assertEquals($data['title'], $update->title);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountUpdateController::__construct
     * @covers \App\Http\Controllers\Admin\AccountUpdateController::destroy
     * @covers \App\Repositories\Admin\Repository::destroy
     */
    public function iShouldBeAbleToDestroyUpdate()
    {
        $update = factory(AccountUpdate::class)->create();

        $this->loginAsAdmin();

        $data = [];
        $data['_token'] = csrf_token();
        $data['_method'] = 'delete';

        $response = $this->delete(route('admin.account_updates.destroy', [$update->account, $update]), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_updates.index', $update->account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The update has been successfully deleted.',
            $flash['message']
        );

        $update->refresh();
        $this->assertTrue($update->trashed());
    }
}
