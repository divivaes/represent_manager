<?php

namespace Tests\Feature;

use App\Models\Account;
use App\Models\AccountTag;
use App\Models\AccountUser;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountTagControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountTagController::__construct
     * @covers \App\Http\Controllers\Admin\AccountTagController::index
     */
    public function iShouldBeAbleToSeeAllTags()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $tag1 = factory(AccountTag::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);
        $tag2 = factory(AccountTag::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $response = $this->get(route('admin.account_tags.index', $account));

        $response->assertStatus(200);
        $response->assertSee($tag1->full_name);
        $response->assertSee($tag2->full_name);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountTagController::__construct
     * @covers \App\Http\Requests\Admin\AccountTagRequest::rules
     * @covers \App\Http\Requests\Admin\AccountTagRequest::authorize
     * @covers \App\Http\Controllers\Admin\AccountTagController::create
     * @covers \App\Http\Controllers\Admin\AccountTagController::store
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\Repository::create
     * @covers \App\Repositories\Admin\AccountTagRepository::__construct
     * @covers \App\Repositories\Admin\AccountTagRepository::prepare
     * @covers \App\Repositories\Admin\AccountTagRepository::create
     */
    public function iShouldBeAbleToCreateTag()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $response = $this->get(route('admin.account_tags.create', $account));

        $response->assertStatus(200);

        $data = [
            '_token' => csrf_token(),
            'user_id' => $user->id,
            'title' => 'First tag'
        ];

        $response = $this->post(route('admin.account_tags.store', $account), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_tags.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The tag has been successfully created.',
            $flash['message']
        );

        $tag = AccountTag::query()->first();

        $this->assertNotNull($tag);
        $this->assertNotNull($tag->uuid);
        $this->assertEquals($tag->title, $data['title']);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountTagController::__construct
     * @covers \App\Http\Controllers\Admin\AccountTagController::edit
     * @covers \App\Http\Controllers\Admin\AccountTagController::update
     * @covers \App\Repositories\Admin\AccountTagRepository::__construct
     * @covers \App\Repositories\Admin\AccountTagRepository::update
     */
    public function iShouldBeAbleToUpdateTag()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $tag = factory(AccountTag::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $response = $this->get(route('admin.account_tags.edit', [$account, $tag]));

        $response->assertStatus(200);

        $data = $tag->toArray();
        $data['_token'] = csrf_token();
        $data['title'] = 'Updated title';

        $response = $this->put(route('admin.account_tags.update', [$account, $tag]), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_tags.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The tag has been successfully updated.',
            $flash['message']
        );

        $tag->refresh();
        $this->assertEquals($data['title'], $tag->title);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountTagController::__construct
     * @covers \App\Http\Controllers\Admin\AccountTagController::destroy
     * @covers \App\Repositories\Admin\Repository::destroy
     * @covers \App\Repositories\Admin\AccountTagRepository::destroy
     */
    public function iShouldBeAbleToDestroyTag()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $tag = factory(AccountTag::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $data = [];
        $data['_token'] = csrf_token();
        $data['_method'] = 'delete';

        $response = $this->delete(route('admin.account_tags.destroy', [$account, $tag]), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_tags.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The tag has been successfully deleted.',
            $flash['message']
        );

        $tag->refresh();
        $this->assertTrue($tag->trashed());
    }

}
