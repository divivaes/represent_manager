<?php

namespace Tests\Feature;

use App\Models\Charge;
use App\Models\Plan;
use App\Models\User;
use App\Services\Client\BraintreeService;
use Carbon\Carbon;
use Mockery;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChargeControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\ChargeController::__construct
     * @covers \App\Http\Controllers\Admin\ChargeController::index
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\ChargeRepository::__construct
     * @covers \App\Repositories\Admin\ChargeRepository::getRecords
     * @covers \App\Repositories\Admin\ChargeRepository::getStatistics
     * @covers \App\Repositories\Admin\ChargeRepository::prepareFilterQuery
     * @covers \App\Repositories\Admin\Traits\GetStatisticsTrait::getStatistics
     * @covers \App\Services\Admin\ExportService::__construct
     * @covers \App\Services\Admin\ExportService::export
     */
    public function iShouldBeAbleToSeeAllCharges()
    {
        $charge1 = factory(Charge::class)->create();
        $charge2 = factory(Charge::class)->create();

        $this->loginAsAdmin();

        $response = $this->get(route('admin.charges.index'));

        $response->assertStatus(200);
        $response->assertSee($charge1->user->full_name);
        $response->assertSee($charge2->user->full_name);

        $this->get(route('admin.charges.index', ['export' => 1]));
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\ChargeController::__construct
     * @covers \App\Http\Controllers\Admin\ChargeController::edit
     * @covers \App\Http\Controllers\Admin\ChargeController::pay
     * @covers \App\Repositories\Admin\Repository::update
     * @covers \App\Repositories\Admin\ChargeRepository::update
     * @covers \App\Repositories\Admin\ChargeRepository::__construct
     */
    public function iShouldBeAbleToPayChargeWithIntendedStatus()
    {
        $charge = factory(Charge::class)->create([
            'status' => 'Intended',
        ]);

        $this->loginAsAdmin();

        $braintreeResponse = $this->getFixture('Braintree/transaction.sale.json', false);

        $mock = Mockery::mock(BraintreeService::class);
        $mock->shouldReceive('gateway')->andReturnSelf();
        $mock->shouldReceive('transaction')->andReturnSelf();
        $mock->shouldReceive('sale')->andReturn($braintreeResponse);

        $this->app->instance(BraintreeService::class, $mock);

        $data = $charge->toArray();
        $data['_token'] = csrf_token();

        $response = $this->post(route('admin.charges.pay', $charge), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.charges.index'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The charge has been successfully updated.',
            $flash['message']
        );

        $charge->refresh();
        $this->assertEquals('Authorized', $charge->status);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\ChargeController::__construct
     * @covers \App\Http\Controllers\Admin\ChargeController::edit
     * @covers \App\Http\Controllers\Admin\ChargeController::pay
     * @covers \App\Repositories\Admin\Repository::update
     * @covers \App\Repositories\Admin\ChargeRepository::update
     * @covers \App\Repositories\Admin\ChargeRepository::__construct
     */
    public function iShouldNotBeAbleToPayChargeWithoutIntendedStatus()
    {
        $charge = factory(Charge::class)->create([
            'status' => 'Authorized',
        ]);

        $this->loginAsAdmin();

        $response = $this->get(route('admin.charges.edit', $charge));

        $response->assertStatus(200);

        $data = $charge->toArray();
        $data['_token'] = csrf_token();

        $response = $this->post(route('admin.charges.pay', $charge), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.charges.index'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('error', $flash['level']);
        $this->assertEquals(
            'The charge has been already paid.',
            $flash['message']
        );
    }
}
