<?php

namespace Tests\Feature;

use App\Models\Plan;
use App\Models\User;
use App\Models\Subscription;
use App\Services\Client\BraintreeService;
use Carbon\Carbon;
use Mockery;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SubscriptionControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\SubscriptionController::__construct
     * @covers \App\Http\Controllers\Admin\SubscriptionController::index
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\SubscriptionRepository::__construct
     * @covers \App\Repositories\Admin\SubscriptionRepository::getRecords
     * @covers \App\Repositories\Admin\SubscriptionRepository::getStatistics
     * @covers \App\Repositories\Admin\SubscriptionRepository::prepareFilterQuery
     * @covers \App\Repositories\Admin\Traits\GetStatisticsTrait::getStatistics
     * @covers \App\Services\Admin\ExportService::__construct
     * @covers \App\Services\Admin\ExportService::export
     */
    public function iShouldBeAbleToSeeAllSubscriptions()
    {
        $subscription1 = factory(Subscription::class)->create();
        $subscription2 = factory(Subscription::class)->create();

        $this->loginAsAdmin();

        $response = $this->get(route('admin.subscriptions.index'));

        $response->assertStatus(200);
        $response->assertSee($subscription1->user->full_name);
        $response->assertSee($subscription2->user->full_name);

        $this->get(route('admin.subscriptions.index', ['export' => 1]));
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\SubscriptionController::__construct
     * @covers \App\Http\Controllers\Admin\SubscriptionController::edit
     * @covers \App\Http\Controllers\Admin\SubscriptionController::pay
     * @covers \App\Repositories\Admin\Repository::update
     * @covers \App\Repositories\Admin\SubscriptionRepository::update
     * @covers \App\Repositories\Admin\SubscriptionRepository::__construct
     */
    public function iShouldBeAbleToPaySubscriptionWithIntendedStatus()
    {
        $subscription = factory(Subscription::class)->create([
            'status' => 'Intended',
            'braintree_subscription_first_billing_date' => Carbon::now()->addDays(30),
        ]);

        $this->loginAsAdmin();

        $braintreeResponse = $this->getFixture('Braintree/subscription.create.json', false);

        $mock = Mockery::mock(BraintreeService::class);
        $mock->shouldReceive('gateway')->andReturnSelf();
        $mock->shouldReceive('subscription')->andReturnSelf();
        $mock->shouldReceive('create')->andReturn($braintreeResponse);

        $this->app->instance(BraintreeService::class, $mock);

        $data = $subscription->toArray();
        $data['_token'] = csrf_token();

        $response = $this->post(route('admin.subscriptions.pay', $subscription), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.subscriptions.index'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The subscription has been successfully updated.',
            $flash['message']
        );

        $subscription->refresh();
        $this->assertEquals($braintreeResponse->subscription->status, $subscription->status);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\SubscriptionController::__construct
     * @covers \App\Http\Controllers\Admin\SubscriptionController::edit
     * @covers \App\Http\Controllers\Admin\SubscriptionController::pay
     * @covers \App\Repositories\Admin\Repository::update
     * @covers \App\Repositories\Admin\SubscriptionRepository::update
     * @covers \App\Repositories\Admin\SubscriptionRepository::__construct
     */
    public function iShouldNotBeAbleToPaySubscriptionWithoutIntendedStatus()
    {
        $subscription = factory(Subscription::class)->create([
            'status' => 'Active',
        ]);

        $this->loginAsAdmin();

        $response = $this->get(route('admin.subscriptions.edit', $subscription));

        $response->assertStatus(200);

        $data = $subscription->toArray();
        $data['_token'] = csrf_token();

        $response = $this->post(route('admin.subscriptions.pay', $subscription), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.subscriptions.index'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('error', $flash['level']);
        $this->assertEquals(
            'The subscription has been already paid.',
            $flash['message']
        );
    }
}
