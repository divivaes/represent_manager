<?php

namespace Tests\Feature;

use App\Models\Account;
use App\Models\AccountContact;
use App\Models\AccountMarketingTemplate;
use App\Models\AccountUser;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountMarketingTemplateControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::__construct
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::index
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\AccountMarketingTemplateRepository::__construct
     */
    public function iShouldBeAbleToSeeAllMarketingTemplates()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $template1 = factory(AccountMarketingTemplate::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);
        $template2 = factory(AccountMarketingTemplate::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $response = $this->get(route('admin.account_marketing_templates.index', $account));

        $response->assertStatus(200);
        $response->assertSee($template1->title);
        $response->assertSee($template2->title);
    }


    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::__construct
     * @covers \App\Http\Requests\Admin\AccountMarketingTemplateRequest::rules
     * @covers \App\Http\Requests\Admin\AccountMarketingTemplateRequest::authorize
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::create
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::store
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\Repository::create
     * @covers \App\Repositories\Admin\AccountMarketingTemplateRepository::create
     */
    public function iShouldBeAbleToCreateMarketingTemplate()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $response = $this->get(route('admin.account_marketing_templates.create', $account));

        $response->assertStatus(200);

        $data = [
            '_token' => csrf_token(),
            'user_id' => $user->id,
            'type' => 'Email',
            'title' => 'Test title',
            'content' => 'Test content'
        ];

        $response = $this->post(route('admin.account_marketing_templates.store', $account), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_marketing_templates.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The template has been successfully created.',
            $flash['message']
        );

        $template = AccountMarketingTemplate::query()->first();

        $this->assertNotNull($template);
        $this->assertNotNull($template->uuid);
        $this->assertEquals($template->title, $data['title']);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::__construct
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::edit
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::update
     * @covers \App\Repositories\Admin\Repository::update
     * @covers \App\Repositories\Admin\AccountMarketingTemplateRepository::update
     */
    public function iShouldBeAbleToUpdateMarketingTemplate()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $template = factory(AccountMarketingTemplate::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $response = $this->get(route('admin.account_marketing_templates.edit', [$account, $template]));

        $response->assertStatus(200);

        $data = $template->toArray();
        $data['_token'] = csrf_token();
        $data['title'] = 'This title will be updated';

        $response = $this->put(route('admin.account_marketing_templates.update', [$account, $template]), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_marketing_templates.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The template has been successfully updated.',
            $flash['message']
        );

        $template->refresh();
        $this->assertEquals($data['title'], $template->title);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::__construct
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::destroy
     * @covers \App\Repositories\Admin\Repository::destroy
     * @covers \App\Repositories\Admin\AccountMarketingTemplateRepository::destroy
     */
    public function iShouldBeAbleToDestroyMarketingTemplate()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $template = factory(AccountMarketingTemplate::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $data = [];
        $data['_token'] = csrf_token();
        $data['_method'] = 'delete';

        $response = $this->delete(route('admin.account_marketing_templates.destroy', [$account, $template]), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_marketing_templates.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The template has been successfully deleted.',
            $flash['message']
        );

        $template->refresh();
        $this->assertTrue($template->trashed());
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::__construct
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::prepare
     * @covers \App\Repositories\Admin\AccountMarketingTemplateRepository::__construct
     * @covers \App\Repositories\Admin\AccountMarketingTemplateRepository::prepare
     */
    public function iShouldBeAblePrepareMarketingTemplateForContacts()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $template = factory(AccountMarketingTemplate::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $response = $this->get(route('admin.account_marketing_templates.prepare', [$account, $template]));
        $response->assertStatus(200);
        $response->assertSee($template->title);
        $response->assertSee($template->content);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountMarketingTemplateController::send
     * @covers \App\Http\Requests\Admin\AccountMarketingTemplateRequest::authorize
     * @covers \App\Http\Requests\Admin\AccountMarketingTemplateRequest::rules
     * @covers \App\Repositories\Admin\CrmNotificationRepository::__construct
     * @covers \App\Repositories\Admin\CrmNotificationRepository::create
     * @covers \App\Repositories\Admin\Repository::prepare
     */
    public function iShouldBeAbleSetNotificationToContacts()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $template = factory(AccountMarketingTemplate::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id,
            'type' => 'email'
        ]);

        $contact = factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $data = [
            '_token' => csrf_token(),
            'user_id' => $user->id,
            'title' => $template->title,
            'type' => $template->type,
            'content' => $template->content,
            'contacts' => [
                '0' => $contact->id
            ],
            'sending_datetime' => now()->format('Y-m-d H:i:s'),
        ];

        $response = $this->post(route('admin.account_marketing_templates.send', [$account, $template]), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.account_marketing_templates.index', $account));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The mail will be send to contacts on a listed date.',
            $flash['message']
        );
    }
}
