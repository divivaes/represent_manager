<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DashboardControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\DashboardController::index
     */
    public function iShouldBeAbleToSeeDashboard()
    {
        $this->loginAsAdmin();

        $response = $this->get(route('admin.dashboard.index'));

        $response->assertStatus(200);
        $response->assertSee('Dashboard');
    }
}
