<?php

namespace Tests\Feature;

use App\Models\Account;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountController::__construct
     * @covers \App\Http\Controllers\Admin\AccountController::index
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\AccountRepository::__construct
     * @covers \App\Repositories\Admin\AccountRepository::getRecords
     * @covers \App\Repositories\Admin\AccountRepository::getStatistics
     * @covers \App\Repositories\Admin\AccountRepository::prepareFilterQuery
     * @covers \App\Repositories\Admin\Traits\GetStatisticsTrait::getStatistics
     * @covers \App\Services\Admin\ExportService::__construct
     * @covers \App\Services\Admin\ExportService::export
     */
    public function iShouldBeAbleToSeeAllAccounts()
    {
        $account1 = factory(Account::class)->create();
        $account2 = factory(Account::class)->create();

        $this->loginAsAdmin();

        $response = $this->get(route('admin.accounts.index'));

        $response->assertStatus(200);
        $response->assertSee($account1->name);
        $response->assertSee($account2->name);

        $response = $this->get(route('admin.accounts.index', ['export' => 1]));
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountController::__construct
     * @covers \App\Http\Requests\Admin\AccountRequest::rules
     * @covers \App\Http\Requests\Admin\AccountRequest::authorize
     * @covers \App\Http\Controllers\Admin\AccountController::edit
     * @covers \App\Http\Controllers\Admin\AccountController::update
     * @covers \App\Repositories\Admin\Repository::update
     * @covers \App\Repositories\Admin\AccountRepository::update
     * @covers \App\Repositories\Admin\AccountRepository::__construct
     */
    public function iShouldBeAbleToUpdateAccount()
    {
        $account = factory(Account::class)->create();

        $this->loginAsAdmin();

        $response = $this->get(route('admin.accounts.edit', $account));

        $response->assertStatus(200);

        $data = $account->toArray();
        $data['_token'] = csrf_token();
        $data['name'] = 'Test name';
        $data['microsite'] = 'test.com';
        $data['microsite_is_active'] = 1;

        $response = $this->put(route('admin.accounts.update', $account), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.accounts.index'));

        $flash = session('flash_notification')->first();
        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The account has been successfully updated.',
            $flash['message']
        );

        $account->refresh();
        $this->assertEquals($data['name'], $account->name);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AccountController::__construct
     * @covers \App\Http\Requests\Admin\AccountRequest::rules
     * @covers \App\Http\Requests\Admin\AccountRequest::authorize
     * @covers \App\Http\Controllers\Admin\AccountController::edit
     * @covers \App\Http\Controllers\Admin\AccountController::update
     * @covers \App\Repositories\Admin\Repository::update
     */
    public function iShouldBeAbleToUpdateAccountStatus()
    {
        $account = factory(Account::class)->create([
            'is_active' => true,
        ]);

        $this->loginAsAdmin();

        $data = [];
        $data['_token'] = csrf_token();
        $data['_method'] = 'put';
        $data['is_active'] = 0;

        $response = $this->put(route('admin.accounts.update', $account), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.accounts.index'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The account has been successfully updated.',
            $flash['message']
        );

        $account->refresh();
        $this->assertEquals($data['is_active'], $account->is_active);
    }
}
