<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\UserController::__construct
     * @covers \App\Http\Controllers\Admin\UserController::index
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\UserRepository::__construct
     * @covers \App\Repositories\Admin\UserRepository::getRecords
     * @covers \App\Repositories\Admin\UserRepository::getStatistics
     * @covers \App\Repositories\Admin\UserRepository::prepareFilterQuery
     * @covers \App\Repositories\Admin\Traits\GetStatisticsTrait::getStatistics
     * @covers \App\Services\Admin\ExportService::__construct
     * @covers \App\Services\Admin\ExportService::export
     */
    public function iShouldBeAbleToSeeAllUsers()
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $this->loginAsAdmin();

        $response = $this->get(route('admin.users.index'));

        $response->assertStatus(200);
        $response->assertSee($user1->full_name);
        $response->assertSee($user2->full_name);

        $response = $this->get(route('admin.users.index', ['export' => 1]));
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\UserController::__construct
     * @covers \App\Http\Requests\Admin\UserRequest::rules
     * @covers \App\Http\Requests\Admin\UserRequest::authorize
     * @covers \App\Http\Controllers\Admin\UserController::create
     * @covers \App\Http\Controllers\Admin\UserController::store
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\Repository::create
     * @covers \App\Repositories\Admin\UserRepository::create
     * @covers \App\Repositories\Admin\UserRepository::__construct
     * @covers \App\Models\Boot\UuidAble::bootUuidAble
     */
    public function iShouldBeAbleToCreateUser()
    {
        $this->loginAsAdmin();

        $response = $this->get(route('admin.users.create'));

        $response->assertStatus(200);

        $data = [
            '_token' => csrf_token(),
            'first_name' => 'Alex',
            'last_name' => 'Kolev',
            'phone' => '+0123456789',
            'email' => 'test@test.com',
            'role' => 'admin',
            'is_active' => true,
        ];

        $response = $this->post(route('admin.users.store'), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.users.index'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The user has been successfully created.',
            $flash['message']
        );

        $user = User::byEmail($data['email'])
            ->byFullName(sprintf('%s %s', $data['first_name'], $data['last_name']))
            ->first();

        $this->assertNotNull($user);
        $this->assertNotNull($user->uuid);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\UserController::__construct
     * @covers \App\Http\Requests\Admin\UserRequest::rules
     * @covers \App\Http\Requests\Admin\UserRequest::authorize
     * @covers \App\Http\Controllers\Admin\UserController::edit
     * @covers \App\Http\Controllers\Admin\UserController::update
     * @covers \App\Repositories\Admin\Repository::update
     * @covers \App\Repositories\Admin\UserRepository::update
     * @covers \App\Repositories\Admin\UserRepository::__construct
     */
    public function iShouldBeAbleToUpdateUser()
    {
        $user = factory(User::class)->create();

        $this->loginAsAdmin();

        $response = $this->get(route('admin.users.edit', $user));

        $response->assertStatus(200);

        $data = $user->toArray();
        $data['_token'] = csrf_token();
        $data['email'] = 'test@test.com';

        $response = $this->put(route('admin.users.update', $user), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.users.index'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The user has been successfully updated.',
            $flash['message']
        );

        $user->refresh();
        $this->assertEquals($data['email'], $user->email);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\UserController::__construct
     * @covers \App\Http\Requests\Admin\UserRequest::rules
     * @covers \App\Http\Requests\Admin\UserRequest::authorize
     * @covers \App\Http\Controllers\Admin\UserController::edit
     * @covers \App\Http\Controllers\Admin\UserController::update
     * @covers \App\Repositories\Admin\Repository::update
     */
    public function iShouldBeAbleToUpdateUserStatus()
    {
        $user = factory(User::class)->create();

        $this->loginAsAdmin();

        $data = [];
        $data['_token'] = csrf_token();
        $data['_method'] = 'put';
        $data['is_active'] = 0;

        $response = $this->put(route('admin.users.update', $user), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.users.index'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The user has been successfully updated.',
            $flash['message']
        );

        $user->refresh();
        $this->assertEquals($data['is_active'], $user->is_active);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\UserController::__construct
     * @covers \App\Http\Controllers\Admin\UserController::destroy
     * @covers \App\Repositories\Admin\Repository::destroy
     */
    public function iShouldBeAbleToDestroyUser()
    {
        $user = factory(User::class)->create();

        $this->loginAsAdmin();

        $data = [];
        $data['_token'] = csrf_token();
        $data['_method'] = 'delete';

        $response = $this->delete(route('admin.users.destroy', $user), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.users.index'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The user has been successfully deleted.',
            $flash['message']
        );

        $user->refresh();
        $this->assertTrue($user->trashed());
    }
}
