<?php

namespace Tests\Feature;

use App\Models\Plan;
use App\Services\Client\BraintreeService;
use Mockery;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PlanControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\PlanController::__construct
     * @covers \App\Http\Controllers\Admin\PlanController::index
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\PlanRepository::__construct
     * @covers \App\Repositories\Admin\PlanRepository::getRecords
     * @covers \App\Repositories\Admin\PlanRepository::getStatistics
     * @covers \App\Repositories\Admin\PlanRepository::prepareFilterQuery
     * @covers \App\Repositories\Admin\Traits\GetStatisticsTrait::getStatistics
     * @covers \App\Services\Admin\ExportService::__construct
     * @covers \App\Services\Admin\ExportService::export
     */
    public function iShouldBeAbleToSeeAllPlans()
    {
        $plan1 = factory(Plan::class)->create();
        $plan2 = factory(Plan::class)->create();

        $this->loginAsAdmin();

        $response = $this->get(route('admin.plans.index'));

        $response->assertStatus(200);
        $response->assertSee($plan1->name);
        $response->assertSee($plan2->name);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\PlanController::__construct
     * @covers \App\Http\Requests\Admin\PlanRequest::rules
     * @covers \App\Http\Requests\Admin\PlanRequest::authorize
     * @covers \App\Http\Controllers\Admin\PlanController::edit
     * @covers \App\Http\Controllers\Admin\PlanController::update
     * @covers \App\Repositories\Admin\Repository::update
     * @covers \App\Repositories\Admin\PlanRepository::update
     * @covers \App\Repositories\Admin\PlanRepository::__construct
     */
    public function iShouldBeAbleToUpdatePlan()
    {
        $plan = factory(Plan::class)->create();

        $this->loginAsAdmin();

        $braintreeResponse = $this->getFixture('Braintree/plan.all.json', false);

        $mock = Mockery::mock(BraintreeService::class);
        $mock->shouldReceive('gateway')->andReturnSelf();
        $mock->shouldReceive('plan')->andReturnSelf();
        $mock->shouldReceive('all')->andReturn($braintreeResponse);

        $this->app->instance(BraintreeService::class, $mock);

        $response = $this->get(route('admin.plans.edit', $plan));

        $response->assertStatus(200);

        $data = $plan->toArray();
        $data['_token'] = csrf_token();
        $data['name'] = 'Test name';

        $response = $this->put(route('admin.plans.update', $plan), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.plans.index'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('success', $flash['level']);
        $this->assertEquals(
            'The plan has been successfully updated.',
            $flash['message']
        );

        $plan->refresh();
        $this->assertEquals($data['name'], $plan->name);
    }
}
