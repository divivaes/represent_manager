<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Providers\AppServiceProvider::register
     * @covers \App\Providers\AppServiceProvider::boot
     *
     * @covers \App\Providers\RouteServiceProvider::boot
     * @covers \App\Providers\RouteServiceProvider::map
     * @covers \App\Providers\RouteServiceProvider::mapWebRoutes
     * @covers \App\Providers\RouteServiceProvider::mapAdminRoutes
     *
     * @covers \App\Providers\EventServiceProvider::boot
     *
     * @covers \App\Providers\AuthServiceProvider::boot
     *
     * @covers \App\Http\Controllers\Admin\AuthController::login
     */
    public function iShouldBeAbleToSeeLoginPageAsGuest()
    {
        $response = $this->get(route('admin.auth.login'));

        $response->assertStatus(200);
        $response->assertSeeText('E-mail address');
        $response->assertSeeText('Password');
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AuthController::authenticate
     * @covers \App\Listeners\SuccessfulLogin::handle
     */
    public function iShouldBeAbleToLoginWithActiveAdminAccount()
    {
        $user = factory(User::class)->create();

        $this->assertNull($user->last_login_at);

        $data = [
            '_token' => csrf_token(),
            'email' => $user->email,
            'password' => 'secret',
        ];

        $response = $this->post(route('admin.auth.authenticate'), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.dashboard.index'));

        $this->assertTrue(Auth::check());

        $user->refresh();

        $this->assertNotNull($user->last_login_at);
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AuthController::authenticate
     */
    public function iShouldNotBeAbleToLoginWithUserAccount()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $data = [
            '_token' => csrf_token(),
            'email' => $user->email,
            'password' => 'secret',
        ];

        $response = $this->post(route('admin.auth.authenticate'), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.auth.login'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('danger', $flash['level']);
        $this->assertEquals(
            'The credentials you supplied were not correct or did not grant access to this resource.',
            $flash['message']
        );

        $this->assertFalse(Auth::check());
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AuthController::authenticate
     */
    public function iShouldNotBeAbleToLoginWithInactiveAccount()
    {
        $user = factory(User::class)->create([
            'is_active' => false
        ]);

        $data = [
            '_token' => csrf_token(),
            'email' => $user->email,
            'password' => 'secret',
        ];

        $response = $this->post(route('admin.auth.authenticate'), $data);

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.auth.login'));

        $flash = session('flash_notification')->first();

        $this->assertEquals('danger', $flash['level']);
        $this->assertEquals(
            'The credentials you supplied were not correct or did not grant access to this resource.',
            $flash['message']
        );

        $this->assertFalse(Auth::check());
    }

    /**
     * @test
     * @covers \App\Http\Controllers\Admin\AuthController::logout
     */
    public function iShouldBeAbleToLogout()
    {
        $user = factory(User::class)->create();

        $data = [
            '_token' => csrf_token(),
            'email' => $user->email,
            'password' => 'secret',
        ];

        $this->post(route('admin.auth.authenticate'), $data);

        $this->assertTrue(Auth::check());

        $response = $this->get(route('admin.auth.logout'));

        $response->assertStatus(302);
        $response->assertRedirect(route('admin.auth.login'));

        $this->assertFalse(Auth::check());
    }

    /**
     * @test
     * @covers \App\Http\Middleware\AuthOnlyAdmin::handle
     */
    public function iShouldBeAbleToAccessAdminPagesAsAdmin()
    {
        $user = factory(User::class)->create();

        $data = [
            '_token' => csrf_token(),
            'email' => $user->email,
            'password' => 'secret',
        ];

        $this->post(route('admin.auth.authenticate'), $data);

        $this->assertTrue(Auth::check());

        $response = $this->get(route('admin.dashboard.index'));
        $response->assertStatus(200);
        $response->assertSee(sprintf('Welcome, %s', $user->email));
    }

    /**
     * @test
     * @covers \App\Http\Middleware\AuthOnlyAdmin::handle
     */
    public function iShouldNotBeAbleToAccessAdminPagesAsGuest()
    {
        $this->assertFalse(Auth::check());

        $response = $this->get(route('admin.dashboard.index'));
        $response->assertStatus(302);
        $response->assertRedirect(route('admin.auth.login'));
    }

    /**
     * @test
     * @covers \App\Http\Middleware\AuthOnlyAdmin::handle
     */
    public function iShouldNotBeAbleToAccessAdminPagesAsUser()
    {
        $user = factory(User::class)->create([
            'role' => 'user',
        ]);

        $this->be($user);

        $this->assertTrue(Auth::check());

        $response = $this->get(route('admin.dashboard.index'));
        $response->assertStatus(302);
        $response->assertRedirect(route('admin.auth.login'));
    }

    /**
     * @test
     * @covers \App\Http\Middleware\AuthNotAdmin::handle
     */
    public function iShouldNotBeAbleToSeeLoginPageAsAdmin()
    {
        $user = factory(User::class)->create();

        $data = [
            '_token' => csrf_token(),
            'email' => $user->email,
            'password' => 'secret',
        ];

        $this->post(route('admin.auth.authenticate'), $data);

        $this->assertTrue(Auth::check());

        $response = $this->get(route('admin.auth.login'));
        $response->assertStatus(302);
        $response->assertRedirect(route('admin.dashboard.index'));
    }
}
