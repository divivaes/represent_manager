<?php

namespace Tests\Feature\Models;

use App\Models\Account;
use App\Models\AccountUser;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountUserTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * @test
     * @covers \App\Models\AccountUser::account
     * @covers \App\Models\AccountUser::user
     */
    public function iShouldBeAbleGetAccountOwner()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        $accountUser = factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $this->assertNotEmpty($user);
        $this->assertNotEmpty($account);
        $this->assertNotEmpty($accountUser);

        $this->assertDatabaseHas('account_user', [
            'id' => $accountUser->id,
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $this->assertEquals($account->id, $accountUser->account_id);
        $this->assertEquals($account->id, $accountUser->account->id);

        $this->assertEquals($user->id, $accountUser->user_id);
        $this->assertEquals($user->id, $accountUser->user->id);
    }
}
