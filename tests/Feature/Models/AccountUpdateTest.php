<?php

namespace Tests\Feature\Models;

use App\Models\Account;
use App\Models\AccountUpdate;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountUpdateTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Models\AccountUpdate::account
     */
    public function iShouldBeAbleToAccount()
    {
        $account = factory(Account::class)->create();

        $update = factory(AccountUpdate::class)->create([
            'account_id' => $account->id
        ]);

        $this->assertEquals($account->id, $update->account->id);
    }

    /**
     * @test
     * @covers \App\Models\AccountUpdate::user
     */
    public function iShouldBeAbleGetUser()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $update = factory(AccountUpdate::class)->create([
            'user_id' => $user->id
        ]);

        $this->assertEquals($user->id, $update->user->id);
    }
}
