<?php

namespace Tests\Feature\Models;

use App\Models\User;
use App\Models\UserProvider;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserProviderTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Models\UserProvider::user
     */
    public function iShouldBeAbleToGetUser()
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $userProvider1 = factory(UserProvider::class)->create([
            'user_id' => $user1->id,
        ]);

        $userProvider2 = factory(UserProvider::class)->create([
            'user_id' => $user2->id,
        ]);

        $this->assertEquals($user1->id, $userProvider1->user->id);
        $this->assertEquals($user2->id, $userProvider2->user->id);
    }
}
