<?php

namespace Tests\Feature\Models\Boot;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     * @covers \App\Models\Boot\GeoAble::bootGeoAble
     */
    public function geoDetailsShouldBePopulated()
    {
        $user = factory(User::class)->create([
            'submitter_ip' => null,
        ]);

        $user->save();

        $this->assertEquals('127.0.0.1', $user->submitter_ip);
    }
}
