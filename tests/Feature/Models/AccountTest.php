<?php

namespace Tests\Feature\Models;

use App\Models\Account;
use App\Models\AccountContact;
use App\Models\AccountMarketingTemplate;
use App\Models\AccountTag;
use App\Models\AccountUpdate;
use App\Models\AccountUser;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Models\Account::scopeByUser
     */
    public function iShouldBeAbleToFilterByUser()
    {
        $user1 = factory(User::class)->create([
            'role' => 'user'
        ]);

        $user2 = factory(User::class)->create([
            'role' => 'user'
        ]);

        $accounts = Account::byUser($user1->id)->get();

        $this->assertCount(1, $accounts);
        $this->assertEquals($user1->id, $accounts[0]->users->first()->id);

        $accounts = Account::byUser($user2->id)->get();

        $this->assertCount(1, $accounts);
        $this->assertEquals($user2->id, $accounts[0]->users->first()->id);
    }

    /**
     * @test
     * @covers \App\Models\Account::users
     */
    public function iShouldBeAbleGetAccountOwner()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        $accountUser = factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $this->assertNotEmpty($user);
        $this->assertNotEmpty($account);
        $this->assertNotEmpty($accountUser);

        $this->assertDatabaseHas('account_user', [
            'id' => $accountUser->id,
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $this->assertEquals($account->id, $accountUser->account_id);
        $this->assertEquals($account->id, $accountUser->account->id);

        $this->assertInstanceOf(Collection::class, $account->users);
    }

    /**
     * @test
     * @covers \App\Models\Account::contacts
     */
    public function iShouldBeAbleGetAccountContacts()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $this->assertEquals($account->id, $account->contacts[0]->account_id);
        $this->assertEquals($user->id, $account->contacts[0]->user_id);

        $this->assertInstanceOf(Collection::class, $account->contacts);
    }

    /**
     * @test
     * @covers \App\Models\Account::updates
     */
    public function iShouldBeAbleGetAccountUpdates()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        factory(AccountUpdate::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $this->assertCount(1, $account->updates);

        $this->assertEquals($account->id, $account->updates[0]->account_id);
        $this->assertEquals($user->id, $account->updates[0]->user_id);

        $this->assertInstanceOf(Collection::class, $account->updates);
    }

    /**
     * @test
     * @covers \App\Models\Account::marketingTemplates
     */
    public function iShouldBeAbleGetAccountMarketingTemplates()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        factory(AccountMarketingTemplate::class)->create([
            'user_id'    => $user->id,
            'account_id' => $account->id
        ]);

        $this->assertEquals($account->id, $account->marketingTemplates[0]->account_id);
        $this->assertEquals($user->id, $account->marketingTemplates[0]->user_id);

        $this->assertInstanceOf(Collection::class, $account->marketingTemplates);
    }

    /**
     * @test
     * @covers \App\Models\Account::tags
     */
    public function iShouldBeAbleGetTags()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        factory(AccountTag::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $this->assertEquals($account->id, $account->tags[0]->account_id);

        $this->assertInstanceOf(Collection::class, $account->tags);
    }
}
