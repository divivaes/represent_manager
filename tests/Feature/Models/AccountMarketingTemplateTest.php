<?php

namespace Tests\Feature\Models;

use App\Models\Account;
use App\Models\AccountContact;
use App\Models\AccountUser;
use App\Models\CrmNotification;
use App\Models\User;
use App\Models\AccountMarketingTemplate;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountMarketingTemplateTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Models\AccountMarketingTemplate::user
     */
    public function iShouldBeAbleGetByUser()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $template = factory(AccountMarketingTemplate::class)->create([
            'account_id' => $account->id,
            'user_id'    => $user->id
        ]);

        $this->assertEquals($template->user->id, $user->id);
    }

    /**
     * @test
     * @covers \App\Models\AccountMarketingTemplate::account
     */
    public function iShouldBeAbleGetByAccount()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $template = factory(AccountMarketingTemplate::class)->create([
            'account_id' => $account->id,
            'user_id'    => $user->id
        ]);

        $this->assertEquals($template->account->id, $account->id);
    }
}
