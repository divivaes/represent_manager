<?php

namespace Tests\Feature\Models;

use App\Models\Property;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PropertyTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     * @covers \App\Models\Property::scopeByAddress
     * @covers \App\Models\Scopes\ByAddressScope::scopeByAddress
     */
    public function iShouldBeAbleToFilterByAddress()
    {
        $property1 = factory(Property::class)->create([
            'address' => 'Address line 1'
        ]);

        $property2 = factory(Property::class)->create([
            'address' => 'Address line 2'
        ]);

        $properties = Property::byAddress($property1->address)->get();

        $this->assertEquals(1, $properties->count());
        $this->assertEquals($property1->id, $properties[0]->id);

        $properties =  Property::byAddress($property2->address)->get();

        $this->assertEquals(1, $properties->count());
        $this->assertEquals($property2->id, $properties[0]->id);
    }

    /**
     * @test
     * @covers \App\Models\Property::scopeByType
     * @covers \App\Models\Scopes\ByTypeScope::scopeByType
     */
    public function iShouldBeAbleToFilterByType()
    {
        $property1 = factory(Property::class)->create([
            'type' => 'Flat'
        ]);

        $property2 = factory(Property::class)->create([
            'type' => 'Office'
        ]);

        $properties = Property::byType($property1->type)->get();

        $this->assertEquals(1, $properties->count());
        $this->assertEquals($property1->id, $properties[0]->id);

        $properties =  Property::byType($property2->type)->get();

        $this->assertEquals(1, $properties->count());
        $this->assertEquals($property2->id, $properties[0]->id);
    }
}
