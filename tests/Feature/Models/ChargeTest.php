<?php

namespace Tests\Feature\Models;

use App\Models\Account;
use App\Models\AccountContact;
use App\Models\AccountMarketingTemplate;
use App\Models\AccountUpdate;
use App\Models\AccountUser;
use App\Models\Charge;
use App\Models\Plan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChargeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Models\Charge::user
     */
    public function iShouldBeAbleGetUser()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $charge = factory(Charge::class)->create([
            'user_id' => $user->id
        ]);

        $this->assertEquals($user->id, $charge->user->id);
    }

    /**
     * @test
     * @covers \App\Models\Charge::plan
     */
    public function iShouldBeAbleGetPlan()
    {
        $plan = factory(Plan::class)->create();

        $charge = factory(Charge::class)->create([
            'plan_id' => $plan->id
        ]);

        $this->assertEquals($plan->id, $charge->plan->id);
    }

    /**
     * @test
     * @covers \App\Models\Charge::scopeByPlan
     */
    public function iShouldBeAbleToFilterByPlan()
    {
        $charge1 = factory(Charge::class)->create();

        $charge2 = factory(Charge::class)->create();

        $charges = Charge::byPlan($charge1->plan->id)->get();

        $this->assertCount(1, $charges);
        $this->assertEquals($charge1->plan->id, $charges[0]->plan->id);

        $charges = Charge::byPlan($charge2->plan->id)->get();

        $this->assertCount(1, $charges);
        $this->assertEquals($charge2->plan->id, $charges[0]->plan->id);
    }

    /**
     * @test
     * @covers \App\Models\Charge::getStartAtToDatetimeAttribute
     */
    public function iShouldBeAbleToGetStartAtDate()
    {
        $date = Carbon::now();

        $charge = factory(Charge::class)->create([
            'start_at' => $date,
        ]);

        $this->assertEquals(
            $charge->start_at_to_datetime,
            Carbon::parse($date)->toRfc822String()
        );

        $charge = factory(Charge::class)->create([
            'start_at' => null,
        ]);

        $this->assertEquals($charge->start_at_to_datetime, null);
    }

    /**
     * @test
     * @covers \App\Models\Charge::getEndAtToDatetimeAttribute
     */
    public function iShouldBeAbleToGetEndAtDate()
    {
        $date = Carbon::now();

        $charge = factory(Charge::class)->create([
            'end_at' => $date,
        ]);

        $this->assertEquals(
            $charge->end_at_to_datetime,
            Carbon::parse($date)->toRfc822String()
        );

        $charge = factory(Charge::class)->create([
            'end_at' => null,
        ]);

        $this->assertEquals($charge->end_at_to_datetime, null);
    }

    /**
     * @test
     * @covers \App\Models\Charge::getPriceToHumanAttribute
     */
    public function iShouldBeAbleToGetPrice()
    {
        $price = 1;

        $charge = factory(Charge::class)->create([
            'price' => $price,
        ]);

        $this->assertEquals(
            $charge->price_to_human,
            sprintf('$%s USD', $price)
        );

        $charge = factory(Charge::class)->create([
            'price' => null,
        ]);

        $this->assertEquals($charge->price_to_human, null);
    }
}
