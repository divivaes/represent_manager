<?php

namespace Tests\Feature\Models;

use App\Models\Account;
use App\Models\AccountContact;
use App\Models\AccountTag;
use App\Models\AccountUser;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountTagTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     * @covers \App\Models\AccountTag::user
     */
    public function iShouldBeAbleGetByUser()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $tag = factory(AccountTag::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $this->assertEquals($tag->user->id, $user->id);
    }

    /**
     * @test
     * @covers \App\Models\AccountTag::account
     */
    public function iShouldBeAbleGetByAccount()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $tag = factory(AccountTag::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $this->assertEquals($tag->account->id, $account->id);
    }

    /**
     * @test
     * @covers \App\Models\AccountTag::contacts
     */
    public function iShouldBeAbleGetByContacts()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $tag = factory(AccountTag::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $this->assertInstanceOf(Collection::class, $tag->contacts);
    }
}
