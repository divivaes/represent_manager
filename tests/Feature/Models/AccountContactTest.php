<?php

namespace Tests\Feature;

use App\Models\Account;
use App\Models\AccountContactTag;
use App\Models\AccountTag;
use App\Models\AccountUser;
use App\Models\User;
use App\Models\AccountContact;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccountContactTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     * @covers \App\Models\AccountContact::scopeSort
     * @covers \App\Models\Scopes\SortScope::scopeSort
     */
    public function iShouldBeAbleToSort()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $contact1 = factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id'    => $user->id
        ]);
        $contact2 = factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id'    => $user->id
        ]);

        $contacts = AccountContact::sort()->get();

        $this->assertEquals($contact2->id, $contacts[0]->id);
        $this->assertEquals($contact1->id, $contacts[1]->id);
    }

    /**
     * @test
     * @covers \App\Models\AccountContact::user
     */
    public function iShouldBeAbleGetByUser()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $contact = factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $this->assertEquals($contact->user->id, $user->id);
    }

    /**
     * @test
     * @covers \App\Models\AccountContact::account
     */
    public function iShouldBeAbleGetByAccount()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $contact = factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $this->assertEquals($contact->account->id, $account->id);
    }

    /**
     * @test
     * @covers \App\Models\AccountContact::tags
     */
    public function iShouldBeAbleGetTags()
    {
        $user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'user_id' => $user->id,
            'account_id' => $account->id
        ]);

        $contact = factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $this->assertInstanceOf(Collection::class, $contact->tags);
    }
}
