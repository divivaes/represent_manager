<?php

namespace Tests\Feature\Models;

use App\Models\Account;
use App\Models\AccountContact;
use App\Models\AccountMarketingTemplate;
use App\Models\AccountUpdate;
use App\Models\AccountUser;
use App\Models\Charge;
use App\Models\Plan;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PlanTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Models\Plan::scopeSort
     */
    public function iShouldBeAbleToGetPlansWithSort()
    {
        $plan1 = factory(Plan::class)->create();
        $plan2 = factory(Plan::class)->create();

        $plans = Plan::sort()->get();

        $this->assertCount(2, $plans);
        $this->assertEquals($plan1->id, $plans[0]->id);
        $this->assertEquals($plan2->id, $plans[1]->id);
    }

    /**
     * @test
     * @covers \App\Models\Plan::subscriptions
     */
    public function iShouldBeAbleToGetSubscriptions()
    {
        $plan = factory(Plan::class)->create();

        factory(Subscription::class, 3)->create([
            'plan_id' => $plan->id,
        ]);

        factory(Subscription::class, 2)->create();

        $this->assertCount(3, $plan->subscriptions);
    }

    /**
     * @test
     * @covers \App\Models\Plan::charges
     */
    public function iShouldBeAbleToGetCharges()
    {
        $plan = factory(Plan::class)->create();

        factory(Charge::class, 3)->create([
            'plan_id' => $plan->id,
        ]);

        factory(Charge::class, 2)->create();

        $this->assertCount(3, $plan->charges);
    }

    /**
     * @test
     * @covers \App\Models\Plan::getPriceToHumanAttribute
     */
    public function iShouldBeAbleToGetPrice()
    {
        $price = 1;

        $plan = factory(Plan::class)->create([
            'price' => $price,
        ]);

        $this->assertEquals(
            $plan->price_to_human,
            sprintf('$%s USD', $price)
        );

        $plan = factory(Plan::class)->create([
            'price' => null,
        ]);

        $this->assertEquals($plan->price_to_human, null);
    }
}
