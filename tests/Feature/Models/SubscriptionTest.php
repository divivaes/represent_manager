<?php

namespace Tests\Feature\Models;

use App\Models\Account;
use App\Models\AccountUser;
use App\Models\Plan;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SubscriptionTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     * @covers \App\Models\Subscription::user
     */
    public function iShouldBeAbleToGetSubscriptionUser()
    {
        $user = factory(User::class)->create();

        $subscription = factory(Subscription::class)->create([
            'user_id' => $user->id,
        ]);

        $this->assertEquals($subscription->user->id, $user->id);
    }

    /**
     * @test
     * @covers \App\Models\Subscription::plan
     */
    public function iShouldBeAbleToGetSubscriptionPlan()
    {
        $plan = factory(Plan::class)->create();

        $subscription = factory(Subscription::class)->create([
            'plan_id' => $plan->id,
        ]);

        $this->assertEquals($subscription->plan->id, $plan->id);
    }

    /**
     * @test
     * @covers \App\Models\Subscription::scopeByPlan
     */
    public function iShouldBeAbleToFilterByPlan()
    {
        $subscription1 = factory(Subscription::class)->create();

        $subscription2 = factory(Subscription::class)->create();

        $subscriptions = Subscription::byPlan($subscription1->plan->id)->get();

        $this->assertCount(1, $subscriptions);
        $this->assertEquals($subscription1->plan->id, $subscriptions[0]->plan->id);

        $subscriptions = Subscription::byPlan($subscription2->plan->id)->get();

        $this->assertCount(1, $subscriptions);
        $this->assertEquals($subscription2->plan->id, $subscriptions[0]->plan->id);
    }

    /**
     * @test
     * @covers \App\Models\Subscription::getStartAtToDatetimeAttribute
     */
    public function iShouldBeAbleToGetStartAtDate()
    {
        $date = Carbon::now();

        $subscription = factory(Subscription::class)->create([
            'braintree_subscription_first_billing_date' => $date,
        ]);

        $this->assertEquals(
            $subscription->start_at_to_datetime,
            Carbon::parse($date)->toRfc822String()
        );

        $subscription = factory(Subscription::class)->create([
            'braintree_subscription_first_billing_date' => null,
        ]);

        $this->assertEquals($subscription->start_at_to_datetime, null);
    }

    /**
     * @test
     * @covers \App\Models\Subscription::getEndAtToDatetimeAttribute
     */
    public function iShouldBeAbleToGetEndAtDate()
    {
        $date = Carbon::now();

        $subscription = factory(Subscription::class)->create([
            'braintree_subscription_paid_through_date' => $date,
        ]);

        $this->assertEquals(
            $subscription->end_at_to_datetime,
            Carbon::parse($date)->toRfc822String()
        );

        $subscription = factory(Subscription::class)->create([
            'braintree_subscription_paid_through_date' => null,
        ]);

        $this->assertEquals($subscription->end_at_to_datetime, null);
    }

    /**
     * @test
     * @covers \App\Models\Subscription::getPriceToHumanAttribute
     */
    public function iShouldBeAbleToGetPrice()
    {
        $price = 1;

        $subscription = factory(Subscription::class)->create([
            'braintree_subscription_price' => $price,
        ]);

        $this->assertEquals(
            $subscription->price_to_human,
            sprintf('$%s USD', $price)
        );

        $subscription = factory(Subscription::class)->create([
            'braintree_subscription_price' => null,
        ]);

        $this->assertEquals($subscription->price_to_human, null);
    }
}
