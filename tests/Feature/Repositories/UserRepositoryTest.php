<?php

namespace Tests\Feature;

use App\Models\User;
use App\Repositories\Admin\UserRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserRepositoryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\Repository::create
     * @covers \App\Repositories\Admin\UserRepository::__construct
     * @covers \App\Repositories\Admin\UserRepository::create
     */
    public function iShouldBeAbleToCreateUserWithoutPassword()
    {
        $repository = app()->make(UserRepository::class);

        $data = [
            'first_name' => 'Alex',
            'last_name' => 'Kolev',
            'phone' => '+0123456789',
            'email' => 'test@test.com',
        ];

        $response = $repository->create($data);

        $this->assertInstanceOf(User::class, $response);
        $this->assertNull($response->password);
        $this->assertTrue(User::where('email', 'test@test.com')->exists());
    }

    /**
     * @test
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\Repository::create
     * @covers \App\Repositories\Admin\UserRepository::__construct
     * @covers \App\Repositories\Admin\UserRepository::create
     */
    public function iShouldBeAbleToCreateUserWithPassword()
    {
        $repository = app()->make(UserRepository::class);

        $data = [
            'first_name' => 'Alex',
            'last_name' => 'Kolev',
            'phone' => '+0123456789',
            'email' => 'test@test.com',
            'password' => 'secret',
            'password_confirmation' => 'secret',
        ];

        $response = $repository->create($data);

        $this->assertInstanceOf(User::class, $response);
        $this->assertNotNull($response->password);
        $this->assertTrue(User::where('email', 'test@test.com')->exists());
    }

    /**
     * @test
     * @covers \App\Repositories\Admin\Repository::prepare
     * @covers \App\Repositories\Admin\Repository::update
     * @covers \App\Repositories\Admin\UserRepository::__construct
     * @covers \App\Repositories\Admin\UserRepository::update
     */
    public function iShouldBeAbleToUpdateUser()
    {
        $user = factory(User::class)->create();

        $repository = app()->make(UserRepository::class);

        $data = [
            'first_name' => 'Alex',
            'last_name' => 'Kolev',
            'phone' => '+0123456789',
            'email' => 'test@test.com',
            'password' => 'secret',
            'password_confirmation' => 'secret',
        ];

        $response = $repository->update($user, $data);

        $this->assertInstanceOf(User::class, $response);
        $this->assertTrue(User::where([
            'first_name' => 'Alex',
            'last_name' => 'Kolev',
            'phone' => '+0123456789',
            'email' => 'test@test.com',
        ])->exists());
    }
}
