<?php

namespace Tests\Feature\Commands;

use App\Console\Commands\CrmNotificationCommand;
use App\Models\Account;
use App\Models\AccountContact;
use App\Models\AccountMarketingTemplate;
use App\Models\AccountUser;
use App\Models\CrmNotification;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class CrmNotificationCommandTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @covers \App\Console\Commands\CrmNotificationCommand::__construct
     */
    public function iShouldHaveMailNotificationCommand()
    {
        $this->assertTrue(class_exists(CrmNotificationCommand::class));
    }

    /**
     * @test
     * @covers \App\Console\Commands\CrmNotificationCommand::__construct
     * @covers \App\Console\Commands\CrmNotificationCommand::handle
     * @covers \App\Repositories\Admin\CrmNotificationRepository::edit
     * @covers \App\Services\Admin\CrmNotificationService::send
     * @covers \App\Services\Admin\CrmNotificationService::sendEmail
     * @covers \App\Mail\CrmNotificationMail::__construct
     * @covers \App\Mail\CrmNotificationMail::build
     * @covers \App\Console\Kernel::commands
     * @covers \App\Console\Kernel::schedule
     */
    public function iShouldBeAbleSendEmailToContacts()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $template = factory(AccountMarketingTemplate::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id,
            'type' => 'email'
        ]);

        $contact = factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $notification = factory(CrmNotification::class)->create([
            'contact_id' => $contact->id,
            'contact_email' => $contact->email,
            'template_id' => $template->id,
            'template_title' => $template->title,
            'template_type' => $template->type,
            'template_content' => $template->content,
            'is_sent' => false,
            'sending_datetime' => now()->format('Y-m-d H:i:s')
        ]);

        $this->assertEquals($notification->is_sent, false);

        $this->artisan('notify:contacts')
            ->expectsOutput("Command run successfully")
            ->assertExitCode(0);

        $notification->refresh();
        $this->assertEquals($notification->is_sent, true);
    }

    /**
     * @test
     * @covers \App\Console\Commands\CrmNotificationCommand::__construct
     * @covers \App\Console\Commands\CrmNotificationCommand::handle
     * @covers \App\Repositories\Admin\CrmNotificationRepository::edit
     * @covers \App\Services\Admin\CrmNotificationService::send
     * @covers \App\Services\Admin\CrmNotificationService::sendSms
     * @covers \App\Console\Kernel::commands
     * @covers \App\Console\Kernel::schedule
     */
    public function iShouldBeAbleSendSmsToContacts()
    {
        $user = $this->loginAsAdmin();

        $account = factory(Account::class)->create();

        factory(AccountUser::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $template = factory(AccountMarketingTemplate::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id,
            'type' => 'sms'
        ]);

        $contact = factory(AccountContact::class)->create([
            'account_id' => $account->id,
            'user_id' => $user->id
        ]);

        $notification = factory(CrmNotification::class)->create([
            'contact_id' => $contact->id,
            'contact_email' => $contact->email,
            'template_id' => $template->id,
            'template_title' => $template->title,
            'template_type' => $template->type,
            'template_content' => $template->content,
            'is_sent' => false,
            'sending_datetime' => now()->format('Y-m-d H:i:s')
        ]);

        $this->assertEquals($notification->is_sent, false);

        $this->artisan('notify:contacts')
            ->expectsOutput("Command run successfully")
            ->assertExitCode(0);
    }
}
