<?php

Route::get('/', 'PageController@index')->name('client.pages.index');
Route::get('/pricing', 'PageController@pricing')->name('client.pages.pricing');

Route::get('/connect/{provider}', 'ConnectController@redirect')->name('client.connect.redirect');
Route::get('/connect/{provider}/callback', 'ConnectController@callback')->name('client.connect.callback');

Route::group(['middleware' => 'auth.not.user'], function () {
    Route::get('/signup', 'AuthController@signup')->name('client.auth.signup');
    Route::post('/register', 'AuthController@register')->name('client.auth.register');

    Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('client.password.forgot');
    Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('client.password.email');
    Route::post('/password/reset', 'ForgotPasswordController@reset')->name('client.password.update');
    Route::get('/password/reset/{email}/{token}', 'ForgotPasswordController@showResetForm')
        ->name('client.password.reset');

    Route::get('/signin', 'AuthController@signin')->name('client.auth.signin');
    Route::post('/login', 'AuthController@login')->name('client.auth.login');
});

Route::group(['middleware' => 'auth.only.user'], function () {

    Route::get('/logout', 'AuthController@logout')->name('client.auth.logout');

    Route::get('/cabinet', 'CabinetController@index')->name('client.cabinet.index');
    Route::post('/cabinet/test', 'CabinetController@test')->name('client.cabinet.test');

    Route::get('/account/edit', 'AccountController@edit')->name('client.account.edit');
    Route::put('/account/update', 'AccountController@update')->name('client.account.update');

    Route::get('/payment', 'PaymentController@index')->name('client.payment.index');
    Route::post('/payment', 'PaymentController@store')->name('client.payment.store');
});
