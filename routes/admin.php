<?php

Route::group(['middleware' => ['auth.not.admin']], function () {
    Route::get('login', 'AuthController@login')->name('admin.auth.login');
    Route::post('authenticate', 'AuthController@authenticate')->name('admin.auth.authenticate');
});

Route::group(['middleware' => ['auth.only.admin']], function () {
    Route::get('logout', 'AuthController@logout')->name('admin.auth.logout');
});

Route::group(['middleware' => ['auth.only.admin']], function () {
    Route::get('', function () {
        return redirect(route('admin.dashboard.index'));
    });

    Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard.index');

    Route::group(['prefix' => 'properties/'], function () {
        Route::get('', 'PropertyController@index')
            ->name('admin.properties.index');
        Route::get('create/', 'PropertyController@create')
            ->name('admin.properties.create');
        Route::get('{record}/edit', 'PropertyController@edit')
            ->name('admin.properties.edit');
        Route::post('store/', 'PropertyController@store')
            ->name('admin.properties.store');
        Route::put('{record}/update/', 'PropertyController@update')
            ->name('admin.properties.update');
        Route::put('{record}/updateStatus/', 'PropertyController@updateStatus')
            ->name('admin.properties.updateStatus');
        Route::delete('{record}/destroy/', 'PropertyController@destroy')
            ->name('admin.properties.destroy');

        Route::group(['prefix' => '{property}/videos/'], function () {
            Route::get('', 'PropertyVideoController@index')
                ->name('admin.property_videos.index');
            Route::get('create', 'PropertyVideoController@create')
                ->name('admin.property_videos.create');
            Route::get('{record}/edit', 'PropertyVideoController@edit')
                ->name('admin.property_videos.edit');
            Route::post('store', 'PropertyVideoController@store')
                ->name('admin.property_videos.store');
            Route::post('{record}/generate', 'PropertyVideoController@generate')
                ->name('admin.property_videos.generate');
            Route::put('{record}/update', 'PropertyVideoController@update')
                ->name('admin.property_videos.update');
            Route::delete('{record}/destroy', 'PropertyVideoController@destroy')
                ->name('admin.property_videos.destroy');

            Route::group(['prefix' => '{video}/files/'], function () {
                Route::get('', 'PropertyVideoFileController@index')
                    ->name('admin.property_video_files.index');
                Route::post('store', 'PropertyVideoFileController@store')
                    ->name('admin.property_video_files.store');
                Route::delete('{record}/destroy', 'PropertyVideoFileController@destroy')
                    ->name('admin.property_video_files.destroy');
            });
        });
    });

    Route::group(['prefix' => 'accounts/'], function () {
        Route::get('', 'AccountController@index')->name('admin.accounts.index');
        Route::get('{record}/edit', 'AccountController@edit')->name('admin.accounts.edit');
        Route::put('{record}/update/', 'AccountController@update')->name('admin.accounts.update');

        Route::group(['prefix' => '{account}/updates'], function () {
            Route::get('', 'AccountUpdateController@index')->name('admin.account_updates.index');
            Route::get('create/', 'AccountUpdateController@create')->name('admin.account_updates.create');
            Route::get('{record}/edit', 'AccountUpdateController@edit')->name('admin.account_updates.edit');
            Route::post('store/', 'AccountUpdateController@store')->name('admin.account_updates.store');
            Route::put('{record}/update/', 'AccountUpdateController@update')->name('admin.account_updates.update');
            Route::delete('{record}/destroy/', 'AccountUpdateController@destroy')
                ->name('admin.account_updates.destroy');
        });

        Route::group(['prefix' => '{account}/contacts/'], function () {
            Route::get('', 'AccountContactController@index')->name('admin.account_contacts.index');
            Route::get('create/', 'AccountContactController@create')->name('admin.account_contacts.create');
            Route::get('{record}/edit', 'AccountContactController@edit')->name('admin.account_contacts.edit');
            Route::post('store/', 'AccountContactController@store')->name('admin.account_contacts.store');
            Route::put('{record}/update/', 'AccountContactController@update')->name('admin.account_contacts.update');
            Route::delete('{record}/destroy/', 'AccountContactController@destroy')
                ->name('admin.account_contacts.destroy');
            Route::post('import', 'AccountContactController@import')->name('admin.account_contacts.import');
        });

        Route::group(['prefix' => '{account}/marketing-templates/'], function () {
            Route::get('', 'AccountMarketingTemplateController@index')->name('admin.account_marketing_templates.index');
            Route::get('create/', 'AccountMarketingTemplateController@create')
                ->name('admin.account_marketing_templates.create');
            Route::get('{record}/edit', 'AccountMarketingTemplateController@edit')
                ->name('admin.account_marketing_templates.edit');
            Route::post('store/', 'AccountMarketingTemplateController@store')
                ->name('admin.account_marketing_templates.store');
            Route::put('{record}/update/', 'AccountMarketingTemplateController@update')
                ->name('admin.account_marketing_templates.update');
            Route::delete('{record}/destroy/', 'AccountMarketingTemplateController@destroy')
                ->name('admin.account_marketing_templates.destroy');
            Route::get('{record}/prepare', 'AccountMarketingTemplateController@prepare')
                ->name('admin.account_marketing_templates.prepare');
            Route::post('{record}/send/', 'AccountMarketingTemplateController@send')
                ->name('admin.account_marketing_templates.send');
        });

        Route::group(['prefix' => '{account}/tags/'], function () {
            Route::get('', 'AccountTagController@index')->name('admin.account_tags.index');
            Route::get('create/', 'AccountTagController@create')->name('admin.account_tags.create');
            Route::get('{record}/edit', 'AccountTagController@edit')->name('admin.account_tags.edit');
            Route::post('store/', 'AccountTagController@store')->name('admin.account_tags.store');
            Route::put('{record}/update/', 'AccountTagController@update')->name('admin.account_tags.update');
            Route::delete('{record}/destroy/', 'AccountTagController@destroy')->name('admin.account_tags.destroy');
        });
    });

    Route::group(['prefix' => 'users/'], function () {
        Route::get('', 'UserController@index')->name('admin.users.index');
        Route::get('create/', 'UserController@create')->name('admin.users.create');
        Route::get('{record}/edit', 'UserController@edit')->name('admin.users.edit');
        Route::post('store/', 'UserController@store')->name('admin.users.store');
        Route::put('{record}/update/', 'UserController@update')->name('admin.users.update');
        Route::delete('{record}/destroy/', 'UserController@destroy')->name('admin.users.destroy');
    });

    Route::group(['prefix' => 'payments/'], function () {
        Route::get('create/', 'PaymentController@create')->name('admin.payments.create');
        Route::post('store/', 'PaymentController@store')->name('admin.payments.store');
    });

    Route::group(['prefix' => 'charges/'], function () {
        Route::get('', 'ChargeController@index')->name('admin.charges.index');
        Route::get('{record}/edit', 'ChargeController@edit')->name('admin.charges.edit');
        Route::post('{record}/pay/', 'ChargeController@pay')->name('admin.charges.pay');
    });

    Route::group(['prefix' => 'subscriptions/'], function () {
        Route::get('', 'SubscriptionController@index')->name('admin.subscriptions.index');
        Route::get('{record}/edit', 'SubscriptionController@edit')->name('admin.subscriptions.edit');
        Route::post('{record}/pay/', 'SubscriptionController@pay')->name('admin.subscriptions.pay');
    });

    Route::group(['prefix' => 'transactions/'], function () {
        Route::get('', 'TransactionController@index')->name('admin.transactions.index');
    });

    Route::group(['prefix' => 'plans/'], function () {
        Route::get('', 'PlanController@index')->name('admin.plans.index');
        Route::get('{record}/edit', 'PlanController@edit')->name('admin.plans.edit');
        Route::put('{record}/update/', 'PlanController@update')->name('admin.plans.update');
    });

    Route::group(['prefix' => 'micrositejob/'], function () {
        Route::get('{record}/add', 'MicrositeJobController@add')->name('admin.micrositejobs.add');
    });

    Route::group(['prefix' => 'webhooks/'], function () {
        Route::get('', 'WebhookController@index')->name('admin.webhooks.index');
    });
});
