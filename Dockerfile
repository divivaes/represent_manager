FROM link

WORKDIR /app
COPY --chown=apache ./ /app
COPY ./server-setup/cron.conf /etc/crontabs/apache

RUN mkdir /temp && chown apache:apache /temp

RUN npm install
RUN npm run production
ENV PATH ./node_modules/bower/bin/:$PATH

## Composer install
RUN composer install --no-dev

ENV PATH vendor/bin/:$PATH

EXPOSE 80
ENTRYPOINT ["/app/server-setup/start.sh"]
