#!/bin/sh
git checkout -f

cd /storage/www/staging.reachadvance.com/web

echo "Install npm"
npm install
echo "npm production"
npm run production

echo "Composer install"
composer install

# Config cache clears and rebuilds the configuration cache.
echo "Artisan config cache"
php artisan config:cache

# Start DB migrations
echo "Artisan migrate"
php artisan migrate --force

# Link the storage
echo "Artisan config storate"
php artisan storage:link

# Clear route cache
echo "Artisan route"
php artisan route:clear

# Clear compiled view files
echo "Artisan view"
php artisan view:clear