#!/bin/sh
set -e

DIRNAME="$(dirname $(readlink -f $0))"

EXCLUDE_LIST="${DIRNAME}/exclude_list.txt"

rsync -rltgoDvz --exclude-from ${EXCLUDE_LIST} $(pwd)/ ${STAGE_SERVER_USER}@${STAGE_SERVER}:/storage/www/staging.reachadvance.com/web
ssh ${STAGE_SERVER_USER}@${STAGE_SERVER} "cd web/server-setup/deployment && ./after_deploy.sh"
ssh ${STAGE_SERVER_USER}@${STAGE_SERVER} "rm -rf web/server-setup"

