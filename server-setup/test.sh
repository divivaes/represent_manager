#!/bin/sh

# Exit on error
set -e

# Enable XDebug
$(dirname "$0")/xdebug.sh

echo 'Install PHP Dev Packages'
cp .env.ci .env
composer install

echo 'Config Project'
php artisan key:generate
php artisan config:cache

# Start PHP Tests
echo 'PHP Unit Test'
phpunit --configuration phpunit.xml --coverage-clover clover.xml --coverage-html clover.html

echo 'PHP Coverage Check'
php server-setup/coverage-checker.php clover.xml 50

echo 'PHP PSR2 Test'
phpcs -p --standard=PSR2 app config routes tests

echo 'PHP Laravel Code Analyse'
php artisan code:analyse --paths=app --level=5
