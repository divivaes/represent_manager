# enable xdebug coverage for testing with phpunit (already installed)
if grep -qxF 'zend_extension=/usr/lib/php7/modules/xdebug.so' /etc/php7/php.ini; then
    echo 'XDebug is already enabled'
else
    echo "Enable XDebug for unit testing..."
    echo 'zend_extension=/usr/lib/php7/modules/xdebug.so' >> /etc/php7/php.ini;
    echo 'xdebug.coverage_enable=On' >> /etc/php7/php.ini;
    echo 'xdebug.remote_enable=1' >> /etc/php7/php.ini;
    echo 'xdebug.remote_connect_back=1' >> /etc/php7/php.ini;
    echo 'xdebug.remote_log=/tmp/xdebug.log' >> /etc/php7/php.ini;
    echo 'xdebug.remote_autostart=true' >> /etc/php7/php.ini;
fi
