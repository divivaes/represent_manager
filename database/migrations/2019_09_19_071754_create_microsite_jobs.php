<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMicrositeJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('microsite_jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid', 256)->nullable();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('microsite_id')->nullable();
            $table->foreign('microsite_id')
                ->references('id')
                ->on('microsites')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->string('delete_uuid', 256)->nullable();
            $table->string('delete_domain', 256)->nullable();
            $table->string('delete_type', 20)->nullable();
            $table->enum('status', ['waiting', 'processing', 'done', 'error']);
            $table->string('job_status_notes')->nullable();
            $table->dateTime('job_start_at')->nullable();
            $table->dateTime('job_finish_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('microsite_jobs');
    }
}
