<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid', 256)->nullable();

            $table->string('name', 64)->nullable();
            $table->string('unique_key', 64)->nullable();
            $table->decimal('price')->nullable();
            $table->text('description')->nullable();
            $table->text('features')->nullable();

            $table->string('braintree_plan_id', 128)->nullable();

            $table->boolean('is_active')->default(true);

            $table->unique(['uuid']);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
