<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyVideoFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_video_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid', 256)->nullable();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('property_video_id')->nullable();
            $table->foreign('property_video_id')
                ->references('id')
                ->on('property_videos')
                ->onUpdate('CASCADE')
                ->onDelete('SET NULL');

            $table->string('path', 1024)->nullable();

            $table->integer('order_id')->nullable()->default(0);

            $table->string('submitter_ip', 256)->nullable();
            $table->string('submitter_country', 256)->nullable();
            $table->string('submitter_city', 256)->nullable();
            $table->string('submitter_platform', 256)->nullable();
            $table->string('submitter_browser', 256)->nullable();
            $table->string('submitter_agent', 256)->nullable();

            $table->boolean('is_active')->default(true);

            $table->unique(['user_id', 'property_video_id', 'uuid']);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_video_files');
    }
}
