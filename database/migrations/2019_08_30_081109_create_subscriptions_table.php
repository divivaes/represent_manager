<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid', 256)->nullable();

            $table->unsignedBigInteger('account_id')->nullable();
            $table->string('account_name', 254)->nullable();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('user_first_name', 35)->nullable();
            $table->string('user_last_name', 35)->nullable();
            $table->string('user_email', 254)->nullable();

            $table->unsignedBigInteger('plan_id')->nullable();
            $table->string('plan_name', 254)->nullable();

            $table->string('status', 35)->nullable();

            $table->string('submitter_ip', 256)->nullable();
            $table->string('submitter_country', 256)->nullable();
            $table->string('submitter_city', 256)->nullable();
            $table->string('submitter_platform', 256)->nullable();
            $table->string('submitter_browser', 256)->nullable();
            $table->string('submitter_agent', 256)->nullable();

            $table->string('braintree_subscription_id', 128)->nullable();
            $table->string('braintree_subscription_plan_id', 128)->nullable();
            $table->decimal('braintree_subscription_price')->nullable();
            $table->timestamp('braintree_subscription_first_billing_date')->nullable();
            $table->timestamp('braintree_subscription_paid_through_date')->nullable();
            $table->text('braintree_subscription_payload')->nullable();

            $table->unique(['uuid', 'user_id']);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
