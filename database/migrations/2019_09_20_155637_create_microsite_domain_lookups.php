<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMicrositeDomainLookups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('microsite_domain_lookups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid', 256)->nullable();

            $table->unsignedBigInteger('microsite_id')->nullable();
            $table->foreign('microsite_id')
                ->references('id')
                ->on('microsites')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->string('domain');
            $table->ipAddress('current_ip');
            $table->boolean('pointed')->default(False);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('microsite_domain_lookups');
    }
}
