<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid', 256)->nullable();

            $table->unsignedBigInteger('account_id')->nullable();
            $table->string('account_name', 254)->nullable();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('user_first_name', 35)->nullable();
            $table->string('user_last_name', 35)->nullable();
            $table->string('user_email', 254)->nullable();

            $table->unsignedBigInteger('plan_id')->nullable();
            $table->string('plan_name', 254)->nullable();

            $table->timestamp('start_at')->nullable();
            $table->timestamp('end_at')->nullable();

            $table->decimal('price')->nullable();

            $table->string('status', 35)->nullable();

            $table->string('submitter_ip', 256)->nullable();
            $table->string('submitter_country', 256)->nullable();
            $table->string('submitter_city', 256)->nullable();
            $table->string('submitter_platform', 256)->nullable();
            $table->string('submitter_browser', 256)->nullable();
            $table->string('submitter_agent', 256)->nullable();

            $table->string('braintree_charge_id', 128)->nullable();
            $table->text('braintree_charge_payload')->nullable();

            $table->unique(['uuid', 'user_id']);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charges');
    }
}
