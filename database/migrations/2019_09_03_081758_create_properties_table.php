<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid', 256)->nullable();

            $table->unsignedBigInteger('account_id')->nullable();
            $table->foreign('account_id')
                ->references('id')
                ->on('accounts')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null')
                ->onUpdate('cascade');

            $table->string('address', 512)->nullable();
            $table->string('type', 128)->nullable();
            $table->string('status', 128)->nullable();
            $table->float('price')->nullable();
            $table->integer('year_built')->nullable();
            $table->integer('beds')->nullable();
            $table->integer('baths')->nullable();
            $table->integer('square_feet')->nullable();
            $table->integer('lot_size')->nullable();
            $table->text('description')->nullable();

            $table->string('submitter_ip', 256)->nullable();
            $table->string('submitter_country', 256)->nullable();
            $table->string('submitter_city', 256)->nullable();
            $table->string('submitter_platform', 256)->nullable();
            $table->string('submitter_browser', 256)->nullable();
            $table->string('submitter_agent', 256)->nullable();

            $table->boolean('is_active')->default(true);

            $table->unique(['uuid', 'account_id', 'user_id']);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
