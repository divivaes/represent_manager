<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Account;
use Faker\Generator as Faker;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

$factory->define(Account::class, function (Faker $faker) {
    return [
        'uuid' => (string) Uuid::generate(4),
        'name' => $faker->userName,
        'is_active' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});
