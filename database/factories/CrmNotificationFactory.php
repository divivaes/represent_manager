<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CrmNotification;
use Webpatser\Uuid\Uuid;
use Faker\Generator as Faker;

$factory->define(CrmNotification::class, function (Faker $faker) {

    return [
        'uuid' => Uuid::generate(4),
        'contact_id' => 1,
        'contact_email' => $faker->unique()->safeEmail,
        'template_id' => 1,
        'template_type' => $faker->word,
        'template_title' => $faker->title,
        'template_content' => $faker->sentence,
        'sending_datetime' => now()->format('Y-m-d H:i:s')
    ];
});
