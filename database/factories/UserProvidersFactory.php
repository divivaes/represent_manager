<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use App\Models\UserProvider;
use Faker\Generator as Faker;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(UserProvider::class, function (Faker $faker) {
    return [
        'uuid' => (string) Uuid::generate(4),
        'user_id' => factory(User::class)->create()->id,
        'provider_name' => 'facebook',
        'provider_id' => 1,
        'provider_token' => 'token',
        'provider_token_expire_at' => Carbon::now()->addDays(10),
        'is_active' => true,
    ];
});
