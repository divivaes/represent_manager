<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'uuid' => (string) Uuid::generate(4),
        'first_name' => $faker->userName, # firstName sometimes throws error due to quote in name
        'last_name' => $faker->userName, # lastName sometimes throws error due to quote in name
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->unique()->phoneNumber,
        'email_verified_at' => Carbon::now(),
        'role' => 'admin',
        'is_active' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
        'password' => bcrypt('secret'),
        'remember_token' => Str::random(10),
    ];
});
