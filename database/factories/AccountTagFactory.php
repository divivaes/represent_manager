<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AccountTag;
use Webpatser\Uuid\Uuid;
use App\Models\Account;
use App\Models\User;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(AccountTag::class, function (Faker $faker) {
    return [
        'uuid' => (string) Uuid::generate(4),
        'account_id' => factory(Account::class)->create()->id,
        'user_id' => factory(User::class)->create()->id,
        'title' => $faker->unique()->userName,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});
