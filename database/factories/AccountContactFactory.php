<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\AccountContact;
use Webpatser\Uuid\Uuid;
use App\Models\User;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(AccountContact::class, function (Faker $faker) {

    return [
        'uuid' => (string) Uuid::generate(4),
        'account_id' => 1,
        'user_id' => 1,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->unique()->phoneNumber,
        'notes' => $faker->sentence,
        'status' => 'new',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});
