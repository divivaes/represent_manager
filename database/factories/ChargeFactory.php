<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use App\Models\Plan;
use App\Models\Charge;
use Faker\Generator as Faker;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Charge::class, function (Faker $faker) {
    $user = factory(User::class)->create();

    return [
        'uuid' => (string) Uuid::generate(4),
        'user_id' => $user->id,
        'user_first_name' => $user->first_name,
        'user_last_name' => $user->last_name,
        'plan_id' => factory(Plan::class)->create()->id,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
    ];
});
