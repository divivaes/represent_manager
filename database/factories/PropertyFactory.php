<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Account;
use App\Models\Property;
use App\Models\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Property::class, function (Faker $faker) {

    $types = ["Flat", "Industrial", "Apartment", "Office"];
    $typeId = array_rand($types);

    return [
        'uuid' => (string) Uuid::generate(4),
        'account_id' => factory(Account::class)->create()->id,
        'user_id' => factory(User::class)->create()->id,
        'address' => $faker->address,
        'type' => $types[$typeId],
        'status' => 'Pending',
        'price' => 100,
        'beds' => 2,
        'baths' => 2,
        'is_active' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
    ];
});
