<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\AccountUser;
use Faker\Generator as Faker;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

$factory->define(AccountUser::class, function (Faker $faker) {
    return [
        'uuid' => (string) Uuid::generate(4),
        'is_active' => true,
        'role' => 'admin',
        'account_id' => 1,
        'user_id' => 1,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
    ];
});
