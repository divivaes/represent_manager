<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Account;
use App\Models\User;
use App\Models\AccountUpdate;
use Faker\Generator as Faker;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

$factory->define(AccountUpdate::class, function (Faker $faker) {
    return [
        'uuid' => (string) Uuid::generate(4),
        'title' => $faker->unique()->userName,
        'content' => $faker->text,
        'account_id' => factory(Account::class)->create()->id,
        'user_id' => factory(User::class)->create()->id,
        'is_active' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});
