<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Plan;
use Faker\Generator as Faker;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

$factory->define(Plan::class, function (Faker $faker) {
    return [
        'uuid' => (string) Uuid::generate(4),
        'name' => $faker->userName,
        'price' => 20.00,
        'description' => $faker->text,
        'features' => $faker->text,
        'braintree_plan_id' => 'pl1',
        'is_active' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});
