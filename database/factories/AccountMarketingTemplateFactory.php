<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\AccountMarketingTemplate;
use Webpatser\Uuid\Uuid;
use App\Models\Account;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(AccountMarketingTemplate::class, function (Faker $faker) {

    $types = ['email', 'sms'];
    $typeId = array_rand($types);

    return [
        'uuid' => (string) Uuid::generate(4),
        'account_id' => 1,
        'user_id' => 1,
        'title' => $faker->word,
        'content' => $faker->sentence,
        'type' => $types[$typeId]
    ];
});
