<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

class AccountUserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('account_user')->insert([
            'uuid' => (string) Uuid::generate(4),
            'role' => 'admin',
            'account_id' => 1,
            'user_id' => 1,
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('account_user')->insert([
            'uuid' => (string) Uuid::generate(4),
            'role' => 'admin',
            'account_id' => 2,
            'user_id' => 2,
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
