<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'uuid' => (string) Uuid::generate(4),
            'first_name' => 'Alex',
            'last_name' => 'Kolev',
            'phone' => '+1234567890',
            'email' => 'admin@mtr-design.com',
            'email_verified_at' => Carbon::now(),
            'role' => 'admin',
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'password' => bcrypt('secret'),
            'remember_token' => Str::random(10),
        ]);

        DB::table('users')->insert([
            'uuid' => (string) Uuid::generate(4),
            'first_name' => 'Milen',
            'last_name' => 'Nedev',
            'phone' => '+1234567890',
            'email' => 'user@mtr-design.com',
            'email_verified_at' => Carbon::now(),
            'role' => 'user',
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'password' => bcrypt('secret'),
            'remember_token' => Str::random(10),
        ]);
    }
}
