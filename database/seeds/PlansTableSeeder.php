<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

class PlansTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('plans')->insert([
            'uuid' => (string) Uuid::generate(4),
            'name' => 'Basic',
            'unique_key' => 'basic',
            'price' => 299.00,
            'description' => 'Lorem ipsum dolor sit amet, et vel veri paulo. Ea eirmod utamur scriptorem vim.',
            'features' => "Scheduled Facebook Posts\nBranded Website\nScheduled Website Posts",
            'braintree_plan_id' => "pl1",
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('plans')->insert([
            'uuid' => (string) Uuid::generate(4),
            'name' => 'Pro',
            'unique_key' => 'pro',
            'price' => 499.00,
            'description' => 'Lorem ipsum dolor sit amet, et vel veri paulo. Ea eirmod utamur scriptorem vim.',
            'features' => "Local Market Update Videos\nDedicated Account Manager\nGuaranteed Reach - 5,0000 Prospects",
            'braintree_plan_id' => "pl2",
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('plans')->insert([
            'uuid' => (string) Uuid::generate(4),
            'name' => 'Ultimate Pro',
            'unique_key' => 'ultimate_pro',
            'price' => 699.00,
            'description' => 'Lorem ipsum dolor sit amet, et vel veri paulo. Ea eirmod utamur scriptorem vim.',
            'braintree_plan_id' => "pl3",
            'features' => "Ad Campaign Management\nDedicated Phone Support",
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
